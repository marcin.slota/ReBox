﻿using System;
using OPCDA;
using OPCDA.NET;

namespace ReBox
{
    partial class Rebox
    {
        public static void HelloOPCDA()
        {
            Console.WriteLine("Hello OPC-DA!");
        }
        public bool OpenOpcDaPortByProc(int _i)
        {
            // _i to nr PROC
            int hw_id = SRV[SAVE[PROC[_i].SAVE_ID].SERVER_ID].HW_ID;
            int srv_id = SAVE[PROC[_i].SAVE_ID].SERVER_ID;
            bool ret = false;


            if (OpcDaSrv[srv_id] == null)
            {
                OpcDaSrv[srv_id] = new OpcServer();
                if (OpcDaSrv[srv_id].isConnectedDA == false)
                {
                    try
                    {
                        OpcDaSrv[srv_id].Connect(HW[hw_id].URL);
                 
                    }
                    catch
                    {
                      
                    }
                }
            }

            if (OpcDaSrv[srv_id].isConnectedDA == true)
            {
                if (OpcDaTcpClient[srv_id] == null)
                {
                    try
                    {
                        OpcDaTcpClient[srv_id] = new SyncIOGroup(OpcDaSrv[srv_id]);
                      
                        ret = true;
                    }
                    catch
                    {
                     
                        ret = false;
                    }
                }
                else
                {
                 
                    ret = true;
                }
            }
            else
            {
           
                ret = false;
            }
            return ret;

        }//public bool OpenOpcDaPortByProc(int _i)

        public bool OpenOpcDaPort(int _i)
        {
            // _i to nr MEAS
            bool ret = false;


            if (OpcDaSrv[MEAS[_i].SERVER_ID] == null)
            {
                OpcDaSrv[MEAS[_i].SERVER_ID] = new OpcServer();
                if (OpcDaSrv[MEAS[_i].SERVER_ID].isConnectedDA == false)
                {
                    try
                    {
                        OpcDaSrv[MEAS[_i].SERVER_ID].Connect(HW[SRV[MEAS[_i].SERVER_ID].HW_ID].URL);
                       
                    }
                    catch
                    {
                   
                    }
                }
            }

            if (OpcDaSrv[MEAS[_i].SERVER_ID].isConnectedDA == true)
            {
                if (OpcDaTcpClient[MEAS[_i].SERVER_ID] == null)
                {
                    try
                    {
                        OpcDaTcpClient[MEAS[_i].SERVER_ID] = new SyncIOGroup(OpcDaSrv[MEAS[_i].SERVER_ID]);
                  
                        ret = true;
                    }
                    catch
                    {
                  
                        ret = false;
                    }
                }
                else
                {
                   
                    ret = true;
                }
            }
            else
            {
              
                ret = false;
            }
            return ret;

        }//public bool OpenOpcDaPort(int _i)
        public bool readOpcDa(int _i)
        {
            bool ret = false;

            if (OpenOpcDaPort(_i) == true)
            {
                if (MEAS[_i].OPERATION == "READ")
                {
                    try
                    {

                        //var tmp_value = OpcDaTcpClient[MEAS[_i].SERVER_ID].Read(OPCDATASOURCE.OPC_DS_DEVICE, MEAS[_i].TAG, out OPCItemState Rslt);
                        var tmp_value = OpcDaTcpClient[MEAS[_i].SERVER_ID].Read(OPCDATASOURCE.OPC_DS_DEVICE, MEAS[_i].TAG, out OPCItemState Rslt);
                        
                        if (Rslt != null)
                        {
                            var tmp_val = Rslt.DataValue;
                            if (MEAS[_i].TYPE == "BYTE") MEAS[_i].uVALUE16[0] = Convert.ToByte(tmp_val);
                            if (MEAS[_i].TYPE == "UINT16") MEAS[_i].uVALUE16[0] = Convert.ToUInt16(tmp_val);
                            if (MEAS[_i].TYPE == "INT16") MEAS[_i].dVALUE16[0] = Convert.ToInt16(tmp_val);
                            if (MEAS[_i].TYPE == "UINT32") MEAS[_i].uVALUE32[0] = Convert.ToUInt32(tmp_val);
                            if (MEAS[_i].TYPE == "INT32") MEAS[_i].dVALUE32[0] = Convert.ToInt32(tmp_val);
                            if (MEAS[_i].TYPE == "DOUBLE") MEAS[_i].fVALUE[0] = (double)tmp_val;
                            //if (MEAS[_i].TYPE == "BOOL") MEAS[_i].bVALUE[0] = (bool)tmp_val;
                            MEAS[_i].OK = 1;
                            HW[SRV[MEAS[_i].SERVER_ID].HW_ID].OK = 1;
                            SRV[MEAS[_i].SERVER_ID].OK = 1;
                            ret = true;
                        }
                        else
                        {
                            MEAS[_i].OK = 0;
                            ret = false;
                            SRV[_i].OK = 0; ;
                        }
                    }
                    catch
                    {
                        MEAS[_i].OK = 0;
                        ret = false;
                        SRV[_i].OK = 0;
                    }
                }
            }
            else
            {
                MEAS[_i].OK = 0;
                ret = false;
            }


            return ret;
        }//public bool readOpcDa(int _i)

        public bool writeOpcDa(int _i, string v)
        {
            bool ret = false;
            //_i to nr procedury
            int hw_id = SRV[SAVE[PROC[_i].SAVE_ID].SERVER_ID].HW_ID;
            int srv_id = SAVE[PROC[_i].SAVE_ID].SERVER_ID;

            if (OpenOpcDaPortByProc(_i) == true)
            {
                if (SAVE[PROC[_i].SAVE_ID].OPERATION == "WRITE")
                {
                    if (PROC[_i].TYPE == "VARIABLE")
                    {
                        if (SAVE[PROC[_i].SAVE_ID].TYPE == "BYTE") OpcDaTcpClient[srv_id].Write(SAVE[PROC[_i].SAVE_ID].TAG, (object)(Convert.ToByte(v)));
                        if (SAVE[PROC[_i].SAVE_ID].TYPE == "INT16") OpcDaTcpClient[srv_id].Write(SAVE[PROC[_i].SAVE_ID].TAG, (object)(Convert.ToInt16(v)));
                        if (SAVE[PROC[_i].SAVE_ID].TYPE == "UINT16") OpcDaTcpClient[srv_id].Write(SAVE[PROC[_i].SAVE_ID].TAG, (object)(Convert.ToUInt16(v)));
                        if (SAVE[PROC[_i].SAVE_ID].TYPE == "INT32") OpcDaTcpClient[srv_id].Write(SAVE[PROC[_i].SAVE_ID].TAG, (object)(Convert.ToInt32(v)));
                        if (SAVE[PROC[_i].SAVE_ID].TYPE == "UINT32") OpcDaTcpClient[srv_id].Write(SAVE[PROC[_i].SAVE_ID].TAG, (object)(Convert.ToUInt32(v)));
                        if (SAVE[PROC[_i].SAVE_ID].TYPE == "DOUBLE") OpcDaTcpClient[srv_id].Write(SAVE[PROC[_i].SAVE_ID].TAG, (object)(Convert.ToDouble(v)));
                        if (SAVE[PROC[_i].SAVE_ID].TYPE == "BOOL") OpcDaTcpClient[srv_id].Write(SAVE[PROC[_i].SAVE_ID].TAG, (object)(Convert.ToBoolean(v)));
                    }
                    if (PROC[_i].TYPE == "CONST")
                    {
                        if (SAVE[PROC[_i].SAVE_ID].TYPE == "BYTE") OpcDaTcpClient[srv_id].Write(SAVE[PROC[_i].SAVE_ID].TAG, (object)(Convert.ToByte(PROC[_i].VALUE)));
                        if (SAVE[PROC[_i].SAVE_ID].TYPE == "INT16") OpcDaTcpClient[srv_id].Write(SAVE[PROC[_i].SAVE_ID].TAG, (object)(Convert.ToInt16(PROC[_i].VALUE)));
                        if (SAVE[PROC[_i].SAVE_ID].TYPE == "UINT16") OpcDaTcpClient[srv_id].Write(SAVE[PROC[_i].SAVE_ID].TAG, (object)(Convert.ToUInt16(PROC[_i].VALUE)));
                        if (SAVE[PROC[_i].SAVE_ID].TYPE == "INT32") OpcDaTcpClient[srv_id].Write(SAVE[PROC[_i].SAVE_ID].TAG, (object)(Convert.ToInt32(PROC[_i].VALUE)));
                        if (SAVE[PROC[_i].SAVE_ID].TYPE == "UINT32") OpcDaTcpClient[srv_id].Write(SAVE[PROC[_i].SAVE_ID].TAG, (object)(Convert.ToUInt32(PROC[_i].VALUE)));
                        if (SAVE[PROC[_i].SAVE_ID].TYPE == "DOUBLE") OpcDaTcpClient[srv_id].Write(SAVE[PROC[_i].SAVE_ID].TAG, (object)(Convert.ToDouble(PROC[_i].VALUE)));
                        if (SAVE[PROC[_i].SAVE_ID].TYPE == "BOOL") OpcDaTcpClient[srv_id].Write(SAVE[PROC[_i].SAVE_ID].TAG, (object)(Convert.ToBoolean(PROC[_i].VALUE)));
                    }

                }
            }

            return ret;
        }//public bool writeOpcDa(int _i, string v)
    }
}
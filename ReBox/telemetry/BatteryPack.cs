// <auto-generated>
//     Generated by the protocol buffer compiler.  DO NOT EDIT!
//     source: telemetry/BatteryPack.proto
// </auto-generated>
#pragma warning disable 1591, 0612, 3021, 8981
#region Designer generated code

using pb = global::Google.Protobuf;
using pbc = global::Google.Protobuf.Collections;
using pbr = global::Google.Protobuf.Reflection;
using scg = global::System.Collections.Generic;
namespace BatteryPack {

  /// <summary>Holder for reflection information generated from telemetry/BatteryPack.proto</summary>
  public static partial class BatteryPackReflection {

    #region Descriptor
    /// <summary>File descriptor for telemetry/BatteryPack.proto</summary>
    public static pbr::FileDescriptor Descriptor {
      get { return descriptor; }
    }
    private static pbr::FileDescriptor descriptor;

    static BatteryPackReflection() {
      byte[] descriptorData = global::System.Convert.FromBase64String(
          string.Concat(
            "Cht0ZWxlbWV0cnkvQmF0dGVyeVBhY2sucHJvdG8SDGJhdHRlcnlfcGFjaxof",
            "Z29vZ2xlL3Byb3RvYnVmL3RpbWVzdGFtcC5wcm90byLqAQoLQmF0dGVyeVBh",
            "Y2sSHAoPc3RhdGVfb2ZfY2hhcmdlGAEgASgFSACIAQESEwoGY2hhcmdlGAIg",
            "ASgJSAGIAQESFgoJZGlzY2hhcmdlGAMgASgJSAKIAQESGAoLdGVtcGVyYXR1",
            "cmUYBCABKAlIA4gBARIUCgd2b2x0YWdlGAUgASgJSASIAQESFwoPcGFja19p",
            "ZGVudGlmaWVyGAYgASgJQhIKEF9zdGF0ZV9vZl9jaGFyZ2VCCQoHX2NoYXJn",
            "ZUIMCgpfZGlzY2hhcmdlQg4KDF90ZW1wZXJhdHVyZUIKCghfdm9sdGFnZUJ3",
            "Chtjb20uY29kaWJseS5tb2RlbC50ZWxlbWV0cnlCEEJhdHRlcnlQYWNrUHJv",
            "dG/KAhtEb21haW5Nb2RlbFxQcm90b1xUZWxlbWV0cnniAidEb21haW5Nb2Rl",
            "bFxQcm90b1xUZWxlbWV0cnlcR1BCTWV0YWRhdGFiBnByb3RvMw=="));
      descriptor = pbr::FileDescriptor.FromGeneratedCode(descriptorData,
          new pbr::FileDescriptor[] { global::Google.Protobuf.WellKnownTypes.TimestampReflection.Descriptor, },
          new pbr::GeneratedClrTypeInfo(null, null, new pbr::GeneratedClrTypeInfo[] {
            new pbr::GeneratedClrTypeInfo(typeof(global::BatteryPack.BatteryPack), global::BatteryPack.BatteryPack.Parser, new[]{ "StateOfCharge", "Charge", "Discharge", "Temperature", "Voltage", "PackIdentifier" }, new[]{ "StateOfCharge", "Charge", "Discharge", "Temperature", "Voltage" }, null, null, null)
          }));
    }
    #endregion

  }
  #region Messages
  public sealed partial class BatteryPack : pb::IMessage<BatteryPack>
  #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
      , pb::IBufferMessage
  #endif
  {
    private static readonly pb::MessageParser<BatteryPack> _parser = new pb::MessageParser<BatteryPack>(() => new BatteryPack());
    private pb::UnknownFieldSet _unknownFields;
    private int _hasBits0;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public static pb::MessageParser<BatteryPack> Parser { get { return _parser; } }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public static pbr::MessageDescriptor Descriptor {
      get { return global::BatteryPack.BatteryPackReflection.Descriptor.MessageTypes[0]; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    pbr::MessageDescriptor pb::IMessage.Descriptor {
      get { return Descriptor; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public BatteryPack() {
      OnConstruction();
    }

    partial void OnConstruction();

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public BatteryPack(BatteryPack other) : this() {
      _hasBits0 = other._hasBits0;
      stateOfCharge_ = other.stateOfCharge_;
      charge_ = other.charge_;
      discharge_ = other.discharge_;
      temperature_ = other.temperature_;
      voltage_ = other.voltage_;
      packIdentifier_ = other.packIdentifier_;
      _unknownFields = pb::UnknownFieldSet.Clone(other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public BatteryPack Clone() {
      return new BatteryPack(this);
    }

    /// <summary>Field number for the "state_of_charge" field.</summary>
    public const int StateOfChargeFieldNumber = 1;
    private int stateOfCharge_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public int StateOfCharge {
      get { if ((_hasBits0 & 1) != 0) { return stateOfCharge_; } else { return 0; } }
      set {
        _hasBits0 |= 1;
        stateOfCharge_ = value;
      }
    }
    /// <summary>Gets whether the "state_of_charge" field is set</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public bool HasStateOfCharge {
      get { return (_hasBits0 & 1) != 0; }
    }
    /// <summary>Clears the value of the "state_of_charge" field</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void ClearStateOfCharge() {
      _hasBits0 &= ~1;
    }

    /// <summary>Field number for the "charge" field.</summary>
    public const int ChargeFieldNumber = 2;
    private string charge_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public string Charge {
      get { return charge_ ?? ""; }
      set {
        charge_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
      }
    }
    /// <summary>Gets whether the "charge" field is set</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public bool HasCharge {
      get { return charge_ != null; }
    }
    /// <summary>Clears the value of the "charge" field</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void ClearCharge() {
      charge_ = null;
    }

    /// <summary>Field number for the "discharge" field.</summary>
    public const int DischargeFieldNumber = 3;
    private string discharge_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public string Discharge {
      get { return discharge_ ?? ""; }
      set {
        discharge_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
      }
    }
    /// <summary>Gets whether the "discharge" field is set</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public bool HasDischarge {
      get { return discharge_ != null; }
    }
    /// <summary>Clears the value of the "discharge" field</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void ClearDischarge() {
      discharge_ = null;
    }

    /// <summary>Field number for the "temperature" field.</summary>
    public const int TemperatureFieldNumber = 4;
    private string temperature_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public string Temperature {
      get { return temperature_ ?? ""; }
      set {
        temperature_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
      }
    }
    /// <summary>Gets whether the "temperature" field is set</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public bool HasTemperature {
      get { return temperature_ != null; }
    }
    /// <summary>Clears the value of the "temperature" field</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void ClearTemperature() {
      temperature_ = null;
    }

    /// <summary>Field number for the "voltage" field.</summary>
    public const int VoltageFieldNumber = 5;
    private string voltage_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public string Voltage {
      get { return voltage_ ?? ""; }
      set {
        voltage_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
      }
    }
    /// <summary>Gets whether the "voltage" field is set</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public bool HasVoltage {
      get { return voltage_ != null; }
    }
    /// <summary>Clears the value of the "voltage" field</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void ClearVoltage() {
      voltage_ = null;
    }

    /// <summary>Field number for the "pack_identifier" field.</summary>
    public const int PackIdentifierFieldNumber = 6;
    private string packIdentifier_ = "";
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public string PackIdentifier {
      get { return packIdentifier_; }
      set {
        packIdentifier_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public override bool Equals(object other) {
      return Equals(other as BatteryPack);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public bool Equals(BatteryPack other) {
      if (ReferenceEquals(other, null)) {
        return false;
      }
      if (ReferenceEquals(other, this)) {
        return true;
      }
      if (StateOfCharge != other.StateOfCharge) return false;
      if (Charge != other.Charge) return false;
      if (Discharge != other.Discharge) return false;
      if (Temperature != other.Temperature) return false;
      if (Voltage != other.Voltage) return false;
      if (PackIdentifier != other.PackIdentifier) return false;
      return Equals(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public override int GetHashCode() {
      int hash = 1;
      if (HasStateOfCharge) hash ^= StateOfCharge.GetHashCode();
      if (HasCharge) hash ^= Charge.GetHashCode();
      if (HasDischarge) hash ^= Discharge.GetHashCode();
      if (HasTemperature) hash ^= Temperature.GetHashCode();
      if (HasVoltage) hash ^= Voltage.GetHashCode();
      if (PackIdentifier.Length != 0) hash ^= PackIdentifier.GetHashCode();
      if (_unknownFields != null) {
        hash ^= _unknownFields.GetHashCode();
      }
      return hash;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public override string ToString() {
      return pb::JsonFormatter.ToDiagnosticString(this);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void WriteTo(pb::CodedOutputStream output) {
    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
      output.WriteRawMessage(this);
    #else
      if (HasStateOfCharge) {
        output.WriteRawTag(8);
        output.WriteInt32(StateOfCharge);
      }
      if (HasCharge) {
        output.WriteRawTag(18);
        output.WriteString(Charge);
      }
      if (HasDischarge) {
        output.WriteRawTag(26);
        output.WriteString(Discharge);
      }
      if (HasTemperature) {
        output.WriteRawTag(34);
        output.WriteString(Temperature);
      }
      if (HasVoltage) {
        output.WriteRawTag(42);
        output.WriteString(Voltage);
      }
      if (PackIdentifier.Length != 0) {
        output.WriteRawTag(50);
        output.WriteString(PackIdentifier);
      }
      if (_unknownFields != null) {
        _unknownFields.WriteTo(output);
      }
    #endif
    }

    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    void pb::IBufferMessage.InternalWriteTo(ref pb::WriteContext output) {
      if (HasStateOfCharge) {
        output.WriteRawTag(8);
        output.WriteInt32(StateOfCharge);
      }
      if (HasCharge) {
        output.WriteRawTag(18);
        output.WriteString(Charge);
      }
      if (HasDischarge) {
        output.WriteRawTag(26);
        output.WriteString(Discharge);
      }
      if (HasTemperature) {
        output.WriteRawTag(34);
        output.WriteString(Temperature);
      }
      if (HasVoltage) {
        output.WriteRawTag(42);
        output.WriteString(Voltage);
      }
      if (PackIdentifier.Length != 0) {
        output.WriteRawTag(50);
        output.WriteString(PackIdentifier);
      }
      if (_unknownFields != null) {
        _unknownFields.WriteTo(ref output);
      }
    }
    #endif

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public int CalculateSize() {
      int size = 0;
      if (HasStateOfCharge) {
        size += 1 + pb::CodedOutputStream.ComputeInt32Size(StateOfCharge);
      }
      if (HasCharge) {
        size += 1 + pb::CodedOutputStream.ComputeStringSize(Charge);
      }
      if (HasDischarge) {
        size += 1 + pb::CodedOutputStream.ComputeStringSize(Discharge);
      }
      if (HasTemperature) {
        size += 1 + pb::CodedOutputStream.ComputeStringSize(Temperature);
      }
      if (HasVoltage) {
        size += 1 + pb::CodedOutputStream.ComputeStringSize(Voltage);
      }
      if (PackIdentifier.Length != 0) {
        size += 1 + pb::CodedOutputStream.ComputeStringSize(PackIdentifier);
      }
      if (_unknownFields != null) {
        size += _unknownFields.CalculateSize();
      }
      return size;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void MergeFrom(BatteryPack other) {
      if (other == null) {
        return;
      }
      if (other.HasStateOfCharge) {
        StateOfCharge = other.StateOfCharge;
      }
      if (other.HasCharge) {
        Charge = other.Charge;
      }
      if (other.HasDischarge) {
        Discharge = other.Discharge;
      }
      if (other.HasTemperature) {
        Temperature = other.Temperature;
      }
      if (other.HasVoltage) {
        Voltage = other.Voltage;
      }
      if (other.PackIdentifier.Length != 0) {
        PackIdentifier = other.PackIdentifier;
      }
      _unknownFields = pb::UnknownFieldSet.MergeFrom(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void MergeFrom(pb::CodedInputStream input) {
    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
      input.ReadRawMessage(this);
    #else
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            _unknownFields = pb::UnknownFieldSet.MergeFieldFrom(_unknownFields, input);
            break;
          case 8: {
            StateOfCharge = input.ReadInt32();
            break;
          }
          case 18: {
            Charge = input.ReadString();
            break;
          }
          case 26: {
            Discharge = input.ReadString();
            break;
          }
          case 34: {
            Temperature = input.ReadString();
            break;
          }
          case 42: {
            Voltage = input.ReadString();
            break;
          }
          case 50: {
            PackIdentifier = input.ReadString();
            break;
          }
        }
      }
    #endif
    }

    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    void pb::IBufferMessage.InternalMergeFrom(ref pb::ParseContext input) {
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            _unknownFields = pb::UnknownFieldSet.MergeFieldFrom(_unknownFields, ref input);
            break;
          case 8: {
            StateOfCharge = input.ReadInt32();
            break;
          }
          case 18: {
            Charge = input.ReadString();
            break;
          }
          case 26: {
            Discharge = input.ReadString();
            break;
          }
          case 34: {
            Temperature = input.ReadString();
            break;
          }
          case 42: {
            Voltage = input.ReadString();
            break;
          }
          case 50: {
            PackIdentifier = input.ReadString();
            break;
          }
        }
      }
    }
    #endif

  }

  #endregion

}

#endregion Designer generated code

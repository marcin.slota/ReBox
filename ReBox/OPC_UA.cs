﻿using System;
using System.Xml;
using Opc.Ua;
using Opc.Ua.Client;

namespace ReBox
{
    partial class Rebox
    {
        public static void HelloOPCUA()
        {
            Console.WriteLine("Hello OPC-UA!");
        }
        public bool OpenOpcUaPortByProc(int _i)
        {
            // _i to nr PROC
            bool ret = false;

            return ret;
        }
        public bool OpenOpcUaPort(int _i)
        {
            // _i to nr MEAS
            bool ret = false;

            return ret;
        }

        public async Task<bool> readOpcUa(int _i)
        {
            bool ret = false;
            bool reconnect = false;


            if (OpcUaTcpClient[MEAS[_i].SERVER_ID] == null)
            {
                reconnect = true;
            }

            if (reconnect == true)
            {
                try
                {

                    // Tworzenie konfiguracji klienta
                    ApplicationConfiguration config = new ApplicationConfiguration();
                    config.ClientConfiguration = new ClientConfiguration(); // Dodanie konfiguracji klienta
                    config.SecurityConfiguration = new SecurityConfiguration();
                    config.SecurityConfiguration.ApplicationCertificate = new CertificateIdentifier();
                    config.CertificateValidator = new CertificateValidator();
                    config.CertificateValidator.CertificateValidation += (validator, e) => { e.Accept = true; };

                    // Tworzenie instancji klienta
                    EndpointDescription endpointDescription = new EndpointDescription(HW[SRV[MEAS[_i].SERVER_ID].HW_ID].URL);
                    UserIdentity user = new UserIdentity(SRV[MEAS[_i].SERVER_ID].USERNAME, SRV[MEAS[_i].SERVER_ID].PASSWORD);
                    ConfiguredEndpoint endpoint = new ConfiguredEndpoint(null, endpointDescription, EndpointConfiguration.Create(config));



                    if (SRV[MEAS[_i].SERVER_ID].USERNAME == "")
                    {
                        OpcUaTcpClient[MEAS[_i].SERVER_ID] = await Session.Create(config, endpoint, true, NAME, 60000, null, null);
                    }
                    else
                    {
                        OpcUaTcpClient[MEAS[_i].SERVER_ID] = await Session.Create(config, endpoint, true, NAME, 60000, user, null);
                    }


                    reconnect = false;
                }
                catch
                {

                }

            }
            if (reconnect == false)
            {
                if (MEAS[_i].OPERATION == "READ")
                {
                    try
                    {
                        NodeId nodeId = new NodeId(MEAS[_i].TAG);
                        var tmp_value = await OpcUaTcpClient[MEAS[_i].SERVER_ID].ReadValueAsync(nodeId);
                        var tmp_val = tmp_value.Value;
                        if (MEAS[_i].TYPE == "BYTE") MEAS[_i].uVALUE16[0] = Convert.ToByte(tmp_val);
                        else if (MEAS[_i].TYPE == "UINT16") MEAS[_i].uVALUE16[0] = Convert.ToUInt16(tmp_val);
                        else if (MEAS[_i].TYPE == "INT16") MEAS[_i].dVALUE16[0] = Convert.ToInt16(tmp_val);
                        else if (MEAS[_i].TYPE == "UINT32") MEAS[_i].uVALUE32[0] = Convert.ToUInt32(tmp_val);
                        else if (MEAS[_i].TYPE == "INT32") MEAS[_i].dVALUE32[0] = Convert.ToInt32(tmp_val);
                        else if (MEAS[_i].TYPE == "DOUBLE") MEAS[_i].fVALUE[0] = (double)tmp_val;
                        else if (MEAS[_i].TYPE == "BOOL") MEAS[_i].bVALUE[0] = (bool)tmp_val;
                        MEAS[_i].OK = 1;
                        HW[SRV[MEAS[_i].SERVER_ID].HW_ID].OK = 1;
                        SRV[MEAS[_i].SERVER_ID].OK = 1;
                        ret = true;
                    }
                    catch
                    {
                        MEAS[_i].OK = 0;
                        ret = false;
                        SRV[MEAS[_i].SERVER_ID].OK = 0;
                    }
                }
                OpcUaTcpClient[MEAS[_i].SERVER_ID].Close();
                OpcUaTcpClient[MEAS[_i].SERVER_ID] = null;
            }
            else
            {
                MEAS[_i].OK = 0;
                ret = false;
            }
            //close opc
            return ret;
        }//public async Task<bool> readOpcUa(int _i)

        public async Task<bool> writeOpcUa(int _i, string v)
        {
            //_i to nr procedury
            int hw_id = SRV[SAVE[PROC[_i].SAVE_ID].SERVER_ID].HW_ID;
            int srv_id = SAVE[PROC[_i].SAVE_ID].SERVER_ID;

            bool ret = false;
            bool reconnect = false;


            if (OpcUaTcpClient[MEAS[_i].SERVER_ID] == null)
            {
                reconnect = true;
            }

            if (reconnect == true)
            {
                try
                {

                    // Tworzenie konfiguracji klienta
                    ApplicationConfiguration config = new ApplicationConfiguration();
                    config.ClientConfiguration = new ClientConfiguration(); // Dodanie konfiguracji klienta
                    config.SecurityConfiguration = new SecurityConfiguration();
                    config.SecurityConfiguration.ApplicationCertificate = new CertificateIdentifier();
                    config.CertificateValidator = new CertificateValidator();
                    config.CertificateValidator.CertificateValidation += (validator, e) => { e.Accept = true; };

                    // Tworzenie instancji klienta
                    EndpointDescription endpointDescription = new EndpointDescription(HW[hw_id].URL);
                    UserIdentity user = new UserIdentity(SRV[srv_id].USERNAME, SRV[srv_id].PASSWORD);
                    ConfiguredEndpoint endpoint = new ConfiguredEndpoint(null, endpointDescription, EndpointConfiguration.Create(config));



                    if (SRV[srv_id].USERNAME == "")
                    {
                        OpcUaTcpClient[srv_id] = await Session.Create(config, endpoint, true, NAME, 60000, null, null);
                    }
                    else
                    {
                        OpcUaTcpClient[srv_id] = await Session.Create(config, endpoint, true, NAME, 60000, user, null);
                    }


                    reconnect = false;
                }
                catch
                {

                }

            }
            if (reconnect == false)
            {
                if (SAVE[PROC[_i].SAVE_ID].OPERATION == "WRITE")
                {

                    try
                    {

                        NodeId nodeId = new NodeId(SAVE[PROC[_i].SAVE_ID].TAG);

                        DataValue valueToWrite = new DataValue();
                        // Wartość, którą chcesz zapisać
                        if (PROC[_i].TYPE == "VARIABLE")
                        {
                            if (SAVE[PROC[_i].SAVE_ID].TYPE == "BYTE") valueToWrite = new DataValue(new Variant((byte)Convert.ToByte(v))); // Wartość 5 jako typ Byte
                            if (SAVE[PROC[_i].SAVE_ID].TYPE == "INT16") valueToWrite = new DataValue(new Variant((Int16)Convert.ToInt16(v))); // Wartość 5 jako typ Byte
                            if (SAVE[PROC[_i].SAVE_ID].TYPE == "UINT16") valueToWrite = new DataValue(new Variant((UInt16)Convert.ToUInt16(v))); // Wartość 5 jako typ Byte
                            if (SAVE[PROC[_i].SAVE_ID].TYPE == "INT32") valueToWrite = new DataValue(new Variant((Int32)Convert.ToInt32(v))); // Wartość 5 jako typ Byte
                            if (SAVE[PROC[_i].SAVE_ID].TYPE == "UINT32") valueToWrite = new DataValue(new Variant((UInt32)Convert.ToUInt32(v))); // Wartość 5 jako typ Byte
                            if (SAVE[PROC[_i].SAVE_ID].TYPE == "DOUBLE") valueToWrite = new DataValue(new Variant((Double)Convert.ToDouble(v))); // Wartość 5 jako typ Byte
                            if (SAVE[PROC[_i].SAVE_ID].TYPE == "BOOL") valueToWrite = new DataValue(new Variant((bool)Convert.ToBoolean(v))); // Wartość 5 jako typ Byte
                        }
                        if (PROC[_i].TYPE == "CONST")
                        {
                            if (SAVE[PROC[_i].SAVE_ID].TYPE == "BYTE") valueToWrite = new DataValue(new Variant((byte)Convert.ToByte(PROC[_i].VALUE))); // Wartość 5 jako typ Byte
                            if (SAVE[PROC[_i].SAVE_ID].TYPE == "INT16") valueToWrite = new DataValue(new Variant((Int16)Convert.ToInt16(PROC[_i].VALUE))); // Wartość 5 jako typ Byte
                            if (SAVE[PROC[_i].SAVE_ID].TYPE == "UINT16") valueToWrite = new DataValue(new Variant((UInt16)Convert.ToUInt16(PROC[_i].VALUE))); // Wartość 5 jako typ Byte
                            if (SAVE[PROC[_i].SAVE_ID].TYPE == "INT32") valueToWrite = new DataValue(new Variant((Int32)Convert.ToInt32(PROC[_i].VALUE))); // Wartość 5 jako typ Byte
                            if (SAVE[PROC[_i].SAVE_ID].TYPE == "UINT32") valueToWrite = new DataValue(new Variant((UInt32)Convert.ToUInt32(PROC[_i].VALUE))); // Wartość 5 jako typ Byte
                            if (SAVE[PROC[_i].SAVE_ID].TYPE == "DOUBLE") valueToWrite = new DataValue(new Variant((Double)Convert.ToDouble(PROC[_i].VALUE))); // Wartość 5 jako typ Byte
                            if (SAVE[PROC[_i].SAVE_ID].TYPE == "BOOL") valueToWrite = new DataValue(new Variant((bool)Convert.ToBoolean(PROC[_i].VALUE))); // Wartość 5 jako typ Byte
                        }


                        // Tworzenie obiektu WriteValue
                        Opc.Ua.WriteValue writeValue = new Opc.Ua.WriteValue
                        {
                            NodeId = nodeId,
                            AttributeId = Attributes.Value,
                            Value = valueToWrite
                        };

                        // Tworzenie obiektu WriteValueCollection i dodawanie WriteValue
                        WriteValueCollection nodesToWrite = new WriteValueCollection();
                        nodesToWrite.Add(writeValue);

                        // Tworzenie obiektów do przechowania statusu i informacji diagnostycznych
                        StatusCodeCollection statusCodes = null;
                        RequestHeader requestHeader = new RequestHeader();

                        DiagnosticInfoCollection diagnosticInfos = null;

                        // Wykonaj zapis
                        OpcUaTcpClient[srv_id].Write(requestHeader, nodesToWrite, out statusCodes, out diagnosticInfos);


                        HW[hw_id].OK = 1;
                        SRV[srv_id].OK = 1;

                        ret = true;
                    }
                    catch
                    {
                        //MEAS[_i].OK = 0;
                        ret = false;
                        SRV[srv_id].OK = 0;
                    }
                }

            }
            else
            {
                //MEAS[_i].OK = 0;
                ret = false;
            }
            //close opc
            return ret;
        }//public async Task<bool> writeOpcUa(int _i, string v)
    }
}
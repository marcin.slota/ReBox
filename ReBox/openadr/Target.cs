// <auto-generated>
//     Generated by the protocol buffer compiler.  DO NOT EDIT!
//     source: openadr/Target.proto
// </auto-generated>
#pragma warning disable 1591, 0612, 3021, 8981
#region Designer generated code

using pb = global::Google.Protobuf;
using pbc = global::Google.Protobuf.Collections;
using pbr = global::Google.Protobuf.Reflection;
using scg = global::System.Collections.Generic;
namespace Openadrevent {

  /// <summary>Holder for reflection information generated from openadr/Target.proto</summary>
  public static partial class TargetReflection {

    #region Descriptor
    /// <summary>File descriptor for openadr/Target.proto</summary>
    public static pbr::FileDescriptor Descriptor {
      get { return descriptor; }
    }
    private static pbr::FileDescriptor descriptor;

    static TargetReflection() {
      byte[] descriptorData = global::System.Convert.FromBase64String(
          string.Concat(
            "ChRvcGVuYWRyL1RhcmdldC5wcm90bxIMb3BlbmFkcmV2ZW50IucBCgZUYXJn",
            "ZXQSHQoVZW5kX2RldmljZV9hc3NldF9tcmlkGAEgAygJEhgKEG1ldGVyX2Fz",
            "c2V0X21yaWQYAiADKAkSGAoQYWdncmVnYXRlZF9wbm9kZRgDIAMoCRINCgVw",
            "bm9kZRgEIAMoCRIeChZzZXJ2aWNlX2RlbGl2ZXJ5X3BvaW50GAUgAygJEhAK",
            "CGdyb3VwX2lkGAYgAygJEhIKCmdyb3VwX25hbWUYByADKAkSEwoLcmVzb3Vy",
            "Y2VfaWQYCCADKAkSDgoGdmVuX2lkGAkgAygJEhAKCHBhcnR5X2lkGAogAygJ",
            "QmwKGWNvbS5jb2RpYmx5Lm1vZGVsLm9wZW5hZHJCC1RhcmdldFByb3RvygIZ",
            "RG9tYWluTW9kZWxcUHJvdG9cT3BlbmFkcuICJURvbWFpbk1vZGVsXFByb3Rv",
            "XE9wZW5hZHJcR1BCTWV0YWRhdGFiBnByb3RvMw=="));
      descriptor = pbr::FileDescriptor.FromGeneratedCode(descriptorData,
          new pbr::FileDescriptor[] { },
          new pbr::GeneratedClrTypeInfo(null, null, new pbr::GeneratedClrTypeInfo[] {
            new pbr::GeneratedClrTypeInfo(typeof(global::Openadrevent.Target), global::Openadrevent.Target.Parser, new[]{ "EndDeviceAssetMrid", "MeterAssetMrid", "AggregatedPnode", "Pnode", "ServiceDeliveryPoint", "GroupId", "GroupName", "ResourceId", "VenId", "PartyId" }, null, null, null, null)
          }));
    }
    #endregion

  }
  #region Messages
  public sealed partial class Target : pb::IMessage<Target>
  #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
      , pb::IBufferMessage
  #endif
  {
    private static readonly pb::MessageParser<Target> _parser = new pb::MessageParser<Target>(() => new Target());
    private pb::UnknownFieldSet _unknownFields;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public static pb::MessageParser<Target> Parser { get { return _parser; } }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public static pbr::MessageDescriptor Descriptor {
      get { return global::Openadrevent.TargetReflection.Descriptor.MessageTypes[0]; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    pbr::MessageDescriptor pb::IMessage.Descriptor {
      get { return Descriptor; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public Target() {
      OnConstruction();
    }

    partial void OnConstruction();

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public Target(Target other) : this() {
      endDeviceAssetMrid_ = other.endDeviceAssetMrid_.Clone();
      meterAssetMrid_ = other.meterAssetMrid_.Clone();
      aggregatedPnode_ = other.aggregatedPnode_.Clone();
      pnode_ = other.pnode_.Clone();
      serviceDeliveryPoint_ = other.serviceDeliveryPoint_.Clone();
      groupId_ = other.groupId_.Clone();
      groupName_ = other.groupName_.Clone();
      resourceId_ = other.resourceId_.Clone();
      venId_ = other.venId_.Clone();
      partyId_ = other.partyId_.Clone();
      _unknownFields = pb::UnknownFieldSet.Clone(other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public Target Clone() {
      return new Target(this);
    }

    /// <summary>Field number for the "end_device_asset_mrid" field.</summary>
    public const int EndDeviceAssetMridFieldNumber = 1;
    private static readonly pb::FieldCodec<string> _repeated_endDeviceAssetMrid_codec
        = pb::FieldCodec.ForString(10);
    private readonly pbc::RepeatedField<string> endDeviceAssetMrid_ = new pbc::RepeatedField<string>();
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public pbc::RepeatedField<string> EndDeviceAssetMrid {
      get { return endDeviceAssetMrid_; }
    }

    /// <summary>Field number for the "meter_asset_mrid" field.</summary>
    public const int MeterAssetMridFieldNumber = 2;
    private static readonly pb::FieldCodec<string> _repeated_meterAssetMrid_codec
        = pb::FieldCodec.ForString(18);
    private readonly pbc::RepeatedField<string> meterAssetMrid_ = new pbc::RepeatedField<string>();
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public pbc::RepeatedField<string> MeterAssetMrid {
      get { return meterAssetMrid_; }
    }

    /// <summary>Field number for the "aggregated_pnode" field.</summary>
    public const int AggregatedPnodeFieldNumber = 3;
    private static readonly pb::FieldCodec<string> _repeated_aggregatedPnode_codec
        = pb::FieldCodec.ForString(26);
    private readonly pbc::RepeatedField<string> aggregatedPnode_ = new pbc::RepeatedField<string>();
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public pbc::RepeatedField<string> AggregatedPnode {
      get { return aggregatedPnode_; }
    }

    /// <summary>Field number for the "pnode" field.</summary>
    public const int PnodeFieldNumber = 4;
    private static readonly pb::FieldCodec<string> _repeated_pnode_codec
        = pb::FieldCodec.ForString(34);
    private readonly pbc::RepeatedField<string> pnode_ = new pbc::RepeatedField<string>();
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public pbc::RepeatedField<string> Pnode {
      get { return pnode_; }
    }

    /// <summary>Field number for the "service_delivery_point" field.</summary>
    public const int ServiceDeliveryPointFieldNumber = 5;
    private static readonly pb::FieldCodec<string> _repeated_serviceDeliveryPoint_codec
        = pb::FieldCodec.ForString(42);
    private readonly pbc::RepeatedField<string> serviceDeliveryPoint_ = new pbc::RepeatedField<string>();
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public pbc::RepeatedField<string> ServiceDeliveryPoint {
      get { return serviceDeliveryPoint_; }
    }

    /// <summary>Field number for the "group_id" field.</summary>
    public const int GroupIdFieldNumber = 6;
    private static readonly pb::FieldCodec<string> _repeated_groupId_codec
        = pb::FieldCodec.ForString(50);
    private readonly pbc::RepeatedField<string> groupId_ = new pbc::RepeatedField<string>();
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public pbc::RepeatedField<string> GroupId {
      get { return groupId_; }
    }

    /// <summary>Field number for the "group_name" field.</summary>
    public const int GroupNameFieldNumber = 7;
    private static readonly pb::FieldCodec<string> _repeated_groupName_codec
        = pb::FieldCodec.ForString(58);
    private readonly pbc::RepeatedField<string> groupName_ = new pbc::RepeatedField<string>();
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public pbc::RepeatedField<string> GroupName {
      get { return groupName_; }
    }

    /// <summary>Field number for the "resource_id" field.</summary>
    public const int ResourceIdFieldNumber = 8;
    private static readonly pb::FieldCodec<string> _repeated_resourceId_codec
        = pb::FieldCodec.ForString(66);
    private readonly pbc::RepeatedField<string> resourceId_ = new pbc::RepeatedField<string>();
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public pbc::RepeatedField<string> ResourceId {
      get { return resourceId_; }
    }

    /// <summary>Field number for the "ven_id" field.</summary>
    public const int VenIdFieldNumber = 9;
    private static readonly pb::FieldCodec<string> _repeated_venId_codec
        = pb::FieldCodec.ForString(74);
    private readonly pbc::RepeatedField<string> venId_ = new pbc::RepeatedField<string>();
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public pbc::RepeatedField<string> VenId {
      get { return venId_; }
    }

    /// <summary>Field number for the "party_id" field.</summary>
    public const int PartyIdFieldNumber = 10;
    private static readonly pb::FieldCodec<string> _repeated_partyId_codec
        = pb::FieldCodec.ForString(82);
    private readonly pbc::RepeatedField<string> partyId_ = new pbc::RepeatedField<string>();
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public pbc::RepeatedField<string> PartyId {
      get { return partyId_; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public override bool Equals(object other) {
      return Equals(other as Target);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public bool Equals(Target other) {
      if (ReferenceEquals(other, null)) {
        return false;
      }
      if (ReferenceEquals(other, this)) {
        return true;
      }
      if(!endDeviceAssetMrid_.Equals(other.endDeviceAssetMrid_)) return false;
      if(!meterAssetMrid_.Equals(other.meterAssetMrid_)) return false;
      if(!aggregatedPnode_.Equals(other.aggregatedPnode_)) return false;
      if(!pnode_.Equals(other.pnode_)) return false;
      if(!serviceDeliveryPoint_.Equals(other.serviceDeliveryPoint_)) return false;
      if(!groupId_.Equals(other.groupId_)) return false;
      if(!groupName_.Equals(other.groupName_)) return false;
      if(!resourceId_.Equals(other.resourceId_)) return false;
      if(!venId_.Equals(other.venId_)) return false;
      if(!partyId_.Equals(other.partyId_)) return false;
      return Equals(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public override int GetHashCode() {
      int hash = 1;
      hash ^= endDeviceAssetMrid_.GetHashCode();
      hash ^= meterAssetMrid_.GetHashCode();
      hash ^= aggregatedPnode_.GetHashCode();
      hash ^= pnode_.GetHashCode();
      hash ^= serviceDeliveryPoint_.GetHashCode();
      hash ^= groupId_.GetHashCode();
      hash ^= groupName_.GetHashCode();
      hash ^= resourceId_.GetHashCode();
      hash ^= venId_.GetHashCode();
      hash ^= partyId_.GetHashCode();
      if (_unknownFields != null) {
        hash ^= _unknownFields.GetHashCode();
      }
      return hash;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public override string ToString() {
      return pb::JsonFormatter.ToDiagnosticString(this);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void WriteTo(pb::CodedOutputStream output) {
    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
      output.WriteRawMessage(this);
    #else
      endDeviceAssetMrid_.WriteTo(output, _repeated_endDeviceAssetMrid_codec);
      meterAssetMrid_.WriteTo(output, _repeated_meterAssetMrid_codec);
      aggregatedPnode_.WriteTo(output, _repeated_aggregatedPnode_codec);
      pnode_.WriteTo(output, _repeated_pnode_codec);
      serviceDeliveryPoint_.WriteTo(output, _repeated_serviceDeliveryPoint_codec);
      groupId_.WriteTo(output, _repeated_groupId_codec);
      groupName_.WriteTo(output, _repeated_groupName_codec);
      resourceId_.WriteTo(output, _repeated_resourceId_codec);
      venId_.WriteTo(output, _repeated_venId_codec);
      partyId_.WriteTo(output, _repeated_partyId_codec);
      if (_unknownFields != null) {
        _unknownFields.WriteTo(output);
      }
    #endif
    }

    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    void pb::IBufferMessage.InternalWriteTo(ref pb::WriteContext output) {
      endDeviceAssetMrid_.WriteTo(ref output, _repeated_endDeviceAssetMrid_codec);
      meterAssetMrid_.WriteTo(ref output, _repeated_meterAssetMrid_codec);
      aggregatedPnode_.WriteTo(ref output, _repeated_aggregatedPnode_codec);
      pnode_.WriteTo(ref output, _repeated_pnode_codec);
      serviceDeliveryPoint_.WriteTo(ref output, _repeated_serviceDeliveryPoint_codec);
      groupId_.WriteTo(ref output, _repeated_groupId_codec);
      groupName_.WriteTo(ref output, _repeated_groupName_codec);
      resourceId_.WriteTo(ref output, _repeated_resourceId_codec);
      venId_.WriteTo(ref output, _repeated_venId_codec);
      partyId_.WriteTo(ref output, _repeated_partyId_codec);
      if (_unknownFields != null) {
        _unknownFields.WriteTo(ref output);
      }
    }
    #endif

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public int CalculateSize() {
      int size = 0;
      size += endDeviceAssetMrid_.CalculateSize(_repeated_endDeviceAssetMrid_codec);
      size += meterAssetMrid_.CalculateSize(_repeated_meterAssetMrid_codec);
      size += aggregatedPnode_.CalculateSize(_repeated_aggregatedPnode_codec);
      size += pnode_.CalculateSize(_repeated_pnode_codec);
      size += serviceDeliveryPoint_.CalculateSize(_repeated_serviceDeliveryPoint_codec);
      size += groupId_.CalculateSize(_repeated_groupId_codec);
      size += groupName_.CalculateSize(_repeated_groupName_codec);
      size += resourceId_.CalculateSize(_repeated_resourceId_codec);
      size += venId_.CalculateSize(_repeated_venId_codec);
      size += partyId_.CalculateSize(_repeated_partyId_codec);
      if (_unknownFields != null) {
        size += _unknownFields.CalculateSize();
      }
      return size;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void MergeFrom(Target other) {
      if (other == null) {
        return;
      }
      endDeviceAssetMrid_.Add(other.endDeviceAssetMrid_);
      meterAssetMrid_.Add(other.meterAssetMrid_);
      aggregatedPnode_.Add(other.aggregatedPnode_);
      pnode_.Add(other.pnode_);
      serviceDeliveryPoint_.Add(other.serviceDeliveryPoint_);
      groupId_.Add(other.groupId_);
      groupName_.Add(other.groupName_);
      resourceId_.Add(other.resourceId_);
      venId_.Add(other.venId_);
      partyId_.Add(other.partyId_);
      _unknownFields = pb::UnknownFieldSet.MergeFrom(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void MergeFrom(pb::CodedInputStream input) {
    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
      input.ReadRawMessage(this);
    #else
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            _unknownFields = pb::UnknownFieldSet.MergeFieldFrom(_unknownFields, input);
            break;
          case 10: {
            endDeviceAssetMrid_.AddEntriesFrom(input, _repeated_endDeviceAssetMrid_codec);
            break;
          }
          case 18: {
            meterAssetMrid_.AddEntriesFrom(input, _repeated_meterAssetMrid_codec);
            break;
          }
          case 26: {
            aggregatedPnode_.AddEntriesFrom(input, _repeated_aggregatedPnode_codec);
            break;
          }
          case 34: {
            pnode_.AddEntriesFrom(input, _repeated_pnode_codec);
            break;
          }
          case 42: {
            serviceDeliveryPoint_.AddEntriesFrom(input, _repeated_serviceDeliveryPoint_codec);
            break;
          }
          case 50: {
            groupId_.AddEntriesFrom(input, _repeated_groupId_codec);
            break;
          }
          case 58: {
            groupName_.AddEntriesFrom(input, _repeated_groupName_codec);
            break;
          }
          case 66: {
            resourceId_.AddEntriesFrom(input, _repeated_resourceId_codec);
            break;
          }
          case 74: {
            venId_.AddEntriesFrom(input, _repeated_venId_codec);
            break;
          }
          case 82: {
            partyId_.AddEntriesFrom(input, _repeated_partyId_codec);
            break;
          }
        }
      }
    #endif
    }

    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    void pb::IBufferMessage.InternalMergeFrom(ref pb::ParseContext input) {
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            _unknownFields = pb::UnknownFieldSet.MergeFieldFrom(_unknownFields, ref input);
            break;
          case 10: {
            endDeviceAssetMrid_.AddEntriesFrom(ref input, _repeated_endDeviceAssetMrid_codec);
            break;
          }
          case 18: {
            meterAssetMrid_.AddEntriesFrom(ref input, _repeated_meterAssetMrid_codec);
            break;
          }
          case 26: {
            aggregatedPnode_.AddEntriesFrom(ref input, _repeated_aggregatedPnode_codec);
            break;
          }
          case 34: {
            pnode_.AddEntriesFrom(ref input, _repeated_pnode_codec);
            break;
          }
          case 42: {
            serviceDeliveryPoint_.AddEntriesFrom(ref input, _repeated_serviceDeliveryPoint_codec);
            break;
          }
          case 50: {
            groupId_.AddEntriesFrom(ref input, _repeated_groupId_codec);
            break;
          }
          case 58: {
            groupName_.AddEntriesFrom(ref input, _repeated_groupName_codec);
            break;
          }
          case 66: {
            resourceId_.AddEntriesFrom(ref input, _repeated_resourceId_codec);
            break;
          }
          case 74: {
            venId_.AddEntriesFrom(ref input, _repeated_venId_codec);
            break;
          }
          case 82: {
            partyId_.AddEntriesFrom(ref input, _repeated_partyId_codec);
            break;
          }
        }
      }
    }
    #endif

  }

  #endregion

}

#endregion Designer generated code

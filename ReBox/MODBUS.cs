﻿using System;
using NModbus;
using System.Net.Sockets;

namespace ReBox
{
    partial class Rebox
    {
        public static void HelloMODBUS()
        {
            Console.WriteLine("Hello MODBUS!");
        }
        public bool OpenModbusTcpPort(int _i)
        {
            // _i to nr MEAS
            bool ret = false;

            try
            {
                // Sprawdź, czy połączenie TCP jest aktywne
                ModbusTcpClient[SRV[MEAS[_i].SERVER_ID].HW_ID] = null;
                if (ModbusTcpClient[SRV[MEAS[_i].SERVER_ID].HW_ID] == null || !ModbusTcpClient[SRV[MEAS[_i].SERVER_ID].HW_ID].Connected)
                {
                    // Połączenie nie istnieje lub nie jest aktywne, więc utwórz nowe połączenie
                    ModbusTcpClient[SRV[MEAS[_i].SERVER_ID].HW_ID] = new TcpClient(HW[SRV[MEAS[_i].SERVER_ID].HW_ID].URL, Convert.ToInt16(HW[SRV[MEAS[_i].SERVER_ID].HW_ID].PORT));
                   
                }
                else
                {
                    // Połączenie jest aktywne
                    
                }

                // Utwórz factory, jeśli nie istnieje
                if (ModbusTcpFactory[MEAS[_i].SERVER_ID] == null)
                {
                    ModbusTcpFactory[MEAS[_i].SERVER_ID] = new ModbusFactory();
                }
                ModbusClient[MEAS[_i].SERVER_ID] = null;
                // Utwórz klienta Modbus, jeśli nie istnieje

                if (ModbusClient[MEAS[_i].SERVER_ID] == null)
                {
                    ModbusClient[MEAS[_i].SERVER_ID] = ModbusTcpFactory[MEAS[_i].SERVER_ID].CreateMaster(ModbusTcpClient[SRV[MEAS[_i].SERVER_ID].HW_ID]);
            
                }

                ret = true;
            }
            catch (Exception ex)
            {
                // Obsłuż błędy
                ret = false;
           
            }

            return ret;

        }
        public bool OpenModbusTcpPortByProc(int _i)
        {
            bool ret = false;
            // _i to nr PROC
            int hw_id = SRV[SAVE[PROC[_i].SAVE_ID].SERVER_ID].HW_ID;
            int srv_id = SAVE[PROC[_i].SAVE_ID].SERVER_ID;

            try
            {
                // Sprawdź, czy połączenie TCP jest aktywne
                ModbusTcpClient[hw_id] = null;
                if (ModbusTcpClient[SRV[MEAS[_i].SERVER_ID].HW_ID] == null || !ModbusTcpClient[hw_id].Connected)
                {
                    // Połączenie nie istnieje lub nie jest aktywne, więc utwórz nowe połączenie
                    ModbusTcpClient[hw_id] = new TcpClient(HW[hw_id].URL, Convert.ToInt16(HW[hw_id].PORT));
                    
                }
                else
                {
                    // Połączenie jest aktywne
                
                }

                // Utwórz factory, jeśli nie istnieje
                if (ModbusTcpFactory[srv_id] == null)
                {
                    ModbusTcpFactory[srv_id] = new ModbusFactory();
                }
                ModbusClient[srv_id] = null;
                // Utwórz klienta Modbus, jeśli nie istnieje

                if (ModbusClient[srv_id] == null)
                {
                    ModbusClient[srv_id] = ModbusTcpFactory[srv_id].CreateMaster(ModbusTcpClient[hw_id]);
                 
                }

                ret = true;
            }
            catch (Exception ex)
            {
                // Obsłuż błędy
                ret = false;

            }

            return ret;

        }
        public bool readModbusAscii(int _i)
        {
            bool ret = false;

            if (OpenSerialPort(SRV[MEAS[_i].SERVER_ID].HW_ID) == true)
            {
                if (MEAS[_i].OPERATION == "READ_HOLDING_REGISTERS")
                {
                    try
                    {
                        var value = ModbusASCIIClient[MEAS[_i].SERVER_ID].ReadHoldingRegisters((byte)SRV[MEAS[_i].SERVER_ID].SLAVE, Convert.ToUInt16(MEAS[_i].START), Convert.ToUInt16(MEAS[_i].COUNT));
                        if (MEAS[_i].TYPE == "UINT16")
                        {
                            MEAS[_i].uVALUE16 = value;
                        }
                        MEAS[_i].OK = 1;
                        HW[SRV[_i].HW_ID].OK = 1;
                        SRV[_i].OK = 1;
                        ret = true;
                    }
                    catch
                    {
                        MEAS[_i].OK = 0;
                        ret = false;
                        SRV[_i].OK = 0;
                 
                    }
                }

                if (MEAS[_i].OPERATION == "READ_INPUT_REGISTERS")
                {
                    try
                    {
                        var value = ModbusASCIIClient[MEAS[_i].SERVER_ID].ReadInputRegisters((byte)SRV[MEAS[_i].SERVER_ID].SLAVE, Convert.ToUInt16(MEAS[_i].START), Convert.ToUInt16(MEAS[_i].COUNT));
                        if (MEAS[_i].TYPE == "UINT16")
                        {
                            MEAS[_i].uVALUE16 = value;
                        }
                        MEAS[_i].OK = 1;
                        HW[SRV[_i].HW_ID].OK = 1;
                        SRV[_i].OK = 1;
                        ret = true;
                    }
                    catch
                    {
                        MEAS[_i].OK = 0;
                        ret = false;
                        SRV[_i].OK = 0;
              
                    }
                }

                if (MEAS[_i].OPERATION == "READ_COILS")
                {
                    try
                    {
                        var value = ModbusASCIIClient[MEAS[_i].SERVER_ID].ReadCoils((byte)SRV[MEAS[_i].SERVER_ID].SLAVE, Convert.ToUInt16(MEAS[_i].START), Convert.ToUInt16(MEAS[_i].COUNT));
                        if (MEAS[_i].TYPE == "BOOL")
                        {
                            MEAS[_i].bVALUE = value;
                        }
                        MEAS[_i].OK = 1;
                        HW[SRV[_i].HW_ID].OK = 1;
                        SRV[_i].OK = 1;
                        ret = true;
                    }
                    catch
                    {
                        MEAS[_i].OK = 0;
                        ret = false;
                        SRV[_i].OK = 0;
        
                    }
                }

                if (MEAS[_i].OPERATION == "READ_INPUTS")
                {
                    try
                    {
                        var value = ModbusASCIIClient[MEAS[_i].SERVER_ID].ReadInputs((byte)SRV[MEAS[_i].SERVER_ID].SLAVE, Convert.ToUInt16(MEAS[_i].START), Convert.ToUInt16(MEAS[_i].COUNT));
                        if (MEAS[_i].TYPE == "BOOL")
                        {
                            MEAS[_i].bVALUE = value;
                        }
                        MEAS[_i].OK = 1;
                        HW[SRV[_i].HW_ID].OK = 1;
                        SRV[_i].OK = 1;
                        ret = true;
                    }
                    catch
                    {
                        MEAS[_i].OK = 0;
                        ret = false;
                        SRV[_i].OK = 0;
                
                    }
                }
            }
            return ret;
        }
        public bool readModbusRtu(int _i)
        {
            bool ret = false;

            if (OpenSerialPort(SRV[MEAS[_i].SERVER_ID].HW_ID) == true)
            {
                if (MEAS[_i].OPERATION == "READ_HOLDING_REGISTERS")
                {
                    try
                    {
                        var value = ModbusRtuClient[MEAS[_i].SERVER_ID].ReadHoldingRegisters((byte)SRV[MEAS[_i].SERVER_ID].SLAVE, Convert.ToUInt16(MEAS[_i].START), Convert.ToUInt16(MEAS[_i].COUNT));
                        if (MEAS[_i].TYPE == "UINT16")
                        {
                            MEAS[_i].uVALUE16 = value;
                        }
                        MEAS[_i].OK = 1;
                        HW[SRV[_i].HW_ID].OK = 1;
                        SRV[_i].OK = 1;
                        ret = true;
                    }
                    catch
                    {
                        MEAS[_i].OK = 0;
                        ret = false;
                        SRV[_i].OK = 0;
                   
                    }
                }

                if (MEAS[_i].OPERATION == "READ_INPUT_REGISTERS")
                {
                    try
                    {
                        var value = ModbusRtuClient[MEAS[_i].SERVER_ID].ReadInputRegisters((byte)SRV[MEAS[_i].SERVER_ID].SLAVE, Convert.ToUInt16(MEAS[_i].START), Convert.ToUInt16(MEAS[_i].COUNT));
                        if (MEAS[_i].TYPE == "UINT16")
                        {
                            MEAS[_i].uVALUE16 = value;
                        }
                        MEAS[_i].OK = 1;
                        HW[SRV[_i].HW_ID].OK = 1;
                        SRV[_i].OK = 1;
                        ret = true;
                    }
                    catch
                    {
                        MEAS[_i].OK = 0;
                        ret = false;
                        SRV[_i].OK = 0;
               
                    }
                }

                if (MEAS[_i].OPERATION == "READ_COILS")
                {
                    try
                    {
                        var value = ModbusRtuClient[MEAS[_i].SERVER_ID].ReadCoils((byte)SRV[MEAS[_i].SERVER_ID].SLAVE, Convert.ToUInt16(MEAS[_i].START), Convert.ToUInt16(MEAS[_i].COUNT));
                        if (MEAS[_i].TYPE == "BOOL")
                        {
                            MEAS[_i].bVALUE = value;
                        }
                        MEAS[_i].OK = 1;
                        HW[SRV[_i].HW_ID].OK = 1;
                        SRV[_i].OK = 1;
                        ret = true;
                    }
                    catch
                    {
                        MEAS[_i].OK = 0;
                        ret = false;
                        SRV[_i].OK = 0;
                 
                    }
                }

                if (MEAS[_i].OPERATION == "READ_INPUTS")
                {
                    try
                    {
                        var value = ModbusRtuClient[MEAS[_i].SERVER_ID].ReadInputs((byte)SRV[MEAS[_i].SERVER_ID].SLAVE, Convert.ToUInt16(MEAS[_i].START), Convert.ToUInt16(MEAS[_i].COUNT));
                        if (MEAS[_i].TYPE == "BOOL")
                        {
                            MEAS[_i].bVALUE = value;
                        }
                        MEAS[_i].OK = 1;
                        HW[SRV[_i].HW_ID].OK = 1;
                        SRV[_i].OK = 1;
                        ret = true;
                    }
                    catch
                    {
                        MEAS[_i].OK = 0;
                        ret = false;
                        SRV[_i].OK = 0;
                 
                    }
                }
            }
            return ret;
        }



        public bool readModbusTcp(int _i)
        {
            bool ret = false;

            if (OpenModbusTcpPort(_i) == true)
            //if (reconnect == false)
            {
                if (MEAS[_i].OPERATION == "READ_REGISTERS")
                {
                    try
                    {
                        var value = ModbusClient[MEAS[_i].SERVER_ID].ReadHoldingRegisters(1, Convert.ToUInt16(MEAS[_i].START), Convert.ToUInt16(MEAS[_i].COUNT));
                        if (MEAS[_i].TYPE == "UINT16") MEAS[_i].uVALUE16 = value;



                        MEAS[_i].OK = 1;
                        HW[SRV[_i].HW_ID].OK = 1;
                        SRV[MEAS[_i].SERVER_ID].OK = 1;
                        ret = true;
                    }
                    catch
                    {
                        MEAS[_i].OK = 0;
                        ret = false;
                        SRV[MEAS[_i].SERVER_ID].OK = 0;
                
                    }

                }

                if (MEAS[_i].OPERATION == "READ_COILS")
                {
                    try
                    {
                        var value = ModbusClient[MEAS[_i].SERVER_ID].ReadCoils(1, Convert.ToUInt16(MEAS[_i].START), Convert.ToUInt16(MEAS[_i].COUNT));
                        if (MEAS[_i].TYPE == "BOOL") MEAS[_i].bVALUE = value;

                        MEAS[_i].OK = 1;
                        HW[SRV[_i].HW_ID].OK = 1;
                        SRV[MEAS[_i].SERVER_ID].OK = 1;
                        ret = true;
                    }
                    catch
                    {
                        MEAS[_i].OK = 0;
                        ret = false;
                        SRV[MEAS[_i].SERVER_ID].OK = 0;
              
                    }

                }

            }
            else
            {

                MEAS[_i].OK = 0;
       
                ret = false;
            }

            return ret;
        }


        public bool writeModbusTcp(int _i, string v)
        {
            //_i to nr procedury
            int hw_id = SRV[SAVE[PROC[_i].SAVE_ID].SERVER_ID].HW_ID;
            int srv_id = SAVE[PROC[_i].SAVE_ID].SERVER_ID;
            bool ret = false;
            if (OpenModbusTcpPortByProc(_i) == true)
            {
                if (SAVE[PROC[_i].SAVE_ID].OPERATION == "WRITE_REGISTER")
                {
                    //ModbusClient[MEAS[_i].SERVER_ID].ReadHoldingRegisters(1, Convert.ToUInt16(MEAS[_i].START), Convert.ToUInt16(MEAS[_i].COUNT));
                    if (PROC[_i].TYPE == "CONST")
                    {
                        ModbusClient[SAVE[PROC[_i].SAVE_ID].SERVER_ID].WriteSingleRegister(1, Convert.ToUInt16(SAVE[PROC[_i].SAVE_ID].ADDRESS), Convert.ToUInt16(PROC[_i].VALUE));
                    }
                    else
                    {
                        ModbusClient[SAVE[PROC[_i].SAVE_ID].SERVER_ID].WriteSingleRegister(1, Convert.ToUInt16(SAVE[PROC[_i].SAVE_ID].ADDRESS), Convert.ToUInt16(v));
                    }
                }
                if (SAVE[PROC[_i].SAVE_ID].OPERATION == "WRITE_COIL")
                {
                    //ModbusClient[MEAS[_i].SERVER_ID].ReadHoldingRegisters(1, Convert.ToUInt16(MEAS[_i].START), Convert.ToUInt16(MEAS[_i].COUNT));
                    if (PROC[_i].TYPE == "CONST")
                    {
                        ModbusClient[SAVE[PROC[_i].SAVE_ID].SERVER_ID].WriteSingleCoil(1, Convert.ToUInt16(SAVE[PROC[_i].SAVE_ID].ADDRESS), PROC[_i].VALUE == "TRUE");
                    }
                    else
                    {
                        ModbusClient[SAVE[PROC[_i].SAVE_ID].SERVER_ID].WriteSingleCoil(1, Convert.ToUInt16(SAVE[PROC[_i].SAVE_ID].ADDRESS), v == "TRUE");
                    }
                }

            }
            return ret;
        }//public bool writeModbusTcp(int _i, string v)

        public bool writeModbusRtu(int _i, string v)
        {
            //_i to nr procedury
            int hw_id = SRV[SAVE[PROC[_i].SAVE_ID].SERVER_ID].HW_ID;
            int srv_id = SAVE[PROC[_i].SAVE_ID].SERVER_ID;
            bool ret = false;
            if (OpenSerialPortByProc(_i) == true)
            {
                if (SAVE[PROC[_i].SAVE_ID].OPERATION == "WRITE_REGISTER")
                {
                    //ModbusClient[MEAS[_i].SERVER_ID].ReadHoldingRegisters(1, Convert.ToUInt16(MEAS[_i].START), Convert.ToUInt16(MEAS[_i].COUNT));
                    if (PROC[_i].TYPE == "CONST")
                    {
                        ModbusRtuClient[SAVE[PROC[_i].SAVE_ID].SERVER_ID].WriteSingleRegister(1, Convert.ToUInt16(SAVE[PROC[_i].SAVE_ID].ADDRESS), Convert.ToUInt16(PROC[_i].VALUE));
                    }
                    else
                    {
                        ModbusRtuClient[SAVE[PROC[_i].SAVE_ID].SERVER_ID].WriteSingleRegister(1, Convert.ToUInt16(SAVE[PROC[_i].SAVE_ID].ADDRESS), Convert.ToUInt16(v));
                    }
                }
                if (SAVE[PROC[_i].SAVE_ID].OPERATION == "WRITE_COIL")
                {
                    //ModbusClient[MEAS[_i].SERVER_ID].ReadHoldingRegisters(1, Convert.ToUInt16(MEAS[_i].START), Convert.ToUInt16(MEAS[_i].COUNT));
                    if (PROC[_i].TYPE == "CONST")
                    {
                        ModbusRtuClient[SAVE[PROC[_i].SAVE_ID].SERVER_ID].WriteSingleCoil(1, Convert.ToUInt16(SAVE[PROC[_i].SAVE_ID].ADDRESS), PROC[_i].VALUE == "TRUE");
                    }
                    else
                    {
                        ModbusRtuClient[SAVE[PROC[_i].SAVE_ID].SERVER_ID].WriteSingleCoil(1, Convert.ToUInt16(SAVE[PROC[_i].SAVE_ID].ADDRESS), v == "TRUE");
                    }
                }

            }
            return ret;
        }

        public bool writeModbusAscii(int _i, string v)
        {
            //_i to nr procedury
            int hw_id = SRV[SAVE[PROC[_i].SAVE_ID].SERVER_ID].HW_ID;
            int srv_id = SAVE[PROC[_i].SAVE_ID].SERVER_ID;
            bool ret = false;
            if (OpenSerialPortByProc(_i) == true)
            {
                if (SAVE[PROC[_i].SAVE_ID].OPERATION == "WRITE_REGISTER")
                {
                    //ModbusClient[MEAS[_i].SERVER_ID].ReadHoldingRegisters(1, Convert.ToUInt16(MEAS[_i].START), Convert.ToUInt16(MEAS[_i].COUNT));
                    if (PROC[_i].TYPE == "CONST")
                    {
                        ModbusASCIIClient[SAVE[PROC[_i].SAVE_ID].SERVER_ID].WriteSingleRegister(1, Convert.ToUInt16(SAVE[PROC[_i].SAVE_ID].ADDRESS), Convert.ToUInt16(PROC[_i].VALUE));
                    }
                    else
                    {
                        ModbusASCIIClient[SAVE[PROC[_i].SAVE_ID].SERVER_ID].WriteSingleRegister(1, Convert.ToUInt16(SAVE[PROC[_i].SAVE_ID].ADDRESS), Convert.ToUInt16(v));
                    }
                }
                if (SAVE[PROC[_i].SAVE_ID].OPERATION == "WRITE_COIL")
                {
                    //ModbusClient[MEAS[_i].SERVER_ID].ReadHoldingRegisters(1, Convert.ToUInt16(MEAS[_i].START), Convert.ToUInt16(MEAS[_i].COUNT));
                    if (PROC[_i].TYPE == "CONST")
                    {
                        ModbusASCIIClient[SAVE[PROC[_i].SAVE_ID].SERVER_ID].WriteSingleCoil(1, Convert.ToUInt16(SAVE[PROC[_i].SAVE_ID].ADDRESS), PROC[_i].VALUE == "TRUE");
                    }
                    else
                    {
                        ModbusASCIIClient[SAVE[PROC[_i].SAVE_ID].SERVER_ID].WriteSingleCoil(1, Convert.ToUInt16(SAVE[PROC[_i].SAVE_ID].ADDRESS), v == "TRUE");
                    }
                }

            }
            return ret;
        }
    }
}

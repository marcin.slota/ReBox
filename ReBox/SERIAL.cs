﻿using System;
using NModbus;
using NModbus.Serial;
using System.IO.Ports;

namespace ReBox
{
    partial class Rebox
    {
        public static void HelloSERIAL()
        {
            Console.WriteLine("Hello SERIAL!");
        }
        public bool OpenSerialPortByProc(int _i)
        {
            bool ret = false;
            //_i wskazuje na PROC
            int hw_id = SRV[SAVE[PROC[_i].SAVE_ID].SERVER_ID].HW_ID;
            int srv_id = SAVE[PROC[_i].SAVE_ID].SERVER_ID;
            if (serialPort[SRV[MEAS[_i].SERVER_ID].HW_ID] == null)
            {
                serialPort[hw_id] = new SerialPort();
                serialPort[hw_id].PortName = HW[hw_id].PORT;
                serialPort[hw_id].BaudRate = HW[hw_id].BAUD;
                serialPort[hw_id].Parity = HW[hw_id].PARITY;
                serialPort[hw_id].DataBits = HW[hw_id].DATABITS;
                serialPort[hw_id].StopBits = HW[hw_id].STOPBITS;
                serialPort[hw_id].Handshake = HW[hw_id].HANDSHAKING;
            }
            else
            {
                ret = true;
            }
            if (serialPort[hw_id].IsOpen == false)
            {
                try
                {
                    serialPort[hw_id].Open();
                   
                    ret = true;
                }
                catch
                {
                    ret = false;
                  
                }
            }
            else
            {
               
                ret = true;
            }
            if (ret == true)
            {
                try
                {

                    if (SRV[srv_id].TYPE == "MODBUSRTU")
                    {
                        if (ModbusRtuFactory[srv_id] == null)
                        {
                            ModbusRtuFactory[srv_id] = new ModbusFactory();
                        }
                        if (ModbusRtuClient[srv_id] == null)
                        {
                            ModbusRtuClient[srv_id] = ModbusRtuFactory[srv_id].CreateRtuMaster(serialPort[hw_id]);
                        }
                      

                    }
                    if (SRV[srv_id].TYPE == "MODBUSASCII")
                    {
                        if (ModbusASCIIFactory[srv_id] == null)
                        {
                            ModbusASCIIFactory[srv_id] = new ModbusFactory();
                        }
                        if (ModbusASCIIClient[srv_id] == null)
                        {

                            ModbusASCIIClient[srv_id] = ModbusASCIIFactory[srv_id].CreateAsciiMaster(serialPort[hw_id]);
                        }
                        
                    }
                }
                catch
                {
                   
                    ret = false;
                }
            }



            return ret;
        }//public bool OpenSerialPort(int _i)
        public bool OpenSerialPort(int _i)
        {
            bool ret = false;
            //_i wskazuje na MEAS
            if (serialPort[SRV[MEAS[_i].SERVER_ID].HW_ID] == null)
            {
                serialPort[SRV[MEAS[_i].SERVER_ID].HW_ID] = new SerialPort();
                serialPort[SRV[MEAS[_i].SERVER_ID].HW_ID].PortName = HW[SRV[MEAS[_i].SERVER_ID].HW_ID].PORT;
                serialPort[SRV[MEAS[_i].SERVER_ID].HW_ID].BaudRate = HW[SRV[MEAS[_i].SERVER_ID].HW_ID].BAUD;
                serialPort[SRV[MEAS[_i].SERVER_ID].HW_ID].Parity = HW[SRV[MEAS[_i].SERVER_ID].HW_ID].PARITY;
                serialPort[SRV[MEAS[_i].SERVER_ID].HW_ID].DataBits = HW[SRV[MEAS[_i].SERVER_ID].HW_ID].DATABITS;
                serialPort[SRV[MEAS[_i].SERVER_ID].HW_ID].StopBits = HW[SRV[MEAS[_i].SERVER_ID].HW_ID].STOPBITS;
                serialPort[SRV[MEAS[_i].SERVER_ID].HW_ID].Handshake = HW[SRV[MEAS[_i].SERVER_ID].HW_ID].HANDSHAKING;
            }
            else
            {
                ret = true;
            }
            if (serialPort[SRV[MEAS[_i].SERVER_ID].HW_ID].IsOpen == false)
            {
                try
                {
                    serialPort[SRV[MEAS[_i].SERVER_ID].HW_ID].Open();
              
                    ret = true;
                }
                catch
                {
                    ret = false;
                    
                }
            }
            else
            {
               
                ret = true;
            }
            if (ret == true)
            {
                try
                {

                    if (SRV[MEAS[_i].SERVER_ID].TYPE == "MODBUSRTU")
                    {
                        if (ModbusRtuFactory[MEAS[_i].SERVER_ID] == null)
                        {
                            ModbusRtuFactory[MEAS[_i].SERVER_ID] = new ModbusFactory();
                        }
                        if (ModbusRtuClient[MEAS[_i].SERVER_ID] == null)
                        {
                            ModbusRtuClient[MEAS[_i].SERVER_ID] = ModbusRtuFactory[MEAS[_i].SERVER_ID].CreateRtuMaster(serialPort[SRV[MEAS[_i].SERVER_ID].HW_ID]);
                        }
                      

                    }
                    if (SRV[MEAS[_i].SERVER_ID].TYPE == "MODBUSASCII")
                    {
                        if (ModbusASCIIFactory[MEAS[_i].SERVER_ID] == null)
                        {
                            ModbusASCIIFactory[MEAS[_i].SERVER_ID] = new ModbusFactory();
                        }
                        if (ModbusASCIIClient[MEAS[_i].SERVER_ID] == null)
                        {

                            ModbusASCIIClient[MEAS[_i].SERVER_ID] = ModbusASCIIFactory[MEAS[_i].SERVER_ID].CreateAsciiMaster(serialPort[SRV[MEAS[_i].SERVER_ID].HW_ID]);
                        }
                      
                    }
                }
                catch
                {
                   
                    ret = false;
                }
            }



            return ret;
        }//public bool OpenSerialPort(int _i)
    }
}

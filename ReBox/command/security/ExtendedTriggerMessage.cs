// <auto-generated>
//     Generated by the protocol buffer compiler.  DO NOT EDIT!
//     source: command/security/ExtendedTriggerMessage.proto
// </auto-generated>
#pragma warning disable 1591, 0612, 3021, 8981
#region Designer generated code

using pb = global::Google.Protobuf;
using pbc = global::Google.Protobuf.Collections;
using pbr = global::Google.Protobuf.Reflection;
using scg = global::System.Collections.Generic;
namespace ExtendedTriggerMessage {

  /// <summary>Holder for reflection information generated from command/security/ExtendedTriggerMessage.proto</summary>
  public static partial class ExtendedTriggerMessageReflection {

    #region Descriptor
    /// <summary>File descriptor for command/security/ExtendedTriggerMessage.proto</summary>
    public static pbr::FileDescriptor Descriptor {
      get { return descriptor; }
    }
    private static pbr::FileDescriptor descriptor;

    static ExtendedTriggerMessageReflection() {
      byte[] descriptorData = global::System.Convert.FromBase64String(
          string.Concat(
            "Ci1jb21tYW5kL3NlY3VyaXR5L0V4dGVuZGVkVHJpZ2dlck1lc3NhZ2UucHJv",
            "dG8SGGV4dGVuZGVkX3RyaWdnZXJfbWVzc2FnZSL6AgodRXh0ZW5kZWRUcmln",
            "Z2VyTWVzc2FnZUNvbW1hbmQSaQoRcmVxdWVzdGVkX21lc3NhZ2UYASABKA4y",
            "Ti5leHRlbmRlZF90cmlnZ2VyX21lc3NhZ2UuRXh0ZW5kZWRUcmlnZ2VyTWVz",
            "c2FnZUNvbW1hbmQuTWVzc2FnZVRyaWdnZXJFbnVtVHlwZRIZCgxjb25uZWN0",
            "b3JfaWQYAiABKAVIAIgBASLBAQoWTWVzc2FnZVRyaWdnZXJFbnVtVHlwZRIU",
            "ChBCb290Tm90aWZpY2F0aW9uEAASGQoVTG9nU3RhdHVzTm90aWZpY2F0aW9u",
            "EAESHgoaRmlybXdhcmVTdGF0dXNOb3RpZmljYXRpb24QAhINCglIZWFydGJl",
            "YXQQAxIPCgtNZXRlclZhbHVlcxAEEh4KGlNpZ25DaGFyZ2VQb2ludENlcnRp",
            "ZmljYXRlEAUSFgoSU3RhdHVzTm90aWZpY2F0aW9uEAZCDwoNX2Nvbm5lY3Rv",
            "cl9pZCLlAQolRXh0ZW5kZWRUcmlnZ2VyTWVzc2FnZUNvbW1hbmRSZXNwb25z",
            "ZRJsCgZzdGF0dXMYASABKA4yXC5leHRlbmRlZF90cmlnZ2VyX21lc3NhZ2Uu",
            "RXh0ZW5kZWRUcmlnZ2VyTWVzc2FnZUNvbW1hbmRSZXNwb25zZS5UcmlnZ2Vy",
            "TWVzc2FnZVN0YXR1c0VudW1UeXBlIk4KHFRyaWdnZXJNZXNzYWdlU3RhdHVz",
            "RW51bVR5cGUSDAoIQWNjZXB0ZWQQABIMCghSZWplY3RlZBABEhIKDk5vdElt",
            "cGxlbWVudGVkEAJClwEKImNvbS5jb2RpYmx5Lm1vZGVsLmNvbW1hbmQuc2Vj",
            "dXJpdHlCG0V4dGVuZGVkVHJpZ2dlck1lc3NhZ2VQcm90b8oCIkRvbWFpbk1v",
            "ZGVsXFByb3RvXENvbW1hbmRcU2VjdXJpdHniAi5Eb21haW5Nb2RlbFxQcm90",
            "b1xDb21tYW5kXFNlY3VyaXR5XEdQQk1ldGFkYXRhYgZwcm90bzM="));
      descriptor = pbr::FileDescriptor.FromGeneratedCode(descriptorData,
          new pbr::FileDescriptor[] { },
          new pbr::GeneratedClrTypeInfo(null, null, new pbr::GeneratedClrTypeInfo[] {
            new pbr::GeneratedClrTypeInfo(typeof(global::ExtendedTriggerMessage.ExtendedTriggerMessageCommand), global::ExtendedTriggerMessage.ExtendedTriggerMessageCommand.Parser, new[]{ "RequestedMessage", "ConnectorId" }, new[]{ "ConnectorId" }, new[]{ typeof(global::ExtendedTriggerMessage.ExtendedTriggerMessageCommand.Types.MessageTriggerEnumType) }, null, null),
            new pbr::GeneratedClrTypeInfo(typeof(global::ExtendedTriggerMessage.ExtendedTriggerMessageCommandResponse), global::ExtendedTriggerMessage.ExtendedTriggerMessageCommandResponse.Parser, new[]{ "Status" }, null, new[]{ typeof(global::ExtendedTriggerMessage.ExtendedTriggerMessageCommandResponse.Types.TriggerMessageStatusEnumType) }, null, null)
          }));
    }
    #endregion

  }
  #region Messages
  public sealed partial class ExtendedTriggerMessageCommand : pb::IMessage<ExtendedTriggerMessageCommand>
  #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
      , pb::IBufferMessage
  #endif
  {
    private static readonly pb::MessageParser<ExtendedTriggerMessageCommand> _parser = new pb::MessageParser<ExtendedTriggerMessageCommand>(() => new ExtendedTriggerMessageCommand());
    private pb::UnknownFieldSet _unknownFields;
    private int _hasBits0;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public static pb::MessageParser<ExtendedTriggerMessageCommand> Parser { get { return _parser; } }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public static pbr::MessageDescriptor Descriptor {
      get { return global::ExtendedTriggerMessage.ExtendedTriggerMessageReflection.Descriptor.MessageTypes[0]; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    pbr::MessageDescriptor pb::IMessage.Descriptor {
      get { return Descriptor; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public ExtendedTriggerMessageCommand() {
      OnConstruction();
    }

    partial void OnConstruction();

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public ExtendedTriggerMessageCommand(ExtendedTriggerMessageCommand other) : this() {
      _hasBits0 = other._hasBits0;
      requestedMessage_ = other.requestedMessage_;
      connectorId_ = other.connectorId_;
      _unknownFields = pb::UnknownFieldSet.Clone(other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public ExtendedTriggerMessageCommand Clone() {
      return new ExtendedTriggerMessageCommand(this);
    }

    /// <summary>Field number for the "requested_message" field.</summary>
    public const int RequestedMessageFieldNumber = 1;
    private global::ExtendedTriggerMessage.ExtendedTriggerMessageCommand.Types.MessageTriggerEnumType requestedMessage_ = global::ExtendedTriggerMessage.ExtendedTriggerMessageCommand.Types.MessageTriggerEnumType.BootNotification;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public global::ExtendedTriggerMessage.ExtendedTriggerMessageCommand.Types.MessageTriggerEnumType RequestedMessage {
      get { return requestedMessage_; }
      set {
        requestedMessage_ = value;
      }
    }

    /// <summary>Field number for the "connector_id" field.</summary>
    public const int ConnectorIdFieldNumber = 2;
    private int connectorId_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public int ConnectorId {
      get { if ((_hasBits0 & 1) != 0) { return connectorId_; } else { return 0; } }
      set {
        _hasBits0 |= 1;
        connectorId_ = value;
      }
    }
    /// <summary>Gets whether the "connector_id" field is set</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public bool HasConnectorId {
      get { return (_hasBits0 & 1) != 0; }
    }
    /// <summary>Clears the value of the "connector_id" field</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void ClearConnectorId() {
      _hasBits0 &= ~1;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public override bool Equals(object other) {
      return Equals(other as ExtendedTriggerMessageCommand);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public bool Equals(ExtendedTriggerMessageCommand other) {
      if (ReferenceEquals(other, null)) {
        return false;
      }
      if (ReferenceEquals(other, this)) {
        return true;
      }
      if (RequestedMessage != other.RequestedMessage) return false;
      if (ConnectorId != other.ConnectorId) return false;
      return Equals(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public override int GetHashCode() {
      int hash = 1;
      if (RequestedMessage != global::ExtendedTriggerMessage.ExtendedTriggerMessageCommand.Types.MessageTriggerEnumType.BootNotification) hash ^= RequestedMessage.GetHashCode();
      if (HasConnectorId) hash ^= ConnectorId.GetHashCode();
      if (_unknownFields != null) {
        hash ^= _unknownFields.GetHashCode();
      }
      return hash;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public override string ToString() {
      return pb::JsonFormatter.ToDiagnosticString(this);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void WriteTo(pb::CodedOutputStream output) {
    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
      output.WriteRawMessage(this);
    #else
      if (RequestedMessage != global::ExtendedTriggerMessage.ExtendedTriggerMessageCommand.Types.MessageTriggerEnumType.BootNotification) {
        output.WriteRawTag(8);
        output.WriteEnum((int) RequestedMessage);
      }
      if (HasConnectorId) {
        output.WriteRawTag(16);
        output.WriteInt32(ConnectorId);
      }
      if (_unknownFields != null) {
        _unknownFields.WriteTo(output);
      }
    #endif
    }

    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    void pb::IBufferMessage.InternalWriteTo(ref pb::WriteContext output) {
      if (RequestedMessage != global::ExtendedTriggerMessage.ExtendedTriggerMessageCommand.Types.MessageTriggerEnumType.BootNotification) {
        output.WriteRawTag(8);
        output.WriteEnum((int) RequestedMessage);
      }
      if (HasConnectorId) {
        output.WriteRawTag(16);
        output.WriteInt32(ConnectorId);
      }
      if (_unknownFields != null) {
        _unknownFields.WriteTo(ref output);
      }
    }
    #endif

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public int CalculateSize() {
      int size = 0;
      if (RequestedMessage != global::ExtendedTriggerMessage.ExtendedTriggerMessageCommand.Types.MessageTriggerEnumType.BootNotification) {
        size += 1 + pb::CodedOutputStream.ComputeEnumSize((int) RequestedMessage);
      }
      if (HasConnectorId) {
        size += 1 + pb::CodedOutputStream.ComputeInt32Size(ConnectorId);
      }
      if (_unknownFields != null) {
        size += _unknownFields.CalculateSize();
      }
      return size;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void MergeFrom(ExtendedTriggerMessageCommand other) {
      if (other == null) {
        return;
      }
      if (other.RequestedMessage != global::ExtendedTriggerMessage.ExtendedTriggerMessageCommand.Types.MessageTriggerEnumType.BootNotification) {
        RequestedMessage = other.RequestedMessage;
      }
      if (other.HasConnectorId) {
        ConnectorId = other.ConnectorId;
      }
      _unknownFields = pb::UnknownFieldSet.MergeFrom(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void MergeFrom(pb::CodedInputStream input) {
    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
      input.ReadRawMessage(this);
    #else
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            _unknownFields = pb::UnknownFieldSet.MergeFieldFrom(_unknownFields, input);
            break;
          case 8: {
            RequestedMessage = (global::ExtendedTriggerMessage.ExtendedTriggerMessageCommand.Types.MessageTriggerEnumType) input.ReadEnum();
            break;
          }
          case 16: {
            ConnectorId = input.ReadInt32();
            break;
          }
        }
      }
    #endif
    }

    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    void pb::IBufferMessage.InternalMergeFrom(ref pb::ParseContext input) {
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            _unknownFields = pb::UnknownFieldSet.MergeFieldFrom(_unknownFields, ref input);
            break;
          case 8: {
            RequestedMessage = (global::ExtendedTriggerMessage.ExtendedTriggerMessageCommand.Types.MessageTriggerEnumType) input.ReadEnum();
            break;
          }
          case 16: {
            ConnectorId = input.ReadInt32();
            break;
          }
        }
      }
    }
    #endif

    #region Nested types
    /// <summary>Container for nested types declared in the ExtendedTriggerMessageCommand message type.</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public static partial class Types {
      public enum MessageTriggerEnumType {
        [pbr::OriginalName("BootNotification")] BootNotification = 0,
        [pbr::OriginalName("LogStatusNotification")] LogStatusNotification = 1,
        [pbr::OriginalName("FirmwareStatusNotification")] FirmwareStatusNotification = 2,
        [pbr::OriginalName("Heartbeat")] Heartbeat = 3,
        [pbr::OriginalName("MeterValues")] MeterValues = 4,
        [pbr::OriginalName("SignChargePointCertificate")] SignChargePointCertificate = 5,
        [pbr::OriginalName("StatusNotification")] StatusNotification = 6,
      }

    }
    #endregion

  }

  public sealed partial class ExtendedTriggerMessageCommandResponse : pb::IMessage<ExtendedTriggerMessageCommandResponse>
  #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
      , pb::IBufferMessage
  #endif
  {
    private static readonly pb::MessageParser<ExtendedTriggerMessageCommandResponse> _parser = new pb::MessageParser<ExtendedTriggerMessageCommandResponse>(() => new ExtendedTriggerMessageCommandResponse());
    private pb::UnknownFieldSet _unknownFields;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public static pb::MessageParser<ExtendedTriggerMessageCommandResponse> Parser { get { return _parser; } }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public static pbr::MessageDescriptor Descriptor {
      get { return global::ExtendedTriggerMessage.ExtendedTriggerMessageReflection.Descriptor.MessageTypes[1]; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    pbr::MessageDescriptor pb::IMessage.Descriptor {
      get { return Descriptor; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public ExtendedTriggerMessageCommandResponse() {
      OnConstruction();
    }

    partial void OnConstruction();

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public ExtendedTriggerMessageCommandResponse(ExtendedTriggerMessageCommandResponse other) : this() {
      status_ = other.status_;
      _unknownFields = pb::UnknownFieldSet.Clone(other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public ExtendedTriggerMessageCommandResponse Clone() {
      return new ExtendedTriggerMessageCommandResponse(this);
    }

    /// <summary>Field number for the "status" field.</summary>
    public const int StatusFieldNumber = 1;
    private global::ExtendedTriggerMessage.ExtendedTriggerMessageCommandResponse.Types.TriggerMessageStatusEnumType status_ = global::ExtendedTriggerMessage.ExtendedTriggerMessageCommandResponse.Types.TriggerMessageStatusEnumType.Accepted;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public global::ExtendedTriggerMessage.ExtendedTriggerMessageCommandResponse.Types.TriggerMessageStatusEnumType Status {
      get { return status_; }
      set {
        status_ = value;
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public override bool Equals(object other) {
      return Equals(other as ExtendedTriggerMessageCommandResponse);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public bool Equals(ExtendedTriggerMessageCommandResponse other) {
      if (ReferenceEquals(other, null)) {
        return false;
      }
      if (ReferenceEquals(other, this)) {
        return true;
      }
      if (Status != other.Status) return false;
      return Equals(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public override int GetHashCode() {
      int hash = 1;
      if (Status != global::ExtendedTriggerMessage.ExtendedTriggerMessageCommandResponse.Types.TriggerMessageStatusEnumType.Accepted) hash ^= Status.GetHashCode();
      if (_unknownFields != null) {
        hash ^= _unknownFields.GetHashCode();
      }
      return hash;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public override string ToString() {
      return pb::JsonFormatter.ToDiagnosticString(this);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void WriteTo(pb::CodedOutputStream output) {
    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
      output.WriteRawMessage(this);
    #else
      if (Status != global::ExtendedTriggerMessage.ExtendedTriggerMessageCommandResponse.Types.TriggerMessageStatusEnumType.Accepted) {
        output.WriteRawTag(8);
        output.WriteEnum((int) Status);
      }
      if (_unknownFields != null) {
        _unknownFields.WriteTo(output);
      }
    #endif
    }

    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    void pb::IBufferMessage.InternalWriteTo(ref pb::WriteContext output) {
      if (Status != global::ExtendedTriggerMessage.ExtendedTriggerMessageCommandResponse.Types.TriggerMessageStatusEnumType.Accepted) {
        output.WriteRawTag(8);
        output.WriteEnum((int) Status);
      }
      if (_unknownFields != null) {
        _unknownFields.WriteTo(ref output);
      }
    }
    #endif

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public int CalculateSize() {
      int size = 0;
      if (Status != global::ExtendedTriggerMessage.ExtendedTriggerMessageCommandResponse.Types.TriggerMessageStatusEnumType.Accepted) {
        size += 1 + pb::CodedOutputStream.ComputeEnumSize((int) Status);
      }
      if (_unknownFields != null) {
        size += _unknownFields.CalculateSize();
      }
      return size;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void MergeFrom(ExtendedTriggerMessageCommandResponse other) {
      if (other == null) {
        return;
      }
      if (other.Status != global::ExtendedTriggerMessage.ExtendedTriggerMessageCommandResponse.Types.TriggerMessageStatusEnumType.Accepted) {
        Status = other.Status;
      }
      _unknownFields = pb::UnknownFieldSet.MergeFrom(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void MergeFrom(pb::CodedInputStream input) {
    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
      input.ReadRawMessage(this);
    #else
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            _unknownFields = pb::UnknownFieldSet.MergeFieldFrom(_unknownFields, input);
            break;
          case 8: {
            Status = (global::ExtendedTriggerMessage.ExtendedTriggerMessageCommandResponse.Types.TriggerMessageStatusEnumType) input.ReadEnum();
            break;
          }
        }
      }
    #endif
    }

    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    void pb::IBufferMessage.InternalMergeFrom(ref pb::ParseContext input) {
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            _unknownFields = pb::UnknownFieldSet.MergeFieldFrom(_unknownFields, ref input);
            break;
          case 8: {
            Status = (global::ExtendedTriggerMessage.ExtendedTriggerMessageCommandResponse.Types.TriggerMessageStatusEnumType) input.ReadEnum();
            break;
          }
        }
      }
    }
    #endif

    #region Nested types
    /// <summary>Container for nested types declared in the ExtendedTriggerMessageCommandResponse message type.</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public static partial class Types {
      public enum TriggerMessageStatusEnumType {
        [pbr::OriginalName("Accepted")] Accepted = 0,
        [pbr::OriginalName("Rejected")] Rejected = 1,
        [pbr::OriginalName("NotImplemented")] NotImplemented = 2,
      }

    }
    #endregion

  }

  #endregion

}

#endregion Designer generated code

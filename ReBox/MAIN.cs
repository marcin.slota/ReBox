﻿using System;
using System.IO;
using System.IO.Ports;
using System.Net;
using System.Net.Http;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Xml;





using Google.Protobuf;
using Command;

using Opc.Ua;
using Opc.Ua.Client;

using OPCDA;
using OPCDA.NET;


using NModbus;
using NModbus.IO;
using NModbus.Extensions.Enron;
using NModbus.Serial;

using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
namespace ReBox
{

    public enum ERROR_STATE
    {
        ERROR = 0,
        WARNINIG,
        INFO,
        CRITICAL,
        CLOSING,
        PROBLEMS
    };

    public struct SystemFile
    {
        public string filePath { get; set; }
        public string fileName { get; set; }
        public string fileRelease { get; set; }
        public string fileTimeStamp { get; set; }
        public string fileHardware { get; set; }
        public string fileHardware_Version { get; set; }
    }
    public struct HardwareInfo
    {
        public string NAME { get; set; }
        public string TYPE { get; set; }
        public string PORT { get; set; }
        public int BAUD { get; set; }
        public Parity PARITY { get; set; }
        public int DATABITS { get; set; }
        public StopBits STOPBITS { get; set; }
        public Handshake HANDSHAKING { get; set; }
        public string URL { get; set; }
        public int ON { get; set; }
        public int OK { get; set; }

        public HardwareInfo(string name = "", string type = "", string port = "", int baud = 2400, Parity parity = Parity.None, int databits = 8, StopBits stopbits = StopBits.One, Handshake handshaking = Handshake.None, string url = "", int on = 0, int ok = 0)
        {
            NAME = name;
            TYPE = type;
            PORT = port;
            BAUD = baud;
            PARITY = parity;
            DATABITS = databits;
            STOPBITS = stopbits;
            HANDSHAKING = handshaking;
            URL = url;
            OK = ok;
            ON = on;
        }
        public void Update(string name = "", string type = "", string port = "", int baud = 2400, Parity parity = Parity.None, int databits = 8, StopBits stopbits = StopBits.One, Handshake handshaking = Handshake.None, string url = "", int on = 0, int ok = 0)
        {
            NAME = name;
            TYPE = type;
            PORT = port;
            BAUD = baud;
            PARITY = parity;
            DATABITS = databits;
            STOPBITS = stopbits;
            HANDSHAKING = handshaking;
            URL = url;
            ON = on;
            OK = ok;
        }
    }

    public struct ServerInfo
    {
        public string NAME { get; set; }
        public string TYPE { get; set; }
        public int HW_ID { get; set; }
        public int SLAVE { get; set; }
        public int PORT { get; set; }
        public string USERNAME { get; set; }
        public string PASSWORD { get; set; }
        public string CERTIFICATE { get; set; }
        public int ON { get; set; }
        public int OK { get; set; }
        public ServerInfo(string name = "", string type = "", int hw_id = 0, int slave = 0, int port = 0, string username = "", string password = "", string certificate = "", int on = 0, int ok = 0)
        {
            NAME = name;
            TYPE = type;
            HW_ID = hw_id;
            SLAVE = slave;
            PORT = port;
            USERNAME = username;
            PASSWORD = password;
            CERTIFICATE = certificate;
            OK = ok;
            ON = on;
        }
        public void Update(string name = "", string type = "", int hw_id = 0, int slave = 0, int port = 0, string username = "", string password = "", string certificate = "", int on = 0, int ok = 0)
        {
            NAME = name;
            TYPE = type;
            HW_ID = hw_id;
            SLAVE = slave;
            PORT = port;
            USERNAME = username;
            PASSWORD = password;
            CERTIFICATE = certificate;
            ON = on;
            OK = ok;
        }

    }

    public struct MeasurementInfo
    {
        public string NAME { get; set; }
        public string TYPE { get; set; }
        public int SERVER_ID { get; set; }
        public string OPERATION { get; set; }
        public int START { get; set; }
        public int COUNT { get; set; }
        public string TAG { get; set; }
        public int ON { get; set; }
        public int OK { get; set; }
        public double[] fVALUE { get; set; }
        public UInt16[] uVALUE16 { get; set; }
        public Int16[] dVALUE16 { get; set; }
        public UInt32[] uVALUE32 { get; set; }
        public Int32[] dVALUE32 { get; set; }
        public bool[] bVALUE { get; set; }
        public string TIMESTAMP { get; set; }
        public MeasurementInfo(string name = "", string type = "", int server_id = 0, string operation = "", int start = 0, int count = 1, string tag = "", int on = 0, int ok = 0, bool bvalue = false, UInt16 uvalue16 = 0, Int16 dvalue16 = 0, UInt32 uvalue32 = 0, Int32 dvalue32 = 0, double fvalue = 0, string timestamp = "")
        {
            NAME = name;
            TYPE = type;
            SERVER_ID = server_id;
            OPERATION = operation;
            START = start;
            COUNT = count;
            TAG = tag;
            OK = ok;
            ON = on;
            fVALUE = new double[100];
            dVALUE16 = new short[100];
            uVALUE16 = new ushort[100];
            dVALUE32 = new int[100];
            uVALUE32 = new uint[100];
            bVALUE = new bool[100];
            TIMESTAMP = timestamp;
        }
        public void Update(string name = "", string type = "", int server_id = 0, string operation = "", int start = 0, int count = 1, string tag = "", int on = 0, int ok = 0, bool bvalue = false, UInt16 uvalue16 = 0, Int16 dvalue16 = 0, UInt32 uvalue32 = 0, Int32 dvalue32 = 0, double fvalue = 0, string timestamp = "")
        {
            NAME = name;
            TYPE = type;
            SERVER_ID = server_id;
            OPERATION = operation;
            START = start;
            COUNT = count;
            TAG = tag;
            ON = on;
            OK = ok;
            fVALUE = new double[100];
            dVALUE16 = new short[100];
            uVALUE16 = new ushort[100];
            dVALUE32 = new int[100];
            uVALUE32 = new uint[100];
            bVALUE = new bool[100];
            TIMESTAMP = timestamp;
        }
    }

    public struct DataInfo
    {
        public string NAME { get; set; }
        public string TYPE { get; set; }
        public int MEAS_ID { get; set; }
        public int POS { get; set; }
        public double MULT { get; set; }
        public double ADD { get; set; }
        public int DATA_ID { get; set; }
        public int DB { get; set; }
        public int ON { get; set; }
        public int OK { get; set; }
        public double fVALUE { get; set; }
        public UInt16 uVALUE16 { get; set; }
        public Int16 dVALUE16 { get; set; }
        public UInt32 uVALUE32 { get; set; }
        public Int32 dVALUE32 { get; set; }

        public bool bVALUE { get; set; }
        public string TIMESTAMP { get; set; }


        public DataInfo(string name = "", string type = "", int meas_id = 0, int pos = 0, double mult = 1, double add = 0, int data_id = 0, int db = 0, int on = 0, int ok = 0, bool bvalue = false, UInt16 uvalue16 = 0, Int16 dvalue16 = 0, UInt32 uvalue32 = 0, Int32 dvalue32 = 0, double fvalue = 0, string timestamp = "")
        {
            NAME = name;
            TYPE = type;
            MEAS_ID = meas_id;
            POS = pos;
            MULT = mult;
            ADD = add;
            DATA_ID = data_id;
            DB = db;
            OK = ok;
            ON = on;
            fVALUE = fvalue;
            dVALUE16 = dvalue16;
            uVALUE16 = uvalue16;
            dVALUE32 = dvalue32;
            uVALUE32 = uvalue32;
            bVALUE = bvalue;
            TIMESTAMP = timestamp;
        }
        public void Update(string name = "", string type = "", int meas_id = 0, int pos = 0, double mult = 1, double add = 0, int data_id = 0, int db = 0, int on = 0, int ok = 0, bool bvalue = false, UInt16 uvalue16 = 0, Int16 dvalue16 = 0, UInt32 uvalue32 = 0, Int32 dvalue32 = 0, double fvalue = 0, string timestamp = "")
        {
            NAME = name;
            TYPE = type;
            MEAS_ID = meas_id;
            POS = pos;
            MULT = mult;
            ADD = add;
            DATA_ID = data_id;
            DB = db;
            OK = ok;
            ON = on;
            fVALUE = fvalue;
            dVALUE16 = dvalue16;
            uVALUE16 = uvalue16;
            dVALUE32 = dvalue32;
            uVALUE32 = uvalue32;
            bVALUE = bvalue;
            TIMESTAMP = timestamp;
        }
    }
    public struct Payload
    {
        public string NAME { get; set; }
        public string TOPIC { get; set; }
        public string SAMPLETYPE { get; set; }
        public string PHASE { get; set; }
        public string SN { get; set; }


        public Payload(string name, string topic, string sampletype = "", string phase = "", string sn = "")
        {
            NAME = name;
            TOPIC = topic;
            SAMPLETYPE = sampletype;
            PHASE = phase;
            SN = sn;
        }
        public void Update(string name, string topic, string sampletype = "", string phase = "", string sn = "")
        {
            NAME = name;
            TOPIC = topic;
            PHASE = phase;
            SAMPLETYPE = sampletype;
            SN = sn;
        }
    }

    public struct Item
    {
        public string NAME { get; set; }
        public string OBJECT { get; set; }
        public string TYPE { get; set; }
        public int PAYLOAD_ID { get; set; }
        public int DATA_ID { get; set; }


        public Item(string name, string type, int payload_id = 0, int data_id = 0, string obj = "")
        {
            NAME = name;
            TYPE = type;
            PAYLOAD_ID = payload_id;
            DATA_ID = data_id;
            OBJECT = obj;
        }
        public void Update(string name, string type, int payload_id = 0, int data_id = 0, string obj = "")
        {
            NAME = name;
            TYPE = type;
            PAYLOAD_ID = payload_id;
            DATA_ID = data_id;
            OBJECT = obj;
        }
    }


    public struct DisplayInfo
    {
        public string NAME { get; set; }
        public string TYPE { get; set; }
        public int DATA_ID { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int NEG { get; set; }
        public int POINTER_ON { get; set; }
        public int POINTER_OFF { get; set; }
        public double MIN { get; set; }
        public double MAX { get; set; }
        public int RANGE { get; set; }
        public int ON { get; set; }
        public int OK { get; set; }
        public DisplayInfo(string name = "", string type = "", int data_id = 0, int x = 0, int y = 0, int neg = 0, int pointer_on = 0, int pointer_off = 0, int on = 0, int ok = 0)
        {
            NAME = name;
            TYPE = type;
            DATA_ID = data_id;
            X = x;
            Y = y;
            NEG = neg;
            POINTER_ON = pointer_on;
            POINTER_OFF = pointer_off;
            OK = ok;
            ON = on;
            MIN = 0;
            MAX = 0;
            RANGE = 0;

        }
        public void Update(string name = "", string type = "", int data_id = 0, int x = 0, int y = 0, int neg = 0, int pointer_on = 0, int pointer_off = 0, int on = 0, int ok = 0)
        {
            NAME = name;
            TYPE = type;
            DATA_ID = data_id;
            X = x;
            Y = y;
            NEG = neg;
            POINTER_ON = pointer_on;
            POINTER_OFF = pointer_off;
            ON = on;
            OK = ok;
        }
    }
    public struct Receiver
    {
        public string NAME { get; set; }
        public string SN { get; set; }
        public string TOPIC { get; set; }
        public string RESPONSE { get; set; }
        public int ON { get; set; }
        public int OK { get; set; }
        public Receiver(string name = "", string sn = "", string topic = "", string response = "", int ok = 0, int on = 0)
        {
            NAME = name;
            SN = sn;
            TOPIC = topic;
            RESPONSE = response;
            ON = on;
            OK = ok;
        }
        public void Update(string name = "", string sn = "", string topic = "", string response = "", int ok = 0, int on = 0)
        {
            NAME = name;
            SN = sn;
            TOPIC = topic;
            RESPONSE = response;
            ON = on;
            OK = ok;
        }
    }

    //COMMAND to kolejka polecen
    public struct Commands
    {
        public string NAME { get; set; }
        public string SN { get; set; }
        public string TIMESTAMP { get; set; }
        public int PROCEDURE_NUMBER { get; set; }
        public string VALUE { get; set; }
        public int ON { get; set; }
        public int OK { get; set; }
        public Commands(string name = "", string sn = "", string timestamp = "", int procedure_number = 0, string value = "", int ok = 0, int on = 0)
        {
            NAME = name;
            SN = sn;
            TIMESTAMP = timestamp;
            PROCEDURE_NUMBER = procedure_number;
            VALUE = value;
            ON = on;
            OK = ok;
        }
        public void Update(string name = "", string sn = "", string timestamp = "", int procedure_number = 0, string value = "", int ok = 0, int on = 0)
        {
            NAME = name;
            SN = sn;
            TIMESTAMP = timestamp;
            PROCEDURE_NUMBER = procedure_number;
            VALUE = value;
            ON = on;
            OK = ok;
        }

    }

    public struct Proc
    {
        public string NAME { get; set; }
        public int PROCEDURE_NUMBER { get; set; }
        public int SAVE_ID { get; set; }
        public string TYPE { get; set; }
        public string VALUE { get; set; }
        public int ON { get; set; }
        public int OK { get; set; }
        public Proc(string name = "", int procedure_number = 0, int save_id = 0, string type = "", string value = "", int ok = 0, int on = 0)
        {
            NAME = name;
            PROCEDURE_NUMBER = procedure_number;
            SAVE_ID = save_id;
            TYPE = type;
            VALUE = value;
            ON = on;
            OK = ok;
        }
        public void Update(string name = "", int procedure_number = 0, int save_id = 0, string type = "", string value = "", int ok = 0, int on = 0)
        {
            NAME = name;
            PROCEDURE_NUMBER = procedure_number;
            SAVE_ID = save_id;
            TYPE = type;
            VALUE = value;
            ON = on;
            OK = ok;
        }
    }

    public struct Save
    {
        public string NAME { get; set; }
        public string TYPE { get; set; }
        public int PROCEDURE_NUMBER { get; set; }
        public int SERVER_ID { get; set; }
        public string OPERATION { get; set; }
        public string TAG { get; set; }
        public string ADDRESS { get; set; }
        public int ON { get; set; }
        public int OK { get; set; }
        public Save(string name = "", string type = "", int procedure_number = 0, int server_id = 0, string operation = "", string tag = "", string address = "", int ok = 0, int on = 0)
        {
            NAME = name;
            TYPE = type;
            PROCEDURE_NUMBER = procedure_number;
            SERVER_ID = server_id;
            OPERATION = operation;
            TAG = tag;
            ADDRESS = address;
            ON = on;
            OK = ok;
        }
        public void Update(string name = "", string type = "", int procedure_number = 0, int server_id = 0, string operation = "", string tag = "", string address = "", int ok = 0, int on = 0)
        {
            NAME = name;
            TYPE = type;
            PROCEDURE_NUMBER = procedure_number;
            SERVER_ID = server_id;
            OPERATION = operation;
            TAG = tag;
            ADDRESS = address;
            ON = on;
            OK = ok;
        }
    }
    partial class Rebox
    {
        public Int32 INTERVAL = 1000;

        public const int HW_MAX = 250;
        public int HW_COUNT = 0;
        public HardwareInfo[] HW = new HardwareInfo[HW_MAX];

        public const int SRV_MAX = 250;
        public int SRV_COUNT = 0;
        public ServerInfo[] SRV = new ServerInfo[SRV_MAX];

        public const int MEAS_MAX = 250;
        public int MEAS_COUNT = 0;
        public int MEAS_POS = 1;
        public MeasurementInfo[] MEAS = new MeasurementInfo[MEAS_MAX];

        public const int DATA_MAX = 250;
        public int DATA_COUNT = 0;
        public DataInfo[] DATA = new DataInfo[DATA_MAX];

        public const int DISP_MAX = 250;
        public int DISP_COUNT = 0;
        public DisplayInfo[] DISP = new DisplayInfo[DISP_MAX];

        public const int PAYLOAD_MAX = 250;
        public int PAYLOAD_COUNT = 0;
        public Payload[] PAYLOAD = new Payload[PAYLOAD_MAX];

        public const int ITEM_MAX = 250;
        public int ITEM_COUNT = 0;
        public Item[] ITEM = new Item[ITEM_MAX];

        public const int COMMAND_MAX = 32;
        public int COMMAND_COUNT = 0;
        public Commands[] COMMAND = new Commands[COMMAND_MAX];

        public const int RECEIVER_MAX = 32;
        public int RECEIVER_COUNT = 0;
        public Receiver[] RECEIVER = new Receiver[RECEIVER_MAX];

        string[] subscriptionTopics = new string[] { };
        static MqttClient clientMqttSubscription;

        public const int PROC_MAX = 32;
        public int PROC_COUNT = 0;
        public Proc[] PROC = new Proc[PROC_MAX];

        public const int SAVE_MAX = 32;
        public int SAVE_COUNT = 0;
        public Save[] SAVE = new Save[SAVE_MAX];

        SystemFile Sysfile_SYSTEM;
        SystemFile Sysfile_HARDWARE;
        SystemFile Sysfile_MQTT_PUBLISHER;
        SystemFile Sysfile_MQTT_SUBSCRIBER;
        SystemFile Sysfile_CONFIGURATION;
        SystemFile Sysfile_DATABASE;
        SystemFile Sysfile_MEASUREMENT;
        SystemFile Sysfile_SCADA;
        public string Sysfile_FOLDER = "";

        public string SOFTWARE = "";
        public string SOFTWARE_VERSION = "";
        public string FILE_SYSTEM = "./CONFIG/SYSTEM.xml";
        public string FILE_HARDWARE = "./CONFIG/HARDWARE.xml";

        public string SERIALNUMBER = "12340";
        public string HARDWARE = "";
        public string HARDWARE_VERSION = "";
        public string NAME = "";

        public string FILE_XML = "";
        public string FILE_XML_NAME = "";
        public string FILE_LOG = "";
        public string FILE_LOG_NAME = "";
        public string FILE_BACKUP = "";
        public string FILE_BACKUP_NAME = "";
        public string FILE_OFFLINE_DB = "";
        public string FILE_OFFLINE_DB_NAME = "";
        public string FILE_OFFLINE_MQTT = "";
        public string FILE_OFFLINE_MQTT_NAME = "";
        public string FILE_FOLDER = "";
        public string FILE_FOLDER_NAME = "";

        public string SERVER_DB = "";
        public string SERVER_DB_NAME = "";
        public string SERVER_MQTT = "";
        public string SERVER_MQTT_NAME = "";
        public string SERVER_PING = "";
        public string SERVER_PING_NAME = "";
        public string SERVER_PING_URL = "";
        public int SERVER_PING_TIMEOUT = 0;

        public string MQTT_NAME = "";
        public string MQTT_URL = "";
        public string MQTT_PORT = "";
        public string MQTT_TOPIC = "";
        public string MQTT_USERNAME = "";
        public string MQTT_PASSWORD = "";


        public ERROR_STATE DEBUG_LEVEL = 0;
        public string DEBUG_LEVEL_NAME = "";
        public bool shouldun = true;
        public bool run = true;
        public int toolsTimeout = 0;

        public bool DB_offline = false;
        public bool MQTT_offline = false;
        public bool status_file_offlineDB = true;
        public bool status_file_offlineMQTT = true;
        public bool status_file_log = true;
        public bool status_file_backup = true;

        SerialPort[] serialPort = new SerialPort[HW_MAX];
        ModbusFactory[] ModbusRtuFactory = new ModbusFactory[SRV_MAX];
        ModbusFactory[] ModbusASCIIFactory = new ModbusFactory[SRV_MAX];
        IModbusMaster[] ModbusRtuClient = new IModbusMaster[SRV_MAX];
        IModbusMaster[] ModbusASCIIClient = new IModbusMaster[SRV_MAX];



        TcpClient[] ModbusTcpClient = new TcpClient[HW_MAX];
        ModbusFactory[] ModbusTcpFactory = new ModbusFactory[SRV_MAX];
        IModbusMaster[] ModbusClient = new IModbusMaster[SRV_MAX];


        OpcServer[] OpcDaSrv = new OpcServer[SRV_MAX];
        SyncIOGroup[] OpcDaTcpClient = new SyncIOGroup[SRV_MAX];

        public Session[] OpcUaTcpClient = new Session[SRV_MAX]; // OPC-UA

        MqttClient MqttClient;

       

        public  void HelloMAIN()
        {
            Console.WriteLine("Hello MAIN!");
            Console.ReadLine();
        }

        public async void mainloop()
        {

            //Init
            //HelloMAIN();
            readSysfile_SYSTEM();
            readSysfile_HARDWARE();
            readSysfile_CONFIGURATION();
            readSysfile_DATABASE();
            readSysfile_MQTT_PUBLISHER();
            readSysfile_MEASUREMENT();
            readSysfile_SCADA();
            readSysfile_MQTT_SUBSCRIBER();
            Console.WriteLine("Odczytano konfiguracje");

            //ustawienie topików do odczytu poleceń
            for (int r = 1; r < RECEIVER_MAX; r++)
            {
                if (RECEIVER[r].ON == 1)
                {
                    Array.Resize(ref subscriptionTopics, subscriptionTopics.Length + 1);
                    subscriptionTopics[subscriptionTopics.Length - 1] = $"{MQTT_TOPIC}{RECEIVER[r].SN}{RECEIVER[r].TOPIC}";
                }
                Array.Resize(ref subscriptionTopics, subscriptionTopics.Length + 1);
            }


            try
            {
                if (clientMqttSubscription == null)
                    clientMqttSubscription = new MqttClient(MQTT_URL, Convert.ToInt16(MQTT_PORT), false, null, null, MqttSslProtocols.None);

                // Wla
                clientMqttSubscription.MqttMsgPublishReceived += Client_MqttMsgPublishReceived;
                clientMqttSubscription.ConnectionClosed += Client_ConnectionClosed;


                // Połączenie z brokerem MQTT
                if (clientMqttSubscription.IsConnected == false)
                    clientMqttSubscription.Connect(SERIALNUMBER + "1", MQTT_USERNAME, MQTT_PASSWORD);

                if (clientMqttSubscription.IsConnected)
                {


                    foreach (string topic in subscriptionTopics)
                    {
                        if (topic != null)
                            clientMqttSubscription.Subscribe(new string[] { topic }, new byte[] { MqttMsgBase.QOS_LEVEL_AT_LEAST_ONCE });
                        //Console.WriteLine($"Subskrybowano temat '{topic}'");
                    }

                }
                else
                {
                    //Console.WriteLine("Nie udało się połączyć z brokerem MQTT.");
                }
            }
            catch (Exception ex)
            {
                //Console.WriteLine($"Wystąpił błąd: {ex.Message}");
            }


            bool isInternetAvailable = CheckInternet();
            bool isInternetPreviouslyAvailable = !isInternetAvailable;
            string A = GetTimeStamp();
            DateTimeOffset timeA = DateTimeOffset.ParseExact(A, "yyyy-MM-dd HH:mm:ss.fff", null);

            //main loop
            while (shouldun)
            {
                Console.WriteLine("Loop");
                isInternetAvailable = CheckInternet();
                if (isInternetAvailable != isInternetPreviouslyAvailable)
                {
                    isInternetPreviouslyAvailable = isInternetAvailable;

                    if (isInternetAvailable)
                    {
                        sendToLog(ERROR_STATE.INFO, "INTERNET OK");
                    }
                    else
                    {
                        sendToLog(ERROR_STATE.ERROR, "INTERNET ERROR");
                    }
                }

                //Odczyt z plikow OFFLINE
                sendToDBfromOffline();
                sendToMQTTfromOffline();


                //ODCZYTY Z URZĄDZEŃ

                if (MEAS[MEAS_POS].ON == 1)
                {

                    readMeas(MEAS_POS);

                }//if (MEAS[MEAS_POS].ON == 1)

                if (MEAS_POS++ >= MEAS_COUNT) MEAS_POS = 1;

                //GDY WSZYSTKIE ODCZYTY W SERII SĄ ZROBIONE
                if (MEAS_POS == 1)
                {
                    //tutaj wchodzi po wszystkich odczytach

                    for (int i = 1; i < DATA_COUNT; i++)
                    {
                        //ANALIZA OCZYTANYCH DANYCH
                        analyzeData(i);

                        //WYSLANIE DO BAZY DANYCH
                        if (DATA[i].DB == 1 && DATA[i].ON == 1)
                        {
                            if (DB_offline == false)
                            {
                                if (await sendToDB(i) == true)
                                {
                                    var data = readData(i);
                                    Console.WriteLine($"{DATA[i].NAME} {data}");
                                    DB_offline = false;
                                    sendToBackup1(i);
                                }
                                else
                                {
                                    DB_offline = true;
                                    sendToOfflineDB(i);
                                }
                            }
                            else
                            {
                                sendToOfflineDB(i);
                            }

                        }
                        if (DB_offline == false && MQTT_offline == false)
                        {
                            if (shouldun == true) ;
                              
                        }
                        else
                        {
                          
                        }

                        //wyslanie do MQTT

                    }//for (int i = 1; i<DATA_COUNT; i++)

                    for (int i = 1; i < PAYLOAD_COUNT; i++)
                    {
                        if (PAYLOAD[i].NAME != null)
                            sendMessage(i); //tu jest tez zapis do offline

                    }


                    //Odczyt kolejki polecen
                    for (int k = 0; k < COMMAND_MAX; k++)
                    {
                        runFromQueue(k);
                    }
                    string B = GetTimeStamp();
                    DateTimeOffset timeB = DateTimeOffset.ParseExact(B, "yyyy-MM-dd HH:mm:ss.fff", null);
                    TimeSpan difference = timeB - timeA;
                    double milliseconds = difference.TotalMilliseconds;

                    while (milliseconds < INTERVAL)
                    {
                        B = GetTimeStamp();
                        timeB = DateTimeOffset.ParseExact(B, "yyyy-MM-dd HH:mm:ss.fff", null);
                        difference = timeB - timeA;
                        milliseconds = difference.TotalMilliseconds;
                        if (shouldun == false) milliseconds = 2 * INTERVAL;
                    }
                    A = GetTimeStamp();
                    timeA = DateTimeOffset.ParseExact(A, "yyyy-MM-dd HH:mm:ss.fff", null);


                }//if(MEAS_POS == 1)



            }

        }


        // ==================================================================================================================================



       
    }
}
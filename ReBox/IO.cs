﻿using System;
using System.Diagnostics;
using System.IO.Ports;
using System.Net.Http;
using System.Net.NetworkInformation;
using System.Text;
using System.Xml;
using System.Net.Http;

namespace ReBox
{
    partial class Rebox
    {
        public static void HelloIO()
        {
            Console.WriteLine("Hello IO!");
        }

        public bool sendToOfflineDB(int _i)
        {

            bool ret = false;
            Int32 valueInt = 0;
            double valueDouble = 0;
            switch (DATA[_i].TYPE)
            {
                case "BOOL":
                    if (DATA[_i].bVALUE == true) { valueInt = 1; } else { valueInt = 0; };
                    break;
                case "UINT16":
                    valueInt = (int)DATA[_i].uVALUE16;
                    break;
                case "INT16":
                    valueInt = (int)DATA[_i].dVALUE16;
                    break;
                case "UINT32":
                    valueInt = (int)DATA[_i].uVALUE32;
                    break;
                case "INT32":
                    valueInt = (int)DATA[_i].dVALUE32;
                    break;
                case "DOUBLE":
                    valueDouble = (double)DATA[_i].fVALUE;
                    break;
            }
            string myTag;
            myTag = $"{DATA[_i].NAME} @ {SRV[MEAS[DATA[_i].MEAS_ID].SERVER_ID].NAME}";

            var data = new
            {
                timestamp = DATA[_i].TIMESTAMP,
                tag = myTag,
                doublevalue = valueDouble,
                intvalue = valueInt
            };



            string row = $"{data.timestamp},{data.tag},{data.doublevalue},{data.intvalue}";

            // dodanie wiersza do pliku
            try
            {
                using (StreamWriter writer = File.AppendText(FILE_OFFLINE_DB))
                {
                    writer.WriteLine(row);
                }
                //                    setIndFileOffline(true);
                status_file_offlineDB = true;
                sendToLog(ERROR_STATE.INFO, $"{row} WRITE TO OFFLINE1");
                ret = true;
            }
            catch
            {
                //                   setIndFileOffline(false);
                status_file_offlineDB = false;
                sendToLog(ERROR_STATE.ERROR, $"{row} NOT WRITE TO OFFLINE2");
                ret = false;
            }

            return ret;

        }
        public bool sendToOfflineMQTT(string sn, string topic, string message)
        {
            bool ret = false;
            string row = $"{sn} {topic} {message}";
            try
            {
                using (StreamWriter writer = File.AppendText(FILE_OFFLINE_MQTT))
                {
                    writer.WriteLine(row);
                }
                ret = true;
            }
            catch
            {
                ret = false;
            }

            return ret;
        }


        public async Task<bool> readMeas(int _i)
        {
            bool ret = false;
            if (SRV[MEAS[_i].SERVER_ID].TYPE == "OPC-UA")
            {
                ret = await readOpcUa(_i);
            }
            else if (SRV[MEAS[_i].SERVER_ID].TYPE == "OPC-DA")
            {
                ret = readOpcDa(_i);
            }
            else if (SRV[MEAS[_i].SERVER_ID].TYPE == "MODBUSTCP")
            {
                ret = readModbusTcp(_i);
            }
            else if (SRV[MEAS[_i].SERVER_ID].TYPE == "MODBUSRTU")
            {
                ret = readModbusRtu(_i);
            }
            else if (SRV[MEAS[_i].SERVER_ID].TYPE == "MODBUSASCII")
            {
                ret = readModbusAscii(_i);
            }
            MEAS[_i].TIMESTAMP = GetTimeStamp();



            return ret;
        }


        public void sendToBackup1(int _i)
        {
            sendToBackup(_i);
        }
        public void sendToBackup2(int _i)
        {
            //sendToBackup(_i);
        }
        public bool sendToBackup(int _i)
        {
            bool ret = false;
            Int32 valueInt = 0;
            double valueDouble = 0;
            switch (DATA[_i].TYPE)
            {
                case "BOOL":
                    if (DATA[_i].bVALUE == true) { valueInt = 1; } else { valueInt = 0; };
                    break;
                case "UINT16":
                    valueInt = (int)DATA[_i].uVALUE16;
                    break;
                case "INT16":
                    valueInt = (int)DATA[_i].dVALUE16;
                    break;
                case "UINT32":
                    valueInt = (int)DATA[_i].uVALUE32;
                    break;
                case "INT32":
                    valueInt = (int)DATA[_i].dVALUE32;
                    break;
                case "DOUBLE":
                    valueDouble = (double)DATA[_i].fVALUE;
                    break;
            }
            string myTag;
            myTag = $"{DATA[_i].NAME} @ {SRV[MEAS[DATA[_i].MEAS_ID].SERVER_ID].NAME}";

            var data = new
            {
                timestamp = DATA[_i].TIMESTAMP,
                tag = myTag,
                doublevalue = valueDouble,
                intvalue = valueInt
            };



            string row = $"{data.timestamp},{data.tag},{data.doublevalue},{data.intvalue}";

            // dodanie wiersza do pliku
            try
            {
                using (StreamWriter writer = File.AppendText(FILE_BACKUP))
                {
                    writer.WriteLine(row);
                }
                //                    setIndFileOffline(true);
                status_file_backup = true;
                sendToLog(ERROR_STATE.INFO, $"{row} WRITE TO BACKUP");
                ret = true;
            }
            catch
            {
                //                   setIndFileOffline(false);
                status_file_backup = false;
                sendToLog(ERROR_STATE.ERROR, $"{row} NOT WRITE TO BACKUP");
                ret = false;
            }

            return ret;
        }

        public string codeBytes(byte[] bytes)
        {
            string ret = Convert.ToBase64String(bytes);

            return ret;
        }

        public byte[] decodeBytes(string s)
        {
            byte[] ret = Convert.FromBase64String(s);

            return ret;
        }

        public async Task<bool> sendToDB(int _i)
        {
            bool ret = false;
            Int32 valueInt = 0;
            double valueDouble = 0;

            switch (DATA[_i].TYPE)
            {
                case "BOOL":
                    if (DATA[_i].bVALUE == true) { valueInt = 1; } else { valueInt = 0; };
                    break;
                case "BYTE":
                    valueInt = (int)DATA[_i].uVALUE16;
                    break;
                case "UINT16":
                    valueInt = (int)DATA[_i].uVALUE16;
                    break;
                case "INT16":
                    valueInt = (int)DATA[_i].dVALUE16;
                    break;
                case "UINT32":
                    valueInt = (int)DATA[_i].uVALUE32;
                    break;
                case "INT32":
                    valueInt = (int)DATA[_i].dVALUE32;
                    break;
                case "DOUBLE":
                    valueDouble = (double)DATA[_i].fVALUE;
                    break;
                case "ROW":
                    valueInt = (int)DATA[_i].dVALUE32;
                    valueDouble = (double)DATA[_i].fVALUE;
                    break;
            }
            string myTag;
            if (DATA[_i].TYPE == "ROW")
                myTag = $"{DATA[_i].NAME}";
            else
                myTag = $"{DATA[_i].NAME} @ {SRV[MEAS[DATA[_i].MEAS_ID].SERVER_ID].NAME}";


            var data = new
            {
                timestamp = DATA[_i].TIMESTAMP,
                tag = myTag,
                doublevalue = valueDouble,
                intvalue = valueInt
            };
            using (var client = new System.Net.Http.HttpClient())
            {

                try
                {
                    var uri = new Uri(SERVER_DB);
                    var jsonData = Newtonsoft.Json.JsonConvert.SerializeObject(data);
                    var content = new StringContent(jsonData, Encoding.UTF8, "application/json");
                    var response = await client.PostAsync(uri, content);


                    if (response.IsSuccessStatusCode)
                    {
                        // Sukces - dane zostały zapisane na serwerze
                        ret = true;
                        sendToLog(ERROR_STATE.INFO, $"{data} WRITE TO DB1");
                    }
                    else
                    {
                        // Błąd - nie udało się zapisać danych na serwerze
                        sendToLog(ERROR_STATE.ERROR, $"{data} NOT WRITE TO DB1");
                        ret = false;
                    }
                    
                }
                catch
                {
                    ret = false;
                }
            }



            return ret;
        }
        /*
        public async Task<bool> sendToDB2(int _i)
        {
            bool ret = true;

            return ret;
        }
        */
        public async Task sendToDBfromOffline()
        {
            string s = FILE_OFFLINE_DB;
            bool test = false;
            while (test == false)
            {
                if (File.Exists(s) == false)
                {
                    FileStream stream = File.Create(s);
                    stream.Close();
                }
                if (File.Exists(s) && new FileInfo(s).Length > 0)
                {
                    string[] lines = File.ReadAllLines(s);
                    string[] values = lines[0].Split(',');
                    DATA[DATA_COUNT].TIMESTAMP = values[0];
                    DATA[DATA_COUNT].NAME = values[1];
                    DATA[DATA_COUNT].MEAS_ID = 0;
                    DATA[DATA_COUNT].TYPE = "ROW";
                    DATA[DATA_COUNT].dVALUE32 = Convert.ToInt32(values[3]);
                    DATA[DATA_COUNT].fVALUE = Convert.ToDouble(values[2]);
                    if (await sendToDB(DATA_COUNT))
                    {

                        File.WriteAllLines(FILE_OFFLINE_DB, lines.Skip(1).ToArray());
                        sendToBackup(DATA_COUNT);
                        DB_offline = false;
                    }
                    else
                    {
                        DB_offline = true;
                        test = true;
                    }
                }
                else
                {
                    test = true;
                    DB_offline = false;
                }
            }

            // while (true) ;
        }

        public async Task sendToMQTTfromOffline()
        {
            string s = FILE_OFFLINE_MQTT;
            string topic = "";
            string message = "";
            string sn = "";
            bool ret = false;
            bool test = false;
            while (test == false)
            {
                if (File.Exists(s) == false)
                {
                    FileStream stream = File.Create(s);
                    stream.Close();
                }
                if (File.Exists(s) && new FileInfo(s).Length > 0)
                {
                    string[] lines = File.ReadAllLines(s);
                    string[] values = lines[0].Split(' ');


                    sn = values[0];
                    topic = values[1];
                    message = values[2];

                    ret = sendToMqtt(decodeBytes(message), sn, topic);
                    if (ret == true)
                    {
                        File.WriteAllLines(FILE_OFFLINE_MQTT, lines.Skip(1).ToArray());
                    }
                    else
                    {
                        test = true;
                    }
                    //zapis do MQTT
                }
                else
                {
                    MQTT_offline = false;
                    test = true;
                }
            }
        }//public async Task sendToMQTTfromOffline()

        public bool sendToLog(ERROR_STATE type, string s)
        {
            bool ret = false;
            string row = "";
            if (DEBUG_LEVEL >= type)
            {
                if (type == ERROR_STATE.ERROR) { row = $"ERROR:   {GetTimeStamp()}\t {s}"; }
                if (type == ERROR_STATE.WARNINIG) { row = $"WARNING: {GetTimeStamp()}\t {s}"; }
                if (type == ERROR_STATE.INFO) { row = $"INFO:    {GetTimeStamp()}\t {s}"; }
                try
                {
                    using (StreamWriter writer = File.AppendText(FILE_LOG))
                    {
                        writer.WriteLine(row);
                    }

                    ret = true;
                }
                catch
                {

                    ret = false;
                }
                Debug.WriteLine(row);

            }
            return ret;
        }
        public void analyzeData(int i)
        {
            if (DATA[i].ON == 1 && MEAS[DATA[i].MEAS_ID].OK == 1)
            {
                UInt16 utmp16 = 0;
                Int16 dtmp16 = 0;
                UInt32 utmp32 = 0;
                Int32 dtmp32 = 0;

                double ftmp = 0;
                bool btmp = false;
                double tmp = 0;

                if (DATA[i].MEAS_ID > 0)
                {
                    if (MEAS[DATA[i].MEAS_ID].TYPE == "BYTE") { utmp16 = (UInt16)MEAS[DATA[i].MEAS_ID].uVALUE16[DATA[i].POS]; tmp = utmp16; }
                    if (MEAS[DATA[i].MEAS_ID].TYPE == "UINT16") { utmp16 = (UInt16)MEAS[DATA[i].MEAS_ID].uVALUE16[DATA[i].POS]; tmp = utmp16; }
                    if (MEAS[DATA[i].MEAS_ID].TYPE == "INT16") { dtmp16 = (Int16)MEAS[DATA[i].MEAS_ID].dVALUE16[DATA[i].POS]; tmp = dtmp16; }
                    if (MEAS[DATA[i].MEAS_ID].TYPE == "UINT32") { utmp32 = (UInt32)MEAS[DATA[i].MEAS_ID].uVALUE32[DATA[i].POS]; tmp = utmp32; }
                    if (MEAS[DATA[i].MEAS_ID].TYPE == "INT32") { dtmp32 = (Int32)MEAS[DATA[i].MEAS_ID].dVALUE32[DATA[i].POS]; tmp = dtmp32; }
                    if (MEAS[DATA[i].MEAS_ID].TYPE == "DOUBLE") { ftmp = MEAS[DATA[i].MEAS_ID].fVALUE[DATA[i].POS]; tmp = ftmp; }
                    if (MEAS[DATA[i].MEAS_ID].TYPE == "BOOL") { btmp = MEAS[DATA[i].MEAS_ID].bVALUE[DATA[i].POS]; }
                }
                else
                {
                    tmp = 0;
                }

                if (DATA[i].TYPE == "BYTE") tmp = (Int16)tmp;
                if (DATA[i].TYPE == "INT16") tmp = (Int16)tmp;
                if (DATA[i].TYPE == "UINT16") tmp = (UInt16)tmp;
                if (DATA[i].TYPE == "INT32") tmp = (Int32)tmp;
                if (DATA[i].TYPE == "UINT32") tmp = (UInt32)tmp;

                if (MEAS[DATA[i].MEAS_ID].TYPE == "DOUBLE") { if (DATA[i].TYPE == "DOUBLE") tmp = (double)tmp; }
                else { if (DATA[i].TYPE == "DOUBLE") tmp = (Int16)tmp; }



                tmp *= DATA[i].MULT;
                tmp += DATA[i].ADD;

                if (DATA[DATA[i].DATA_ID].TYPE == "BYTE") tmp += DATA[DATA[i].DATA_ID].dVALUE16;
                if (DATA[DATA[i].DATA_ID].TYPE == "INT16") tmp += DATA[DATA[i].DATA_ID].dVALUE16;
                if (DATA[DATA[i].DATA_ID].TYPE == "UINT16") tmp += DATA[DATA[i].DATA_ID].uVALUE16;
                if (DATA[DATA[i].DATA_ID].TYPE == "INT32") tmp += DATA[DATA[i].DATA_ID].dVALUE32;
                if (DATA[DATA[i].DATA_ID].TYPE == "UINT32") tmp += DATA[DATA[i].DATA_ID].uVALUE32;
                if (DATA[DATA[i].DATA_ID].TYPE == "DOUBLE") tmp += DATA[DATA[i].DATA_ID].fVALUE;


                //jezeli wskazanie jest na pozycje 0 to robimy mnozenie po dodaniu
                if (DATA[i].MEAS_ID == 0)
                {
                    tmp *= DATA[i].MULT;
                }

                if (DATA[i].TYPE == "BYTE")
                {
                    DATA[i].uVALUE16 = (UInt16)tmp;
                }
                if (DATA[i].TYPE == "UINT16")
                {
                    DATA[i].uVALUE16 = (UInt16)tmp;
                }
                if (DATA[i].TYPE == "INT16")
                {
                    DATA[i].dVALUE16 = (Int16)tmp;
                }
                if (DATA[i].TYPE == "UINT32")
                {
                    DATA[i].uVALUE32 = (UInt32)tmp;
                }
                if (DATA[i].TYPE == "INT32")
                {
                    DATA[i].dVALUE32 = (Int32)tmp;
                }
                if (DATA[i].TYPE == "DOUBLE")
                {
                    DATA[i].fVALUE = (double)tmp;
                }
                if (DATA[i].TYPE == "BOOL")
                {
                    DATA[i].bVALUE = MEAS[DATA[i].MEAS_ID].bVALUE[DATA[i].POS];
                }
                DATA[i].TIMESTAMP = MEAS[DATA[i].MEAS_ID].TIMESTAMP;
            }
        }




        public string GetTimeStamp()
        {
            DateTimeOffset now = DateTimeOffset.Now;
            return now.ToString("yyyy-MM-dd HH:mm:ss.fff");
        }

        public bool CheckInternet()
        {
            bool ret = false;
            try
            {
                using (var ping = new Ping())
                {
                    var result = ping.Send(SERVER_PING_URL, SERVER_PING_TIMEOUT);
                    if (result != null)
                    {
                        if (result.Status == IPStatus.Success)
                        {
                            ret = true;
                        }
                    }

                }
            }
            catch
            {
                ret = false;
            }

            return ret;
        }


        //========================================================================================= SYSTEM
        public void readSysfile_SYSTEM()
        {
            XmlReader xmlReader;

            try
            {
                Sysfile_SYSTEM.filePath = FILE_SYSTEM;

                xmlReader = XmlReader.Create(Sysfile_SYSTEM.filePath);
                while (xmlReader.Read())
                {

                    if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "VERSION") && (xmlReader.HasAttributes))
                    {
                        if (xmlReader.GetAttribute("NAME") != null) Sysfile_SYSTEM.fileName = xmlReader.GetAttribute("NAME");
                        if (xmlReader.GetAttribute("RELEASE") != null) Sysfile_SYSTEM.fileRelease = xmlReader.GetAttribute("RELEASE");
                        if (xmlReader.GetAttribute("TIMESTAMP") != null) Sysfile_SYSTEM.fileTimeStamp = xmlReader.GetAttribute("TIMESTAMP");
                        if (xmlReader.GetAttribute("HARDWARE") != null) Sysfile_SYSTEM.fileHardware = xmlReader.GetAttribute("HARDWARE");
                        if (xmlReader.GetAttribute("HARDWARE_VERSION") != null) Sysfile_SYSTEM.fileHardware_Version = xmlReader.GetAttribute("HARDWARE_VERSION");
                    }


                    if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "SYSTEM") && (xmlReader.HasAttributes))
                    {
                        if (xmlReader.GetAttribute("SOFTWARE") != null) SOFTWARE = xmlReader.GetAttribute("SOFTWARE");
                        if (xmlReader.GetAttribute("SOFTWARE_VERSION") != null) SOFTWARE_VERSION = xmlReader.GetAttribute("SOFTWARE_VERSION");
                        if (xmlReader.GetAttribute("SYSFILE_FOLDER") != null) Sysfile_FOLDER = xmlReader.GetAttribute("SYSFILE_FOLDER");
                        if (xmlReader.GetAttribute("SYSFILE_CONFIGURATION") != null) Sysfile_CONFIGURATION.filePath = Sysfile_FOLDER + xmlReader.GetAttribute("SYSFILE_CONFIGURATION");
                        if (xmlReader.GetAttribute("SYSFILE_MEASUREMENT") != null) Sysfile_MEASUREMENT.filePath = Sysfile_FOLDER + xmlReader.GetAttribute("SYSFILE_MEASUREMENT");
                        if (xmlReader.GetAttribute("SYSFILE_MQTT_PUBLISHER") != null) Sysfile_MQTT_PUBLISHER.filePath = Sysfile_FOLDER + xmlReader.GetAttribute("SYSFILE_MQTT_PUBLISHER");
                        if (xmlReader.GetAttribute("SYSFILE_MQTT_SUBSCRIBER") != null) Sysfile_MQTT_SUBSCRIBER.filePath = Sysfile_FOLDER + xmlReader.GetAttribute("SYSFILE_MQTT_SUBSCRIBER");
                        if (xmlReader.GetAttribute("SYSFILE_DATABASE") != null) Sysfile_DATABASE.filePath = Sysfile_FOLDER + xmlReader.GetAttribute("SYSFILE_DATABASE");
                        if (xmlReader.GetAttribute("SYSFILE_SCADA") != null) Sysfile_SCADA.filePath = Sysfile_FOLDER + xmlReader.GetAttribute("SYSFILE_SCADA");
                    }
                }
            }
            catch
            {
            }
        }
        //========================================================================================= HARDWARE
        public void readSysfile_HARDWARE()
        {
            XmlReader xmlReader;

            try
            {
                Sysfile_HARDWARE.filePath = FILE_HARDWARE;

                xmlReader = XmlReader.Create(Sysfile_HARDWARE.filePath);
                while (xmlReader.Read())
                {

                    if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "VERSION") && (xmlReader.HasAttributes))
                    {
                        if (xmlReader.GetAttribute("NAME") != null) Sysfile_HARDWARE.fileName = xmlReader.GetAttribute("NAME");
                        if (xmlReader.GetAttribute("RELEASE") != null) Sysfile_HARDWARE.fileRelease = xmlReader.GetAttribute("RELEASE");
                        if (xmlReader.GetAttribute("TIMESTAMP") != null) Sysfile_HARDWARE.fileTimeStamp = xmlReader.GetAttribute("TIMESTAMP");
                        if (xmlReader.GetAttribute("HARDWARE") != null) Sysfile_HARDWARE.fileHardware = xmlReader.GetAttribute("HARDWARE");
                        if (xmlReader.GetAttribute("HARDWARE_VERSION") != null) Sysfile_HARDWARE.fileHardware_Version = xmlReader.GetAttribute("HARDWARE_VERSION");
                    }


                    if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "HARDWARE_INFO") && (xmlReader.HasAttributes))
                    {
                        if (xmlReader.GetAttribute("HARDWARE") != null) HARDWARE = xmlReader.GetAttribute("HARDWARE");
                        if (xmlReader.GetAttribute("HARDWARE_VERSION") != null) HARDWARE_VERSION = xmlReader.GetAttribute("HARDWARE_VERSION");
                        if (xmlReader.GetAttribute("SERIAL_NUMBER") != null) SERIALNUMBER = xmlReader.GetAttribute("SERIAL_NUMBER");
                    }
                }
            }
            catch
            {
            }
        }

        //========================================================================================= MQTT PUBLISHER
        public void readSysfile_MQTT_PUBLISHER()
        {
            XmlReader xmlReader;

            try
            {
                xmlReader = XmlReader.Create(Sysfile_MQTT_PUBLISHER.filePath);
                while (xmlReader.Read())
                {

                    if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "VERSION") && (xmlReader.HasAttributes))
                    {
                        if (xmlReader.GetAttribute("NAME") != null) Sysfile_MQTT_PUBLISHER.fileName = xmlReader.GetAttribute("NAME");
                        if (xmlReader.GetAttribute("RELEASE") != null) Sysfile_MQTT_PUBLISHER.fileRelease = xmlReader.GetAttribute("RELEASE");
                        if (xmlReader.GetAttribute("TIMESTAMP") != null) Sysfile_MQTT_PUBLISHER.fileTimeStamp = xmlReader.GetAttribute("TIMESTAMP");
                        if (xmlReader.GetAttribute("HARDWARE") != null) Sysfile_MQTT_PUBLISHER.fileHardware = xmlReader.GetAttribute("HARDWARE");
                        if (xmlReader.GetAttribute("HARDWARE_VERSION") != null) Sysfile_MQTT_PUBLISHER.fileHardware_Version = xmlReader.GetAttribute("HARDWARE_VERSION");
                    }

                    if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "PAYLOAD") && (xmlReader.HasAttributes))
                    {
                        PAYLOAD[Convert.ToInt32(xmlReader.GetAttribute("ID"))].Update(
                                                                                        name: xmlReader.GetAttribute("NAME"),
                                                                                        topic: xmlReader.GetAttribute("TOPIC"),
                                                                                        sn: xmlReader.GetAttribute("SN"));

                        if (xmlReader.GetAttribute("SAMPLETYPE") != null) PAYLOAD[Convert.ToInt32(xmlReader.GetAttribute("ID"))].SAMPLETYPE = xmlReader.GetAttribute("SAMPLETYPE");
                        if (xmlReader.GetAttribute("PHASE") != null) PAYLOAD[Convert.ToInt32(xmlReader.GetAttribute("ID"))].PHASE = xmlReader.GetAttribute("PHASE");

                        if (PAYLOAD_COUNT <= Convert.ToInt32(xmlReader.GetAttribute("ID")))
                            PAYLOAD_COUNT = Convert.ToInt32(xmlReader.GetAttribute("ID")) + 1;

                    }

                    if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "ITEM") && (xmlReader.HasAttributes))
                    {
                        ITEM[Convert.ToInt32(xmlReader.GetAttribute("ID"))].Update(
                                                                                        name: xmlReader.GetAttribute("NAME"),
                                                                                        obj: xmlReader.GetAttribute("OBJECT"),
                                                                                        type: xmlReader.GetAttribute("TYPE"),
                                                                                        data_id: Convert.ToInt16(xmlReader.GetAttribute("DATA_ID")),
                                                                                        payload_id: Convert.ToInt16(xmlReader.GetAttribute("PAYLOAD_ID"))
                                                                                        );

                        if (ITEM_COUNT <= Convert.ToInt32(xmlReader.GetAttribute("ID")))
                            ITEM_COUNT = Convert.ToInt32(xmlReader.GetAttribute("ID")) + 1;

                    }

                }
            }
            catch
            {
            }
        }

        //========================================================================================= MQTT SUBSCRIBER
        public void readSysfile_MQTT_SUBSCRIBER()
        {
            XmlReader xmlReader;

            try
            {
                xmlReader = XmlReader.Create(Sysfile_MQTT_SUBSCRIBER.filePath);
                while (xmlReader.Read())
                {

                    if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "VERSION") && (xmlReader.HasAttributes))
                    {
                        if (xmlReader.GetAttribute("NAME") != null) Sysfile_MQTT_SUBSCRIBER.fileName = xmlReader.GetAttribute("NAME");
                        if (xmlReader.GetAttribute("RELEASE") != null) Sysfile_MQTT_SUBSCRIBER.fileRelease = xmlReader.GetAttribute("RELEASE");
                        if (xmlReader.GetAttribute("TIMESTAMP") != null) Sysfile_MQTT_SUBSCRIBER.fileTimeStamp = xmlReader.GetAttribute("TIMESTAMP");
                        if (xmlReader.GetAttribute("HARDWARE") != null) Sysfile_MQTT_SUBSCRIBER.fileHardware = xmlReader.GetAttribute("HARDWARE");
                        if (xmlReader.GetAttribute("HARDWARE_VERSION") != null) Sysfile_MQTT_SUBSCRIBER.fileHardware_Version = xmlReader.GetAttribute("HARDWARE_VERSION");
                    }

                    if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "RECEIVER") && (xmlReader.HasAttributes))
                    {
                        RECEIVER[Convert.ToInt32(xmlReader.GetAttribute("ID"))].Update(
                                                                                        name: xmlReader.GetAttribute("NAME"),
                                                                                        sn: xmlReader.GetAttribute("SN"),
                                                                                        topic: xmlReader.GetAttribute("TOPIC"),
                                                                                        response: xmlReader.GetAttribute("RESPONSE"),
                                                                                        on: 1
                            );
                    }//if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "RECEIVER") && (xmlReader.HasAttributes))



                    if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "PROC") && (xmlReader.HasAttributes))
                    {
                        PROC[Convert.ToInt32(xmlReader.GetAttribute("ID"))].Update(
                                                                                        name: xmlReader.GetAttribute("NAME"),
                                                                                        procedure_number: Convert.ToInt16(xmlReader.GetAttribute("PROCEDURE_NUMBER")),
                                                                                        save_id: Convert.ToInt16(xmlReader.GetAttribute("SAVE_ID")),
                                                                                        type: xmlReader.GetAttribute("TYPE"),
                                                                                        value: xmlReader.GetAttribute("VALUE"),
                                                                                        on: 1
                            );
                    }//if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "PROC") && (xmlReader.HasAttributes))

                    if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "SAVE") && (xmlReader.HasAttributes))
                    {
                        SAVE[Convert.ToInt32(xmlReader.GetAttribute("ID"))].Update(
                                                                                        name: xmlReader.GetAttribute("NAME"),
                                                                                        type: xmlReader.GetAttribute("TYPE"),
                                                                                        server_id: Convert.ToInt16(xmlReader.GetAttribute("SERVER_ID")),
                                                                                        operation: xmlReader.GetAttribute("OPERATION"),
                                                                                        tag: xmlReader.GetAttribute("TAG"),
                                                                                        address: xmlReader.GetAttribute("ADDRESS"),
                                                                                        on: 1
                            );
                    }//if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "SAVE") && (xmlReader.HasAttributes))


                }
            }
            catch
            {
            }
        }

        //========================================================================================= CONFIGURATION
        public void readSysfile_CONFIGURATION()
        {
            XmlReader xmlReader;

            try
            {
                xmlReader = XmlReader.Create(Sysfile_CONFIGURATION.filePath);
                while (xmlReader.Read())
                {

                    if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "VERSION") && (xmlReader.HasAttributes))
                    {
                        if (xmlReader.GetAttribute("NAME") != null) Sysfile_CONFIGURATION.fileName = xmlReader.GetAttribute("NAME");
                        if (xmlReader.GetAttribute("RELEASE") != null) Sysfile_CONFIGURATION.fileRelease = xmlReader.GetAttribute("RELEASE");
                        if (xmlReader.GetAttribute("TIMESTAMP") != null) Sysfile_CONFIGURATION.fileTimeStamp = xmlReader.GetAttribute("TIMESTAMP");
                        if (xmlReader.GetAttribute("HARDWARE") != null) Sysfile_CONFIGURATION.fileHardware = xmlReader.GetAttribute("HARDWARE");
                        if (xmlReader.GetAttribute("HARDWARE_VERSION") != null) Sysfile_CONFIGURATION.fileHardware_Version = xmlReader.GetAttribute("HARDWARE_VERSION");
                    }

                    if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "FILE_LOG") && (xmlReader.HasAttributes))
                    {
                        FILE_LOG = xmlReader.GetAttribute("FILENAME");
                        FILE_LOG_NAME = xmlReader.GetAttribute("NAME");
                    }//if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "FILE_LOG") && (xmlReader.HasAttributes))

                    if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "FILE_BACKUP") && (xmlReader.HasAttributes))
                    {
                        FILE_BACKUP = xmlReader.GetAttribute("FILENAME");
                        FILE_BACKUP_NAME = xmlReader.GetAttribute("NAME");
                    }//if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "FILE_BACKUP") && (xmlReader.HasAttributes))

                    if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "FILE_OFFLINE_DB") && (xmlReader.HasAttributes))
                    {
                        FILE_OFFLINE_DB = xmlReader.GetAttribute("FILENAME");
                        FILE_OFFLINE_DB_NAME = xmlReader.GetAttribute("NAME");
                    }//if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "FILE_OFFLINE1") && (xmlReader.HasAttributes))

                    if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "FILE_OFFLINE_MQTT") && (xmlReader.HasAttributes))
                    {
                        FILE_OFFLINE_MQTT = xmlReader.GetAttribute("FILENAME");
                        FILE_OFFLINE_MQTT_NAME = xmlReader.GetAttribute("NAME");
                    }//if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "FILE_OFFLINE2") && (xmlReader.HasAttributes))


                    if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "DEBUG") && (xmlReader.HasAttributes))
                    {
                        //string s = xmlReader.GetAttribute("DEBUG_LEVEL");
                        //int i = Convert.ToInt16(s);
                        //ERROR_STATE e = (ERROR_STATE)(i);
                        DEBUG_LEVEL = (ERROR_STATE)(Convert.ToInt16(xmlReader.GetAttribute("DEBUG_LEVEL")));
                        DEBUG_LEVEL_NAME = xmlReader.GetAttribute("NAME");
                    }//if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "DEBUG") && (xmlReader.HasAttributes))

                }
            }
            catch
            {
            }
        }

        //========================================================================================= MEASUREMENT
        public void readSysfile_MEASUREMENT()
        {
            XmlReader xmlReader;

            try
            {
                xmlReader = XmlReader.Create(Sysfile_MEASUREMENT.filePath);
                while (xmlReader.Read())
                {

                    if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "VERSION") && (xmlReader.HasAttributes))
                    {
                        if (xmlReader.GetAttribute("NAME") != null) Sysfile_MEASUREMENT.fileName = xmlReader.GetAttribute("NAME");
                        if (xmlReader.GetAttribute("RELEASE") != null) Sysfile_MEASUREMENT.fileRelease = xmlReader.GetAttribute("RELEASE");
                        if (xmlReader.GetAttribute("TIMESTAMP") != null) Sysfile_MEASUREMENT.fileTimeStamp = xmlReader.GetAttribute("TIMESTAMP");
                        if (xmlReader.GetAttribute("HARDWARE") != null) Sysfile_MEASUREMENT.fileHardware = xmlReader.GetAttribute("HARDWARE");
                        if (xmlReader.GetAttribute("HARDWARE_VERSION") != null) Sysfile_MEASUREMENT.fileHardware_Version = xmlReader.GetAttribute("HARDWARE_VERSION");
                    }

                    if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "SYSTEM") && (xmlReader.HasAttributes))
                    {
                        if (xmlReader.GetAttribute("INTERVAL") != null)
                            INTERVAL = Convert.ToInt32(xmlReader.GetAttribute("INTERVAL"));
                    }//if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "SYSTEM") && (xmlReader.HasAttributes))


                    //Serial Ports
                    if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "HW") && (xmlReader.HasAttributes))
                    {
                        if (xmlReader.GetAttribute("TYPE") == "SERIAL")
                        {
                            HW[Convert.ToInt32(xmlReader.GetAttribute("ID"))].Update(
                                                                                        name: xmlReader.GetAttribute("NAME"),
                                                                                        type: xmlReader.GetAttribute("TYPE"),
                                                                                        port: xmlReader.GetAttribute("PORT"),
                                                                                        baud: Convert.ToInt32(xmlReader.GetAttribute("BAUD")),
                                                                                        parity: (Parity)System.Enum.Parse(typeof(Parity), xmlReader.GetAttribute("PARITY")),
                                                                                        databits: Convert.ToInt16(xmlReader.GetAttribute("DATABITS")),
                                                                                        stopbits: (StopBits)System.Enum.Parse(typeof(StopBits), xmlReader.GetAttribute("STOPBITS")),
                                                                                        handshaking: (Handshake)System.Enum.Parse(typeof(Handshake), xmlReader.GetAttribute("HANDSHAKING")),
                                                                                        on: 1,
                                                                                        ok: 0);
                        }
                        if (xmlReader.GetAttribute("TYPE") == "TCP")
                        {
                            HW[Convert.ToInt32(xmlReader.GetAttribute("ID"))].Update(
                                                                                        name: xmlReader.GetAttribute("NAME"),
                                                                                        port: xmlReader.GetAttribute("PORT"),
                                                                                        type: xmlReader.GetAttribute("TYPE"),
                                                                                        url: xmlReader.GetAttribute("URL"),
                                                                                        on: 1,
                                                                                        ok: 0);
                        }
                        if (HW_COUNT <= Convert.ToInt32(xmlReader.GetAttribute("ID")))
                            HW_COUNT = Convert.ToInt32(xmlReader.GetAttribute("ID")) + 1;
                    }//if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "CONFIG_HARDWARE") && (xmlReader.HasAttributes))


                    if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "SERVER") && (xmlReader.HasAttributes))
                    {
                        if (xmlReader.GetAttribute("TYPE") == "MODBUSTCP")
                        {
                            SRV[Convert.ToInt32(xmlReader.GetAttribute("ID"))].Update(
                                                                                        name: xmlReader.GetAttribute("NAME"),
                                                                                        type: xmlReader.GetAttribute("TYPE"),
                                                                                        hw_id: Convert.ToInt16(xmlReader.GetAttribute("HW_ID")),
                                                                                        port: Convert.ToInt16(xmlReader.GetAttribute("PORT")),
                                                                                        on: 1,
                                                                                        ok: 0
                                                                                       );
                        }
                        if (xmlReader.GetAttribute("TYPE") == "MODBUSRTU")
                        {
                            SRV[Convert.ToInt32(xmlReader.GetAttribute("ID"))].Update(
                                                                                        name: xmlReader.GetAttribute("NAME"),
                                                                                        type: xmlReader.GetAttribute("TYPE"),
                                                                                        hw_id: Convert.ToInt16(xmlReader.GetAttribute("HW_ID")),
                                                                                        slave: Convert.ToInt16(xmlReader.GetAttribute("SLAVE")),
                                                                                        on: 1,
                                                                                        ok: 0
                                                                                        );
                        }
                        if (xmlReader.GetAttribute("TYPE") == "MODBUSASCII")
                        {
                            SRV[Convert.ToInt32(xmlReader.GetAttribute("ID"))].Update(
                                                                                        name: xmlReader.GetAttribute("NAME"),
                                                                                        type: xmlReader.GetAttribute("TYPE"),
                                                                                        hw_id: Convert.ToInt16(xmlReader.GetAttribute("HW_ID")),
                                                                                        slave: Convert.ToInt16(xmlReader.GetAttribute("SLAVE")),
                                                                                        on: 1,
                                                                                        ok: 0
                                                                                        );
                        }
                        if (xmlReader.GetAttribute("TYPE") == "OPC-UA")
                        {
                            SRV[Convert.ToInt32(xmlReader.GetAttribute("ID"))].Update(
                                                                                        name: xmlReader.GetAttribute("NAME"),
                                                                                        type: xmlReader.GetAttribute("TYPE"),
                                                                                        hw_id: Convert.ToInt16(xmlReader.GetAttribute("HW_ID")),
                                                                                        username: xmlReader.GetAttribute("USERNAME"),
                                                                                        password: xmlReader.GetAttribute("PASSWORD"),
                                                                                        certificate: xmlReader.GetAttribute("SERTIFICATE"),
                                                                                        on: 1,
                                                                                        ok: 0
                                                                                        );
                        }
                        if (xmlReader.GetAttribute("TYPE") == "OPC-DA")
                        {
                            SRV[Convert.ToInt32(xmlReader.GetAttribute("ID"))].Update(
                                                                                        name: xmlReader.GetAttribute("NAME"),
                                                                                        type: xmlReader.GetAttribute("TYPE"),
                                                                                        hw_id: Convert.ToInt16(xmlReader.GetAttribute("HW_ID")),
                                                                                        username: xmlReader.GetAttribute("USERNAME"),
                                                                                        password: xmlReader.GetAttribute("PASSWORD"),
                                                                                        certificate: xmlReader.GetAttribute("SERTIFICATE"),
                                                                                        on: 1,
                                                                                        ok: 0
                                                                                        );

                        }
                        if (SRV_COUNT <= Convert.ToInt32(xmlReader.GetAttribute("ID")))
                            SRV_COUNT = Convert.ToInt32(xmlReader.GetAttribute("ID")) + 1;
                    }//if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "HW") && (xmlReader.HasAttributes))

                    if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "MEAS") && (xmlReader.HasAttributes))
                    {
                        MEAS[Convert.ToInt32(xmlReader.GetAttribute("ID"))].Update(
                                                                                        name: xmlReader.GetAttribute("NAME"),
                                                                                        type: xmlReader.GetAttribute("TYPE"),
                                                                                        server_id: Convert.ToInt16(xmlReader.GetAttribute("SERVER_ID")),
                                                                                        operation: xmlReader.GetAttribute("OPERATION"),
                                                                                        start: Convert.ToInt16(xmlReader.GetAttribute("START")),
                                                                                        count: Convert.ToInt16(xmlReader.GetAttribute("COUNT")),
                                                                                        on: 1,
                                                                                        ok: 0
                                                                                        );
                        if (xmlReader.GetAttribute("TAG") != null) MEAS[Convert.ToInt32(xmlReader.GetAttribute("ID"))].TAG = xmlReader.GetAttribute("TAG");
                        if (MEAS_COUNT <= Convert.ToInt32(xmlReader.GetAttribute("ID")))
                            MEAS_COUNT = Convert.ToInt32(xmlReader.GetAttribute("ID")) + 1;

                    }// if((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "MEAS") && (xmlReader.HasAttributes))

                    if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "DATA") && (xmlReader.HasAttributes))
                    {
                        DATA[Convert.ToInt32(xmlReader.GetAttribute("ID"))].Update(
                                                                                        name: xmlReader.GetAttribute("NAME"),
                                                                                        type: xmlReader.GetAttribute("TYPE"),
                                                                                        meas_id: Convert.ToInt16(xmlReader.GetAttribute("MEAS_ID")),
                                                                                        pos: Convert.ToInt16(xmlReader.GetAttribute("POS")),
                                                                                        db: Convert.ToInt16(xmlReader.GetAttribute("DB")),
                                                                                        on: 1,
                                                                                        ok: 0
                                                                                        );
                        if (xmlReader.GetAttribute("MULT") != null) DATA[Convert.ToInt32(xmlReader.GetAttribute("ID"))].MULT = double.Parse(xmlReader.GetAttribute("MULT"));
                        if (xmlReader.GetAttribute("ADD") != null) DATA[Convert.ToInt32(xmlReader.GetAttribute("ID"))].ADD = double.Parse(xmlReader.GetAttribute("ADD"));
                        if (xmlReader.GetAttribute("DATA_ID") != null) DATA[Convert.ToInt32(xmlReader.GetAttribute("ID"))].DATA_ID = Convert.ToInt16(xmlReader.GetAttribute("DATA_ID"));

                        if (DATA_COUNT <= Convert.ToInt32(xmlReader.GetAttribute("ID")))
                            DATA_COUNT = Convert.ToInt32(xmlReader.GetAttribute("ID")) + 1;

                    }// if((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "DATA") && (xmlReader.HasAttributes))


                }
            }
            catch
            {
            }
        }

        //========================================================================================= DATABASE
        public void readSysfile_DATABASE()
        {
            XmlReader xmlReader;

            try
            {
                xmlReader = XmlReader.Create(Sysfile_DATABASE.filePath);
                while (xmlReader.Read())
                {

                    if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "VERSION") && (xmlReader.HasAttributes))
                    {
                        if (xmlReader.GetAttribute("NAME") != null) Sysfile_DATABASE.fileName = xmlReader.GetAttribute("NAME");
                        if (xmlReader.GetAttribute("RELEASE") != null) Sysfile_DATABASE.fileRelease = xmlReader.GetAttribute("RELEASE");
                        if (xmlReader.GetAttribute("TIMESTAMP") != null) Sysfile_DATABASE.fileTimeStamp = xmlReader.GetAttribute("TIMESTAMP");
                        if (xmlReader.GetAttribute("HARDWARE") != null) Sysfile_DATABASE.fileHardware = xmlReader.GetAttribute("HARDWARE");
                        if (xmlReader.GetAttribute("HARDWARE_VERSION") != null) Sysfile_DATABASE.fileHardware_Version = xmlReader.GetAttribute("HARDWARE_VERSION");
                    }

                    if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "SERVER_DB") && (xmlReader.HasAttributes))
                    {
                        SERVER_DB = xmlReader.GetAttribute("URL");
                        SERVER_DB_NAME = xmlReader.GetAttribute("NAME");
                    }//if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "SERVER_DB") && (xmlReader.HasAttributes))


                    if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "SERVER_MQTT") && (xmlReader.HasAttributes))
                    {
                        MQTT_NAME = xmlReader.GetAttribute("NAME");
                        MQTT_URL = xmlReader.GetAttribute("URL");
                        MQTT_PORT = xmlReader.GetAttribute("PORT");
                        MQTT_TOPIC = xmlReader.GetAttribute("TOPIC");
                        MQTT_USERNAME = xmlReader.GetAttribute("USERNAME");
                        MQTT_PASSWORD = xmlReader.GetAttribute("PASSWORD");
                    }//if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "SERVER_MQTT") && (xmlReader.HasAttributes))

                    if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "SERVER_PING") && (xmlReader.HasAttributes))
                    {
                        SERVER_PING = xmlReader.GetAttribute("URL");
                        SERVER_PING_NAME = xmlReader.GetAttribute("NAME");
                        SERVER_PING_URL = xmlReader.GetAttribute("URL");
                        SERVER_PING_TIMEOUT = Convert.ToInt16(xmlReader.GetAttribute("TIMEOUT"));
                    }//if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "SERVER_PING") && (xmlReader.HasAttributes))
                }
            }
            catch
            {
            }
        }

        //========================================================================================= SCADA
        public void readSysfile_SCADA()
        {
            XmlReader xmlReader;

            try
            {
                xmlReader = XmlReader.Create(Sysfile_SCADA.filePath);
                while (xmlReader.Read())
                {

                    if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "VERSION") && (xmlReader.HasAttributes))
                    {
                        if (xmlReader.GetAttribute("NAME") != null) Sysfile_SCADA.fileName = xmlReader.GetAttribute("NAME");
                        if (xmlReader.GetAttribute("RELEASE") != null) Sysfile_SCADA.fileRelease = xmlReader.GetAttribute("RELEASE");
                        if (xmlReader.GetAttribute("TIMESTAMP") != null) Sysfile_SCADA.fileTimeStamp = xmlReader.GetAttribute("TIMESTAMP");
                        if (xmlReader.GetAttribute("HARDWARE") != null) Sysfile_SCADA.fileHardware = xmlReader.GetAttribute("HARDWARE");
                        if (xmlReader.GetAttribute("HARDWARE_VERSION") != null) Sysfile_SCADA.fileHardware_Version = xmlReader.GetAttribute("HARDWARE_VERSION");
                    }

                    if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "DISP") && (xmlReader.HasAttributes))
                    {
                        DISP[Convert.ToInt32(xmlReader.GetAttribute("ID"))].Update(
                                                                                        name: xmlReader.GetAttribute("NAME"),
                                                                                        type: xmlReader.GetAttribute("TYPE"),
                                                                                        data_id: Convert.ToInt16(xmlReader.GetAttribute("DATA_ID")),
                                                                                        x: Convert.ToInt16(xmlReader.GetAttribute("X")),
                                                                                        y: Convert.ToInt16(xmlReader.GetAttribute("Y")),
                                                                                        neg: Convert.ToInt16(xmlReader.GetAttribute("NEG")),
                                                                                        pointer_on: Convert.ToInt16(xmlReader.GetAttribute("POINTER_ON")),
                                                                                        pointer_off: Convert.ToInt16(xmlReader.GetAttribute("POINTER_ON")),
                                                                                        on: 1,
                                                                                        ok: 0
                            );
                        if (xmlReader.GetAttribute("RANGE") != null) DISP[Convert.ToInt32(xmlReader.GetAttribute("ID"))].RANGE = Convert.ToInt16(xmlReader.GetAttribute("RANGE"));
                        if (xmlReader.GetAttribute("MIN") != null) DISP[Convert.ToInt32(xmlReader.GetAttribute("ID"))].MIN = double.Parse(xmlReader.GetAttribute("MIN"));
                        if (xmlReader.GetAttribute("MAX") != null) DISP[Convert.ToInt32(xmlReader.GetAttribute("ID"))].MAX = double.Parse(xmlReader.GetAttribute("MAX"));
                        if (DISP_COUNT <= Convert.ToInt32(xmlReader.GetAttribute("ID")))
                            DISP_COUNT = Convert.ToInt32(xmlReader.GetAttribute("ID")) + 1;
                    }//if ((xmlReader.NodeType == XmlNodeType.Element) && (xmlReader.Name == "DISP") && (xmlReader.HasAttributes))
                }
            }
            catch
            {
            }
        }
    }
}
// <auto-generated>
//     Generated by the protocol buffer compiler.  DO NOT EDIT!
//     source: command/security/InstallCertificate.proto
// </auto-generated>
#pragma warning disable 1591, 0612, 3021, 8981
#region Designer generated code

using pb = global::Google.Protobuf;
using pbc = global::Google.Protobuf.Collections;
using pbr = global::Google.Protobuf.Reflection;
using scg = global::System.Collections.Generic;
namespace InstallCertificate {

  /// <summary>Holder for reflection information generated from command/security/InstallCertificate.proto</summary>
  public static partial class InstallCertificateReflection {

    #region Descriptor
    /// <summary>File descriptor for command/security/InstallCertificate.proto</summary>
    public static pbr::FileDescriptor Descriptor {
      get { return descriptor; }
    }
    private static pbr::FileDescriptor descriptor;

    static InstallCertificateReflection() {
      byte[] descriptorData = global::System.Convert.FromBase64String(
          string.Concat(
            "Ciljb21tYW5kL3NlY3VyaXR5L0luc3RhbGxDZXJ0aWZpY2F0ZS5wcm90bxIT",
            "aW5zdGFsbF9jZXJ0aWZpY2F0ZRotY29tbWFuZC9zZWN1cml0eS9DZXJ0aWZp",
            "Y2F0ZVVzZUVudW1UeXBlLnByb3RvIn0KGUluc3RhbGxDZXJ0aWZpY2F0ZUNv",
            "bW1hbmQSSwoQY2VydGlmaWNhdGVfdHlwZRgBIAEoDjIxLmNlcnRpZmljYXRl",
            "X3VzZV9lbnVtX3R5cGUuQ2VydGlmaWNhdGVVc2VFbnVtVHlwZRITCgtjZXJ0",
            "aWZpY2F0ZRgCIAEoCSLKAQohSW5zdGFsbENlcnRpZmljYXRlQ29tbWFuZFJl",
            "c3BvbnNlEmAKBnN0YXR1cxgBIAEoDjJQLmluc3RhbGxfY2VydGlmaWNhdGUu",
            "SW5zdGFsbENlcnRpZmljYXRlQ29tbWFuZFJlc3BvbnNlLkNlcnRpZmljYXRl",
            "U3RhdHVzRW51bVR5cGUiQwoZQ2VydGlmaWNhdGVTdGF0dXNFbnVtVHlwZRIM",
            "CghBY2NlcHRlZBAAEgoKBkZhaWxlZBABEgwKCFJlamVjdGVkEAJCkwEKImNv",
            "bS5jb2RpYmx5Lm1vZGVsLmNvbW1hbmQuc2VjdXJpdHlCF0luc3RhbGxDZXJ0",
            "aWZpY2F0ZVByb3RvygIiRG9tYWluTW9kZWxcUHJvdG9cQ29tbWFuZFxTZWN1",
            "cml0eeICLkRvbWFpbk1vZGVsXFByb3RvXENvbW1hbmRcU2VjdXJpdHlcR1BC",
            "TWV0YWRhdGFiBnByb3RvMw=="));
      descriptor = pbr::FileDescriptor.FromGeneratedCode(descriptorData,
          new pbr::FileDescriptor[] { global::CertificateUseEnumType.CertificateUseEnumTypeReflection.Descriptor, },
          new pbr::GeneratedClrTypeInfo(null, null, new pbr::GeneratedClrTypeInfo[] {
            new pbr::GeneratedClrTypeInfo(typeof(global::InstallCertificate.InstallCertificateCommand), global::InstallCertificate.InstallCertificateCommand.Parser, new[]{ "CertificateType", "Certificate" }, null, null, null, null),
            new pbr::GeneratedClrTypeInfo(typeof(global::InstallCertificate.InstallCertificateCommandResponse), global::InstallCertificate.InstallCertificateCommandResponse.Parser, new[]{ "Status" }, null, new[]{ typeof(global::InstallCertificate.InstallCertificateCommandResponse.Types.CertificateStatusEnumType) }, null, null)
          }));
    }
    #endregion

  }
  #region Messages
  public sealed partial class InstallCertificateCommand : pb::IMessage<InstallCertificateCommand>
  #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
      , pb::IBufferMessage
  #endif
  {
    private static readonly pb::MessageParser<InstallCertificateCommand> _parser = new pb::MessageParser<InstallCertificateCommand>(() => new InstallCertificateCommand());
    private pb::UnknownFieldSet _unknownFields;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public static pb::MessageParser<InstallCertificateCommand> Parser { get { return _parser; } }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public static pbr::MessageDescriptor Descriptor {
      get { return global::InstallCertificate.InstallCertificateReflection.Descriptor.MessageTypes[0]; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    pbr::MessageDescriptor pb::IMessage.Descriptor {
      get { return Descriptor; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public InstallCertificateCommand() {
      OnConstruction();
    }

    partial void OnConstruction();

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public InstallCertificateCommand(InstallCertificateCommand other) : this() {
      certificateType_ = other.certificateType_;
      certificate_ = other.certificate_;
      _unknownFields = pb::UnknownFieldSet.Clone(other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public InstallCertificateCommand Clone() {
      return new InstallCertificateCommand(this);
    }

    /// <summary>Field number for the "certificate_type" field.</summary>
    public const int CertificateTypeFieldNumber = 1;
    private global::CertificateUseEnumType.CertificateUseEnumType certificateType_ = global::CertificateUseEnumType.CertificateUseEnumType.CentralSystemRootCertificate;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public global::CertificateUseEnumType.CertificateUseEnumType CertificateType {
      get { return certificateType_; }
      set {
        certificateType_ = value;
      }
    }

    /// <summary>Field number for the "certificate" field.</summary>
    public const int CertificateFieldNumber = 2;
    private string certificate_ = "";
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public string Certificate {
      get { return certificate_; }
      set {
        certificate_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public override bool Equals(object other) {
      return Equals(other as InstallCertificateCommand);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public bool Equals(InstallCertificateCommand other) {
      if (ReferenceEquals(other, null)) {
        return false;
      }
      if (ReferenceEquals(other, this)) {
        return true;
      }
      if (CertificateType != other.CertificateType) return false;
      if (Certificate != other.Certificate) return false;
      return Equals(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public override int GetHashCode() {
      int hash = 1;
      if (CertificateType != global::CertificateUseEnumType.CertificateUseEnumType.CentralSystemRootCertificate) hash ^= CertificateType.GetHashCode();
      if (Certificate.Length != 0) hash ^= Certificate.GetHashCode();
      if (_unknownFields != null) {
        hash ^= _unknownFields.GetHashCode();
      }
      return hash;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public override string ToString() {
      return pb::JsonFormatter.ToDiagnosticString(this);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void WriteTo(pb::CodedOutputStream output) {
    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
      output.WriteRawMessage(this);
    #else
      if (CertificateType != global::CertificateUseEnumType.CertificateUseEnumType.CentralSystemRootCertificate) {
        output.WriteRawTag(8);
        output.WriteEnum((int) CertificateType);
      }
      if (Certificate.Length != 0) {
        output.WriteRawTag(18);
        output.WriteString(Certificate);
      }
      if (_unknownFields != null) {
        _unknownFields.WriteTo(output);
      }
    #endif
    }

    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    void pb::IBufferMessage.InternalWriteTo(ref pb::WriteContext output) {
      if (CertificateType != global::CertificateUseEnumType.CertificateUseEnumType.CentralSystemRootCertificate) {
        output.WriteRawTag(8);
        output.WriteEnum((int) CertificateType);
      }
      if (Certificate.Length != 0) {
        output.WriteRawTag(18);
        output.WriteString(Certificate);
      }
      if (_unknownFields != null) {
        _unknownFields.WriteTo(ref output);
      }
    }
    #endif

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public int CalculateSize() {
      int size = 0;
      if (CertificateType != global::CertificateUseEnumType.CertificateUseEnumType.CentralSystemRootCertificate) {
        size += 1 + pb::CodedOutputStream.ComputeEnumSize((int) CertificateType);
      }
      if (Certificate.Length != 0) {
        size += 1 + pb::CodedOutputStream.ComputeStringSize(Certificate);
      }
      if (_unknownFields != null) {
        size += _unknownFields.CalculateSize();
      }
      return size;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void MergeFrom(InstallCertificateCommand other) {
      if (other == null) {
        return;
      }
      if (other.CertificateType != global::CertificateUseEnumType.CertificateUseEnumType.CentralSystemRootCertificate) {
        CertificateType = other.CertificateType;
      }
      if (other.Certificate.Length != 0) {
        Certificate = other.Certificate;
      }
      _unknownFields = pb::UnknownFieldSet.MergeFrom(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void MergeFrom(pb::CodedInputStream input) {
    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
      input.ReadRawMessage(this);
    #else
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            _unknownFields = pb::UnknownFieldSet.MergeFieldFrom(_unknownFields, input);
            break;
          case 8: {
            CertificateType = (global::CertificateUseEnumType.CertificateUseEnumType) input.ReadEnum();
            break;
          }
          case 18: {
            Certificate = input.ReadString();
            break;
          }
        }
      }
    #endif
    }

    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    void pb::IBufferMessage.InternalMergeFrom(ref pb::ParseContext input) {
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            _unknownFields = pb::UnknownFieldSet.MergeFieldFrom(_unknownFields, ref input);
            break;
          case 8: {
            CertificateType = (global::CertificateUseEnumType.CertificateUseEnumType) input.ReadEnum();
            break;
          }
          case 18: {
            Certificate = input.ReadString();
            break;
          }
        }
      }
    }
    #endif

  }

  public sealed partial class InstallCertificateCommandResponse : pb::IMessage<InstallCertificateCommandResponse>
  #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
      , pb::IBufferMessage
  #endif
  {
    private static readonly pb::MessageParser<InstallCertificateCommandResponse> _parser = new pb::MessageParser<InstallCertificateCommandResponse>(() => new InstallCertificateCommandResponse());
    private pb::UnknownFieldSet _unknownFields;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public static pb::MessageParser<InstallCertificateCommandResponse> Parser { get { return _parser; } }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public static pbr::MessageDescriptor Descriptor {
      get { return global::InstallCertificate.InstallCertificateReflection.Descriptor.MessageTypes[1]; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    pbr::MessageDescriptor pb::IMessage.Descriptor {
      get { return Descriptor; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public InstallCertificateCommandResponse() {
      OnConstruction();
    }

    partial void OnConstruction();

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public InstallCertificateCommandResponse(InstallCertificateCommandResponse other) : this() {
      status_ = other.status_;
      _unknownFields = pb::UnknownFieldSet.Clone(other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public InstallCertificateCommandResponse Clone() {
      return new InstallCertificateCommandResponse(this);
    }

    /// <summary>Field number for the "status" field.</summary>
    public const int StatusFieldNumber = 1;
    private global::InstallCertificate.InstallCertificateCommandResponse.Types.CertificateStatusEnumType status_ = global::InstallCertificate.InstallCertificateCommandResponse.Types.CertificateStatusEnumType.Accepted;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public global::InstallCertificate.InstallCertificateCommandResponse.Types.CertificateStatusEnumType Status {
      get { return status_; }
      set {
        status_ = value;
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public override bool Equals(object other) {
      return Equals(other as InstallCertificateCommandResponse);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public bool Equals(InstallCertificateCommandResponse other) {
      if (ReferenceEquals(other, null)) {
        return false;
      }
      if (ReferenceEquals(other, this)) {
        return true;
      }
      if (Status != other.Status) return false;
      return Equals(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public override int GetHashCode() {
      int hash = 1;
      if (Status != global::InstallCertificate.InstallCertificateCommandResponse.Types.CertificateStatusEnumType.Accepted) hash ^= Status.GetHashCode();
      if (_unknownFields != null) {
        hash ^= _unknownFields.GetHashCode();
      }
      return hash;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public override string ToString() {
      return pb::JsonFormatter.ToDiagnosticString(this);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void WriteTo(pb::CodedOutputStream output) {
    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
      output.WriteRawMessage(this);
    #else
      if (Status != global::InstallCertificate.InstallCertificateCommandResponse.Types.CertificateStatusEnumType.Accepted) {
        output.WriteRawTag(8);
        output.WriteEnum((int) Status);
      }
      if (_unknownFields != null) {
        _unknownFields.WriteTo(output);
      }
    #endif
    }

    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    void pb::IBufferMessage.InternalWriteTo(ref pb::WriteContext output) {
      if (Status != global::InstallCertificate.InstallCertificateCommandResponse.Types.CertificateStatusEnumType.Accepted) {
        output.WriteRawTag(8);
        output.WriteEnum((int) Status);
      }
      if (_unknownFields != null) {
        _unknownFields.WriteTo(ref output);
      }
    }
    #endif

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public int CalculateSize() {
      int size = 0;
      if (Status != global::InstallCertificate.InstallCertificateCommandResponse.Types.CertificateStatusEnumType.Accepted) {
        size += 1 + pb::CodedOutputStream.ComputeEnumSize((int) Status);
      }
      if (_unknownFields != null) {
        size += _unknownFields.CalculateSize();
      }
      return size;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void MergeFrom(InstallCertificateCommandResponse other) {
      if (other == null) {
        return;
      }
      if (other.Status != global::InstallCertificate.InstallCertificateCommandResponse.Types.CertificateStatusEnumType.Accepted) {
        Status = other.Status;
      }
      _unknownFields = pb::UnknownFieldSet.MergeFrom(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void MergeFrom(pb::CodedInputStream input) {
    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
      input.ReadRawMessage(this);
    #else
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            _unknownFields = pb::UnknownFieldSet.MergeFieldFrom(_unknownFields, input);
            break;
          case 8: {
            Status = (global::InstallCertificate.InstallCertificateCommandResponse.Types.CertificateStatusEnumType) input.ReadEnum();
            break;
          }
        }
      }
    #endif
    }

    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    void pb::IBufferMessage.InternalMergeFrom(ref pb::ParseContext input) {
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            _unknownFields = pb::UnknownFieldSet.MergeFieldFrom(_unknownFields, ref input);
            break;
          case 8: {
            Status = (global::InstallCertificate.InstallCertificateCommandResponse.Types.CertificateStatusEnumType) input.ReadEnum();
            break;
          }
        }
      }
    }
    #endif

    #region Nested types
    /// <summary>Container for nested types declared in the InstallCertificateCommandResponse message type.</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public static partial class Types {
      public enum CertificateStatusEnumType {
        [pbr::OriginalName("Accepted")] Accepted = 0,
        [pbr::OriginalName("Failed")] Failed = 1,
        [pbr::OriginalName("Rejected")] Rejected = 2,
      }

    }
    #endregion

  }

  #endregion

}

#endregion Designer generated code

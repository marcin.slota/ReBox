// <auto-generated>
//     Generated by the protocol buffer compiler.  DO NOT EDIT!
//     source: generic/State.proto
// </auto-generated>
#pragma warning disable 1591, 0612, 3021, 8981
#region Designer generated code

using pb = global::Google.Protobuf;
using pbc = global::Google.Protobuf.Collections;
using pbr = global::Google.Protobuf.Reflection;
using scg = global::System.Collections.Generic;
namespace Generic {

  /// <summary>Holder for reflection information generated from generic/State.proto</summary>
  public static partial class StateReflection {

    #region Descriptor
    /// <summary>File descriptor for generic/State.proto</summary>
    public static pbr::FileDescriptor Descriptor {
      get { return descriptor; }
    }
    private static pbr::FileDescriptor descriptor;

    static StateReflection() {
      byte[] descriptorData = global::System.Convert.FromBase64String(
          string.Concat(
            "ChNnZW5lcmljL1N0YXRlLnByb3RvEgdnZW5lcmljIuUFCgVTdGF0ZRIgCg9z",
            "dGF0ZV9vZl9jaGFyZ2UYAiABKAlCAhgBSACIAQESFwoGY2hhcmdlGAMgASgJ",
            "QgIYAUgBiAEBEhoKCWRpc2NoYXJnZRgEIAEoCUICGAFIAogBARIaCg1zZXJp",
            "YWxfbnVtYmVyGAUgASgJSAOIAQESGQoMbWFudWZhY3R1cmVyGAYgASgJSASI",
            "AQESEgoFbW9kZWwYByABKAlIBYgBARITCgZzdGF0dXMYCCABKAlIBogBARId",
            "ChBvcGVyYXRpb25hbF9tb2RlGAkgASgJSAeIAQESFwoKaXBfYWRkcmVzcxgK",
            "IAEoCUgIiAEBEh0KEGZpcm13YXJlX3ZlcnNpb24YCyABKAlICYgBARIdChBo",
            "YXJkd2FyZV92ZXJzaW9uGAwgASgJSAqIAQESGwoOZ3JpZF9jb25uZWN0ZWQY",
            "DSABKAhIC4gBARIYCgtkZXZpY2VfdGltZRgOIAEoCUgMiAEBEhsKDmRhaWx5",
            "X3dvcmt0aW1lGA8gASgJSA2IAQESHQoQcG93ZXJfbGltaXRhdGlvbhgQIAEo",
            "CEgOiAEBEiMKFnBvd2VyX2xpbWl0YXRpb25fdmFsdWUYESABKAlID4gBAUIS",
            "ChBfc3RhdGVfb2ZfY2hhcmdlQgkKB19jaGFyZ2VCDAoKX2Rpc2NoYXJnZUIQ",
            "Cg5fc2VyaWFsX251bWJlckIPCg1fbWFudWZhY3R1cmVyQggKBl9tb2RlbEIJ",
            "Cgdfc3RhdHVzQhMKEV9vcGVyYXRpb25hbF9tb2RlQg0KC19pcF9hZGRyZXNz",
            "QhMKEV9maXJtd2FyZV92ZXJzaW9uQhMKEV9oYXJkd2FyZV92ZXJzaW9uQhEK",
            "D19ncmlkX2Nvbm5lY3RlZEIOCgxfZGV2aWNlX3RpbWVCEQoPX2RhaWx5X3dv",
            "cmt0aW1lQhMKEV9wb3dlcl9saW1pdGF0aW9uQhkKF19wb3dlcl9saW1pdGF0",
            "aW9uX3ZhbHVlQmsKGWNvbS5jb2RpYmx5Lm1vZGVsLmdlbmVyaWNCClN0YXRl",
            "UHJvdG/KAhlEb21haW5Nb2RlbFxQcm90b1xHZW5lcmlj4gIlRG9tYWluTW9k",
            "ZWxcUHJvdG9cR2VuZXJpY1xHUEJNZXRhZGF0YWIGcHJvdG8z"));
      descriptor = pbr::FileDescriptor.FromGeneratedCode(descriptorData,
          new pbr::FileDescriptor[] { },
          new pbr::GeneratedClrTypeInfo(null, null, new pbr::GeneratedClrTypeInfo[] {
            new pbr::GeneratedClrTypeInfo(typeof(global::Generic.State), global::Generic.State.Parser, new[]{ "StateOfCharge", "Charge", "Discharge", "SerialNumber", "Manufacturer", "Model", "Status", "OperationalMode", "IpAddress", "FirmwareVersion", "HardwareVersion", "GridConnected", "DeviceTime", "DailyWorktime", "PowerLimitation", "PowerLimitationValue" }, new[]{ "StateOfCharge", "Charge", "Discharge", "SerialNumber", "Manufacturer", "Model", "Status", "OperationalMode", "IpAddress", "FirmwareVersion", "HardwareVersion", "GridConnected", "DeviceTime", "DailyWorktime", "PowerLimitation", "PowerLimitationValue" }, null, null, null)
          }));
    }
    #endregion

  }
  #region Messages
  public sealed partial class State : pb::IMessage<State>
  #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
      , pb::IBufferMessage
  #endif
  {
    private static readonly pb::MessageParser<State> _parser = new pb::MessageParser<State>(() => new State());
    private pb::UnknownFieldSet _unknownFields;
    private int _hasBits0;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public static pb::MessageParser<State> Parser { get { return _parser; } }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public static pbr::MessageDescriptor Descriptor {
      get { return global::Generic.StateReflection.Descriptor.MessageTypes[0]; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    pbr::MessageDescriptor pb::IMessage.Descriptor {
      get { return Descriptor; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public State() {
      OnConstruction();
    }

    partial void OnConstruction();

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public State(State other) : this() {
      _hasBits0 = other._hasBits0;
      stateOfCharge_ = other.stateOfCharge_;
      charge_ = other.charge_;
      discharge_ = other.discharge_;
      serialNumber_ = other.serialNumber_;
      manufacturer_ = other.manufacturer_;
      model_ = other.model_;
      status_ = other.status_;
      operationalMode_ = other.operationalMode_;
      ipAddress_ = other.ipAddress_;
      firmwareVersion_ = other.firmwareVersion_;
      hardwareVersion_ = other.hardwareVersion_;
      gridConnected_ = other.gridConnected_;
      deviceTime_ = other.deviceTime_;
      dailyWorktime_ = other.dailyWorktime_;
      powerLimitation_ = other.powerLimitation_;
      powerLimitationValue_ = other.powerLimitationValue_;
      _unknownFields = pb::UnknownFieldSet.Clone(other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public State Clone() {
      return new State(this);
    }

    /// <summary>Field number for the "state_of_charge" field.</summary>
    public const int StateOfChargeFieldNumber = 2;
    private string stateOfCharge_;
    [global::System.ObsoleteAttribute]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public string StateOfCharge {
      get { return stateOfCharge_ ?? ""; }
      set {
        stateOfCharge_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
      }
    }
    /// <summary>Gets whether the "state_of_charge" field is set</summary>
    [global::System.ObsoleteAttribute]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public bool HasStateOfCharge {
      get { return stateOfCharge_ != null; }
    }
    /// <summary>Clears the value of the "state_of_charge" field</summary>
    [global::System.ObsoleteAttribute]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void ClearStateOfCharge() {
      stateOfCharge_ = null;
    }

    /// <summary>Field number for the "charge" field.</summary>
    public const int ChargeFieldNumber = 3;
    private string charge_;
    [global::System.ObsoleteAttribute]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public string Charge {
      get { return charge_ ?? ""; }
      set {
        charge_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
      }
    }
    /// <summary>Gets whether the "charge" field is set</summary>
    [global::System.ObsoleteAttribute]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public bool HasCharge {
      get { return charge_ != null; }
    }
    /// <summary>Clears the value of the "charge" field</summary>
    [global::System.ObsoleteAttribute]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void ClearCharge() {
      charge_ = null;
    }

    /// <summary>Field number for the "discharge" field.</summary>
    public const int DischargeFieldNumber = 4;
    private string discharge_;
    [global::System.ObsoleteAttribute]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public string Discharge {
      get { return discharge_ ?? ""; }
      set {
        discharge_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
      }
    }
    /// <summary>Gets whether the "discharge" field is set</summary>
    [global::System.ObsoleteAttribute]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public bool HasDischarge {
      get { return discharge_ != null; }
    }
    /// <summary>Clears the value of the "discharge" field</summary>
    [global::System.ObsoleteAttribute]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void ClearDischarge() {
      discharge_ = null;
    }

    /// <summary>Field number for the "serial_number" field.</summary>
    public const int SerialNumberFieldNumber = 5;
    private string serialNumber_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public string SerialNumber {
      get { return serialNumber_ ?? ""; }
      set {
        serialNumber_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
      }
    }
    /// <summary>Gets whether the "serial_number" field is set</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public bool HasSerialNumber {
      get { return serialNumber_ != null; }
    }
    /// <summary>Clears the value of the "serial_number" field</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void ClearSerialNumber() {
      serialNumber_ = null;
    }

    /// <summary>Field number for the "manufacturer" field.</summary>
    public const int ManufacturerFieldNumber = 6;
    private string manufacturer_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public string Manufacturer {
      get { return manufacturer_ ?? ""; }
      set {
        manufacturer_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
      }
    }
    /// <summary>Gets whether the "manufacturer" field is set</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public bool HasManufacturer {
      get { return manufacturer_ != null; }
    }
    /// <summary>Clears the value of the "manufacturer" field</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void ClearManufacturer() {
      manufacturer_ = null;
    }

    /// <summary>Field number for the "model" field.</summary>
    public const int ModelFieldNumber = 7;
    private string model_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public string Model {
      get { return model_ ?? ""; }
      set {
        model_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
      }
    }
    /// <summary>Gets whether the "model" field is set</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public bool HasModel {
      get { return model_ != null; }
    }
    /// <summary>Clears the value of the "model" field</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void ClearModel() {
      model_ = null;
    }

    /// <summary>Field number for the "status" field.</summary>
    public const int StatusFieldNumber = 8;
    private string status_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public string Status {
      get { return status_ ?? ""; }
      set {
        status_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
      }
    }
    /// <summary>Gets whether the "status" field is set</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public bool HasStatus {
      get { return status_ != null; }
    }
    /// <summary>Clears the value of the "status" field</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void ClearStatus() {
      status_ = null;
    }

    /// <summary>Field number for the "operational_mode" field.</summary>
    public const int OperationalModeFieldNumber = 9;
    private string operationalMode_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public string OperationalMode {
      get { return operationalMode_ ?? ""; }
      set {
        operationalMode_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
      }
    }
    /// <summary>Gets whether the "operational_mode" field is set</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public bool HasOperationalMode {
      get { return operationalMode_ != null; }
    }
    /// <summary>Clears the value of the "operational_mode" field</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void ClearOperationalMode() {
      operationalMode_ = null;
    }

    /// <summary>Field number for the "ip_address" field.</summary>
    public const int IpAddressFieldNumber = 10;
    private string ipAddress_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public string IpAddress {
      get { return ipAddress_ ?? ""; }
      set {
        ipAddress_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
      }
    }
    /// <summary>Gets whether the "ip_address" field is set</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public bool HasIpAddress {
      get { return ipAddress_ != null; }
    }
    /// <summary>Clears the value of the "ip_address" field</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void ClearIpAddress() {
      ipAddress_ = null;
    }

    /// <summary>Field number for the "firmware_version" field.</summary>
    public const int FirmwareVersionFieldNumber = 11;
    private string firmwareVersion_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public string FirmwareVersion {
      get { return firmwareVersion_ ?? ""; }
      set {
        firmwareVersion_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
      }
    }
    /// <summary>Gets whether the "firmware_version" field is set</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public bool HasFirmwareVersion {
      get { return firmwareVersion_ != null; }
    }
    /// <summary>Clears the value of the "firmware_version" field</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void ClearFirmwareVersion() {
      firmwareVersion_ = null;
    }

    /// <summary>Field number for the "hardware_version" field.</summary>
    public const int HardwareVersionFieldNumber = 12;
    private string hardwareVersion_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public string HardwareVersion {
      get { return hardwareVersion_ ?? ""; }
      set {
        hardwareVersion_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
      }
    }
    /// <summary>Gets whether the "hardware_version" field is set</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public bool HasHardwareVersion {
      get { return hardwareVersion_ != null; }
    }
    /// <summary>Clears the value of the "hardware_version" field</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void ClearHardwareVersion() {
      hardwareVersion_ = null;
    }

    /// <summary>Field number for the "grid_connected" field.</summary>
    public const int GridConnectedFieldNumber = 13;
    private bool gridConnected_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public bool GridConnected {
      get { if ((_hasBits0 & 1) != 0) { return gridConnected_; } else { return false; } }
      set {
        _hasBits0 |= 1;
        gridConnected_ = value;
      }
    }
    /// <summary>Gets whether the "grid_connected" field is set</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public bool HasGridConnected {
      get { return (_hasBits0 & 1) != 0; }
    }
    /// <summary>Clears the value of the "grid_connected" field</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void ClearGridConnected() {
      _hasBits0 &= ~1;
    }

    /// <summary>Field number for the "device_time" field.</summary>
    public const int DeviceTimeFieldNumber = 14;
    private string deviceTime_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public string DeviceTime {
      get { return deviceTime_ ?? ""; }
      set {
        deviceTime_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
      }
    }
    /// <summary>Gets whether the "device_time" field is set</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public bool HasDeviceTime {
      get { return deviceTime_ != null; }
    }
    /// <summary>Clears the value of the "device_time" field</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void ClearDeviceTime() {
      deviceTime_ = null;
    }

    /// <summary>Field number for the "daily_worktime" field.</summary>
    public const int DailyWorktimeFieldNumber = 15;
    private string dailyWorktime_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public string DailyWorktime {
      get { return dailyWorktime_ ?? ""; }
      set {
        dailyWorktime_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
      }
    }
    /// <summary>Gets whether the "daily_worktime" field is set</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public bool HasDailyWorktime {
      get { return dailyWorktime_ != null; }
    }
    /// <summary>Clears the value of the "daily_worktime" field</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void ClearDailyWorktime() {
      dailyWorktime_ = null;
    }

    /// <summary>Field number for the "power_limitation" field.</summary>
    public const int PowerLimitationFieldNumber = 16;
    private bool powerLimitation_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public bool PowerLimitation {
      get { if ((_hasBits0 & 2) != 0) { return powerLimitation_; } else { return false; } }
      set {
        _hasBits0 |= 2;
        powerLimitation_ = value;
      }
    }
    /// <summary>Gets whether the "power_limitation" field is set</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public bool HasPowerLimitation {
      get { return (_hasBits0 & 2) != 0; }
    }
    /// <summary>Clears the value of the "power_limitation" field</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void ClearPowerLimitation() {
      _hasBits0 &= ~2;
    }

    /// <summary>Field number for the "power_limitation_value" field.</summary>
    public const int PowerLimitationValueFieldNumber = 17;
    private string powerLimitationValue_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public string PowerLimitationValue {
      get { return powerLimitationValue_ ?? ""; }
      set {
        powerLimitationValue_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
      }
    }
    /// <summary>Gets whether the "power_limitation_value" field is set</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public bool HasPowerLimitationValue {
      get { return powerLimitationValue_ != null; }
    }
    /// <summary>Clears the value of the "power_limitation_value" field</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void ClearPowerLimitationValue() {
      powerLimitationValue_ = null;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public override bool Equals(object other) {
      return Equals(other as State);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public bool Equals(State other) {
      if (ReferenceEquals(other, null)) {
        return false;
      }
      if (ReferenceEquals(other, this)) {
        return true;
      }
      if (StateOfCharge != other.StateOfCharge) return false;
      if (Charge != other.Charge) return false;
      if (Discharge != other.Discharge) return false;
      if (SerialNumber != other.SerialNumber) return false;
      if (Manufacturer != other.Manufacturer) return false;
      if (Model != other.Model) return false;
      if (Status != other.Status) return false;
      if (OperationalMode != other.OperationalMode) return false;
      if (IpAddress != other.IpAddress) return false;
      if (FirmwareVersion != other.FirmwareVersion) return false;
      if (HardwareVersion != other.HardwareVersion) return false;
      if (GridConnected != other.GridConnected) return false;
      if (DeviceTime != other.DeviceTime) return false;
      if (DailyWorktime != other.DailyWorktime) return false;
      if (PowerLimitation != other.PowerLimitation) return false;
      if (PowerLimitationValue != other.PowerLimitationValue) return false;
      return Equals(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public override int GetHashCode() {
      int hash = 1;
      if (HasStateOfCharge) hash ^= StateOfCharge.GetHashCode();
      if (HasCharge) hash ^= Charge.GetHashCode();
      if (HasDischarge) hash ^= Discharge.GetHashCode();
      if (HasSerialNumber) hash ^= SerialNumber.GetHashCode();
      if (HasManufacturer) hash ^= Manufacturer.GetHashCode();
      if (HasModel) hash ^= Model.GetHashCode();
      if (HasStatus) hash ^= Status.GetHashCode();
      if (HasOperationalMode) hash ^= OperationalMode.GetHashCode();
      if (HasIpAddress) hash ^= IpAddress.GetHashCode();
      if (HasFirmwareVersion) hash ^= FirmwareVersion.GetHashCode();
      if (HasHardwareVersion) hash ^= HardwareVersion.GetHashCode();
      if (HasGridConnected) hash ^= GridConnected.GetHashCode();
      if (HasDeviceTime) hash ^= DeviceTime.GetHashCode();
      if (HasDailyWorktime) hash ^= DailyWorktime.GetHashCode();
      if (HasPowerLimitation) hash ^= PowerLimitation.GetHashCode();
      if (HasPowerLimitationValue) hash ^= PowerLimitationValue.GetHashCode();
      if (_unknownFields != null) {
        hash ^= _unknownFields.GetHashCode();
      }
      return hash;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public override string ToString() {
      return pb::JsonFormatter.ToDiagnosticString(this);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void WriteTo(pb::CodedOutputStream output) {
    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
      output.WriteRawMessage(this);
    #else
      if (HasStateOfCharge) {
        output.WriteRawTag(18);
        output.WriteString(StateOfCharge);
      }
      if (HasCharge) {
        output.WriteRawTag(26);
        output.WriteString(Charge);
      }
      if (HasDischarge) {
        output.WriteRawTag(34);
        output.WriteString(Discharge);
      }
      if (HasSerialNumber) {
        output.WriteRawTag(42);
        output.WriteString(SerialNumber);
      }
      if (HasManufacturer) {
        output.WriteRawTag(50);
        output.WriteString(Manufacturer);
      }
      if (HasModel) {
        output.WriteRawTag(58);
        output.WriteString(Model);
      }
      if (HasStatus) {
        output.WriteRawTag(66);
        output.WriteString(Status);
      }
      if (HasOperationalMode) {
        output.WriteRawTag(74);
        output.WriteString(OperationalMode);
      }
      if (HasIpAddress) {
        output.WriteRawTag(82);
        output.WriteString(IpAddress);
      }
      if (HasFirmwareVersion) {
        output.WriteRawTag(90);
        output.WriteString(FirmwareVersion);
      }
      if (HasHardwareVersion) {
        output.WriteRawTag(98);
        output.WriteString(HardwareVersion);
      }
      if (HasGridConnected) {
        output.WriteRawTag(104);
        output.WriteBool(GridConnected);
      }
      if (HasDeviceTime) {
        output.WriteRawTag(114);
        output.WriteString(DeviceTime);
      }
      if (HasDailyWorktime) {
        output.WriteRawTag(122);
        output.WriteString(DailyWorktime);
      }
      if (HasPowerLimitation) {
        output.WriteRawTag(128, 1);
        output.WriteBool(PowerLimitation);
      }
      if (HasPowerLimitationValue) {
        output.WriteRawTag(138, 1);
        output.WriteString(PowerLimitationValue);
      }
      if (_unknownFields != null) {
        _unknownFields.WriteTo(output);
      }
    #endif
    }

    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    void pb::IBufferMessage.InternalWriteTo(ref pb::WriteContext output) {
      if (HasStateOfCharge) {
        output.WriteRawTag(18);
        output.WriteString(StateOfCharge);
      }
      if (HasCharge) {
        output.WriteRawTag(26);
        output.WriteString(Charge);
      }
      if (HasDischarge) {
        output.WriteRawTag(34);
        output.WriteString(Discharge);
      }
      if (HasSerialNumber) {
        output.WriteRawTag(42);
        output.WriteString(SerialNumber);
      }
      if (HasManufacturer) {
        output.WriteRawTag(50);
        output.WriteString(Manufacturer);
      }
      if (HasModel) {
        output.WriteRawTag(58);
        output.WriteString(Model);
      }
      if (HasStatus) {
        output.WriteRawTag(66);
        output.WriteString(Status);
      }
      if (HasOperationalMode) {
        output.WriteRawTag(74);
        output.WriteString(OperationalMode);
      }
      if (HasIpAddress) {
        output.WriteRawTag(82);
        output.WriteString(IpAddress);
      }
      if (HasFirmwareVersion) {
        output.WriteRawTag(90);
        output.WriteString(FirmwareVersion);
      }
      if (HasHardwareVersion) {
        output.WriteRawTag(98);
        output.WriteString(HardwareVersion);
      }
      if (HasGridConnected) {
        output.WriteRawTag(104);
        output.WriteBool(GridConnected);
      }
      if (HasDeviceTime) {
        output.WriteRawTag(114);
        output.WriteString(DeviceTime);
      }
      if (HasDailyWorktime) {
        output.WriteRawTag(122);
        output.WriteString(DailyWorktime);
      }
      if (HasPowerLimitation) {
        output.WriteRawTag(128, 1);
        output.WriteBool(PowerLimitation);
      }
      if (HasPowerLimitationValue) {
        output.WriteRawTag(138, 1);
        output.WriteString(PowerLimitationValue);
      }
      if (_unknownFields != null) {
        _unknownFields.WriteTo(ref output);
      }
    }
    #endif

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public int CalculateSize() {
      int size = 0;
      if (HasStateOfCharge) {
        size += 1 + pb::CodedOutputStream.ComputeStringSize(StateOfCharge);
      }
      if (HasCharge) {
        size += 1 + pb::CodedOutputStream.ComputeStringSize(Charge);
      }
      if (HasDischarge) {
        size += 1 + pb::CodedOutputStream.ComputeStringSize(Discharge);
      }
      if (HasSerialNumber) {
        size += 1 + pb::CodedOutputStream.ComputeStringSize(SerialNumber);
      }
      if (HasManufacturer) {
        size += 1 + pb::CodedOutputStream.ComputeStringSize(Manufacturer);
      }
      if (HasModel) {
        size += 1 + pb::CodedOutputStream.ComputeStringSize(Model);
      }
      if (HasStatus) {
        size += 1 + pb::CodedOutputStream.ComputeStringSize(Status);
      }
      if (HasOperationalMode) {
        size += 1 + pb::CodedOutputStream.ComputeStringSize(OperationalMode);
      }
      if (HasIpAddress) {
        size += 1 + pb::CodedOutputStream.ComputeStringSize(IpAddress);
      }
      if (HasFirmwareVersion) {
        size += 1 + pb::CodedOutputStream.ComputeStringSize(FirmwareVersion);
      }
      if (HasHardwareVersion) {
        size += 1 + pb::CodedOutputStream.ComputeStringSize(HardwareVersion);
      }
      if (HasGridConnected) {
        size += 1 + 1;
      }
      if (HasDeviceTime) {
        size += 1 + pb::CodedOutputStream.ComputeStringSize(DeviceTime);
      }
      if (HasDailyWorktime) {
        size += 1 + pb::CodedOutputStream.ComputeStringSize(DailyWorktime);
      }
      if (HasPowerLimitation) {
        size += 2 + 1;
      }
      if (HasPowerLimitationValue) {
        size += 2 + pb::CodedOutputStream.ComputeStringSize(PowerLimitationValue);
      }
      if (_unknownFields != null) {
        size += _unknownFields.CalculateSize();
      }
      return size;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void MergeFrom(State other) {
      if (other == null) {
        return;
      }
      if (other.HasStateOfCharge) {
        StateOfCharge = other.StateOfCharge;
      }
      if (other.HasCharge) {
        Charge = other.Charge;
      }
      if (other.HasDischarge) {
        Discharge = other.Discharge;
      }
      if (other.HasSerialNumber) {
        SerialNumber = other.SerialNumber;
      }
      if (other.HasManufacturer) {
        Manufacturer = other.Manufacturer;
      }
      if (other.HasModel) {
        Model = other.Model;
      }
      if (other.HasStatus) {
        Status = other.Status;
      }
      if (other.HasOperationalMode) {
        OperationalMode = other.OperationalMode;
      }
      if (other.HasIpAddress) {
        IpAddress = other.IpAddress;
      }
      if (other.HasFirmwareVersion) {
        FirmwareVersion = other.FirmwareVersion;
      }
      if (other.HasHardwareVersion) {
        HardwareVersion = other.HardwareVersion;
      }
      if (other.HasGridConnected) {
        GridConnected = other.GridConnected;
      }
      if (other.HasDeviceTime) {
        DeviceTime = other.DeviceTime;
      }
      if (other.HasDailyWorktime) {
        DailyWorktime = other.DailyWorktime;
      }
      if (other.HasPowerLimitation) {
        PowerLimitation = other.PowerLimitation;
      }
      if (other.HasPowerLimitationValue) {
        PowerLimitationValue = other.PowerLimitationValue;
      }
      _unknownFields = pb::UnknownFieldSet.MergeFrom(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void MergeFrom(pb::CodedInputStream input) {
    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
      input.ReadRawMessage(this);
    #else
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            _unknownFields = pb::UnknownFieldSet.MergeFieldFrom(_unknownFields, input);
            break;
          case 18: {
            StateOfCharge = input.ReadString();
            break;
          }
          case 26: {
            Charge = input.ReadString();
            break;
          }
          case 34: {
            Discharge = input.ReadString();
            break;
          }
          case 42: {
            SerialNumber = input.ReadString();
            break;
          }
          case 50: {
            Manufacturer = input.ReadString();
            break;
          }
          case 58: {
            Model = input.ReadString();
            break;
          }
          case 66: {
            Status = input.ReadString();
            break;
          }
          case 74: {
            OperationalMode = input.ReadString();
            break;
          }
          case 82: {
            IpAddress = input.ReadString();
            break;
          }
          case 90: {
            FirmwareVersion = input.ReadString();
            break;
          }
          case 98: {
            HardwareVersion = input.ReadString();
            break;
          }
          case 104: {
            GridConnected = input.ReadBool();
            break;
          }
          case 114: {
            DeviceTime = input.ReadString();
            break;
          }
          case 122: {
            DailyWorktime = input.ReadString();
            break;
          }
          case 128: {
            PowerLimitation = input.ReadBool();
            break;
          }
          case 138: {
            PowerLimitationValue = input.ReadString();
            break;
          }
        }
      }
    #endif
    }

    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    void pb::IBufferMessage.InternalMergeFrom(ref pb::ParseContext input) {
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            _unknownFields = pb::UnknownFieldSet.MergeFieldFrom(_unknownFields, ref input);
            break;
          case 18: {
            StateOfCharge = input.ReadString();
            break;
          }
          case 26: {
            Charge = input.ReadString();
            break;
          }
          case 34: {
            Discharge = input.ReadString();
            break;
          }
          case 42: {
            SerialNumber = input.ReadString();
            break;
          }
          case 50: {
            Manufacturer = input.ReadString();
            break;
          }
          case 58: {
            Model = input.ReadString();
            break;
          }
          case 66: {
            Status = input.ReadString();
            break;
          }
          case 74: {
            OperationalMode = input.ReadString();
            break;
          }
          case 82: {
            IpAddress = input.ReadString();
            break;
          }
          case 90: {
            FirmwareVersion = input.ReadString();
            break;
          }
          case 98: {
            HardwareVersion = input.ReadString();
            break;
          }
          case 104: {
            GridConnected = input.ReadBool();
            break;
          }
          case 114: {
            DeviceTime = input.ReadString();
            break;
          }
          case 122: {
            DailyWorktime = input.ReadString();
            break;
          }
          case 128: {
            PowerLimitation = input.ReadBool();
            break;
          }
          case 138: {
            PowerLimitationValue = input.ReadString();
            break;
          }
        }
      }
    }
    #endif

  }

  #endregion

}

#endregion Designer generated code

// <auto-generated>
//     Generated by the protocol buffer compiler.  DO NOT EDIT!
//     source: command/security/GetInstalledCertificateIds.proto
// </auto-generated>
#pragma warning disable 1591, 0612, 3021, 8981
#region Designer generated code

using pb = global::Google.Protobuf;
using pbc = global::Google.Protobuf.Collections;
using pbr = global::Google.Protobuf.Reflection;
using scg = global::System.Collections.Generic;
namespace GetInstalledCertificateIds {

  /// <summary>Holder for reflection information generated from command/security/GetInstalledCertificateIds.proto</summary>
  public static partial class GetInstalledCertificateIdsReflection {

    #region Descriptor
    /// <summary>File descriptor for command/security/GetInstalledCertificateIds.proto</summary>
    public static pbr::FileDescriptor Descriptor {
      get { return descriptor; }
    }
    private static pbr::FileDescriptor descriptor;

    static GetInstalledCertificateIdsReflection() {
      byte[] descriptorData = global::System.Convert.FromBase64String(
          string.Concat(
            "CjFjb21tYW5kL3NlY3VyaXR5L0dldEluc3RhbGxlZENlcnRpZmljYXRlSWRz",
            "LnByb3RvEh1nZXRfaW5zdGFsbGVkX2NlcnRpZmljYXRlX2lkcxouY29tbWFu",
            "ZC9zZWN1cml0eS9DZXJ0aWZpY2F0ZUhhc2hEYXRhVHlwZS5wcm90bxotY29t",
            "bWFuZC9zZWN1cml0eS9DZXJ0aWZpY2F0ZVVzZUVudW1UeXBlLnByb3RvInAK",
            "IUdldEluc3RhbGxlZENlcnRpZmljYXRlSWRzQ29tbWFuZBJLChBjZXJ0aWZp",
            "Y2F0ZV90eXBlGAEgASgOMjEuY2VydGlmaWNhdGVfdXNlX2VudW1fdHlwZS5D",
            "ZXJ0aWZpY2F0ZVVzZUVudW1UeXBlIsQCCilHZXRJbnN0YWxsZWRDZXJ0aWZp",
            "Y2F0ZUlkc0NvbW1hbmRSZXNwb25zZRJ+CgZzdGF0dXMYASABKA4ybi5nZXRf",
            "aW5zdGFsbGVkX2NlcnRpZmljYXRlX2lkcy5HZXRJbnN0YWxsZWRDZXJ0aWZp",
            "Y2F0ZUlkc0NvbW1hbmRSZXNwb25zZS5HZXRJbnN0YWxsZWRDZXJ0aWZpY2F0",
            "ZVN0YXR1c0VudW1UeXBlElIKFWNlcnRpZmljYXRlX2hhc2hfZGF0YRgCIAMo",
            "CzIzLmNlcnRpZmljYXRlX2hhc2hfZGF0YV90eXBlLkNlcnRpZmljYXRlSGFz",
            "aERhdGFUeXBlIkMKJUdldEluc3RhbGxlZENlcnRpZmljYXRlU3RhdHVzRW51",
            "bVR5cGUSDAoIQWNjZXB0ZWQQABIMCghOb3RGb3VuZBABQpsBCiJjb20uY29k",
            "aWJseS5tb2RlbC5jb21tYW5kLnNlY3VyaXR5Qh9HZXRJbnN0YWxsZWRDZXJ0",
            "aWZpY2F0ZUlkc1Byb3RvygIiRG9tYWluTW9kZWxcUHJvdG9cQ29tbWFuZFxT",
            "ZWN1cml0eeICLkRvbWFpbk1vZGVsXFByb3RvXENvbW1hbmRcU2VjdXJpdHlc",
            "R1BCTWV0YWRhdGFiBnByb3RvMw=="));
      descriptor = pbr::FileDescriptor.FromGeneratedCode(descriptorData,
          new pbr::FileDescriptor[] { global::CertificateHashDataType.CertificateHashDataTypeReflection.Descriptor, global::CertificateUseEnumType.CertificateUseEnumTypeReflection.Descriptor, },
          new pbr::GeneratedClrTypeInfo(null, null, new pbr::GeneratedClrTypeInfo[] {
            new pbr::GeneratedClrTypeInfo(typeof(global::GetInstalledCertificateIds.GetInstalledCertificateIdsCommand), global::GetInstalledCertificateIds.GetInstalledCertificateIdsCommand.Parser, new[]{ "CertificateType" }, null, null, null, null),
            new pbr::GeneratedClrTypeInfo(typeof(global::GetInstalledCertificateIds.GetInstalledCertificateIdsCommandResponse), global::GetInstalledCertificateIds.GetInstalledCertificateIdsCommandResponse.Parser, new[]{ "Status", "CertificateHashData" }, null, new[]{ typeof(global::GetInstalledCertificateIds.GetInstalledCertificateIdsCommandResponse.Types.GetInstalledCertificateStatusEnumType) }, null, null)
          }));
    }
    #endregion

  }
  #region Messages
  public sealed partial class GetInstalledCertificateIdsCommand : pb::IMessage<GetInstalledCertificateIdsCommand>
  #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
      , pb::IBufferMessage
  #endif
  {
    private static readonly pb::MessageParser<GetInstalledCertificateIdsCommand> _parser = new pb::MessageParser<GetInstalledCertificateIdsCommand>(() => new GetInstalledCertificateIdsCommand());
    private pb::UnknownFieldSet _unknownFields;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public static pb::MessageParser<GetInstalledCertificateIdsCommand> Parser { get { return _parser; } }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public static pbr::MessageDescriptor Descriptor {
      get { return global::GetInstalledCertificateIds.GetInstalledCertificateIdsReflection.Descriptor.MessageTypes[0]; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    pbr::MessageDescriptor pb::IMessage.Descriptor {
      get { return Descriptor; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public GetInstalledCertificateIdsCommand() {
      OnConstruction();
    }

    partial void OnConstruction();

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public GetInstalledCertificateIdsCommand(GetInstalledCertificateIdsCommand other) : this() {
      certificateType_ = other.certificateType_;
      _unknownFields = pb::UnknownFieldSet.Clone(other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public GetInstalledCertificateIdsCommand Clone() {
      return new GetInstalledCertificateIdsCommand(this);
    }

    /// <summary>Field number for the "certificate_type" field.</summary>
    public const int CertificateTypeFieldNumber = 1;
    private global::CertificateUseEnumType.CertificateUseEnumType certificateType_ = global::CertificateUseEnumType.CertificateUseEnumType.CentralSystemRootCertificate;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public global::CertificateUseEnumType.CertificateUseEnumType CertificateType {
      get { return certificateType_; }
      set {
        certificateType_ = value;
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public override bool Equals(object other) {
      return Equals(other as GetInstalledCertificateIdsCommand);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public bool Equals(GetInstalledCertificateIdsCommand other) {
      if (ReferenceEquals(other, null)) {
        return false;
      }
      if (ReferenceEquals(other, this)) {
        return true;
      }
      if (CertificateType != other.CertificateType) return false;
      return Equals(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public override int GetHashCode() {
      int hash = 1;
      if (CertificateType != global::CertificateUseEnumType.CertificateUseEnumType.CentralSystemRootCertificate) hash ^= CertificateType.GetHashCode();
      if (_unknownFields != null) {
        hash ^= _unknownFields.GetHashCode();
      }
      return hash;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public override string ToString() {
      return pb::JsonFormatter.ToDiagnosticString(this);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void WriteTo(pb::CodedOutputStream output) {
    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
      output.WriteRawMessage(this);
    #else
      if (CertificateType != global::CertificateUseEnumType.CertificateUseEnumType.CentralSystemRootCertificate) {
        output.WriteRawTag(8);
        output.WriteEnum((int) CertificateType);
      }
      if (_unknownFields != null) {
        _unknownFields.WriteTo(output);
      }
    #endif
    }

    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    void pb::IBufferMessage.InternalWriteTo(ref pb::WriteContext output) {
      if (CertificateType != global::CertificateUseEnumType.CertificateUseEnumType.CentralSystemRootCertificate) {
        output.WriteRawTag(8);
        output.WriteEnum((int) CertificateType);
      }
      if (_unknownFields != null) {
        _unknownFields.WriteTo(ref output);
      }
    }
    #endif

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public int CalculateSize() {
      int size = 0;
      if (CertificateType != global::CertificateUseEnumType.CertificateUseEnumType.CentralSystemRootCertificate) {
        size += 1 + pb::CodedOutputStream.ComputeEnumSize((int) CertificateType);
      }
      if (_unknownFields != null) {
        size += _unknownFields.CalculateSize();
      }
      return size;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void MergeFrom(GetInstalledCertificateIdsCommand other) {
      if (other == null) {
        return;
      }
      if (other.CertificateType != global::CertificateUseEnumType.CertificateUseEnumType.CentralSystemRootCertificate) {
        CertificateType = other.CertificateType;
      }
      _unknownFields = pb::UnknownFieldSet.MergeFrom(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void MergeFrom(pb::CodedInputStream input) {
    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
      input.ReadRawMessage(this);
    #else
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            _unknownFields = pb::UnknownFieldSet.MergeFieldFrom(_unknownFields, input);
            break;
          case 8: {
            CertificateType = (global::CertificateUseEnumType.CertificateUseEnumType) input.ReadEnum();
            break;
          }
        }
      }
    #endif
    }

    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    void pb::IBufferMessage.InternalMergeFrom(ref pb::ParseContext input) {
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            _unknownFields = pb::UnknownFieldSet.MergeFieldFrom(_unknownFields, ref input);
            break;
          case 8: {
            CertificateType = (global::CertificateUseEnumType.CertificateUseEnumType) input.ReadEnum();
            break;
          }
        }
      }
    }
    #endif

  }

  public sealed partial class GetInstalledCertificateIdsCommandResponse : pb::IMessage<GetInstalledCertificateIdsCommandResponse>
  #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
      , pb::IBufferMessage
  #endif
  {
    private static readonly pb::MessageParser<GetInstalledCertificateIdsCommandResponse> _parser = new pb::MessageParser<GetInstalledCertificateIdsCommandResponse>(() => new GetInstalledCertificateIdsCommandResponse());
    private pb::UnknownFieldSet _unknownFields;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public static pb::MessageParser<GetInstalledCertificateIdsCommandResponse> Parser { get { return _parser; } }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public static pbr::MessageDescriptor Descriptor {
      get { return global::GetInstalledCertificateIds.GetInstalledCertificateIdsReflection.Descriptor.MessageTypes[1]; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    pbr::MessageDescriptor pb::IMessage.Descriptor {
      get { return Descriptor; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public GetInstalledCertificateIdsCommandResponse() {
      OnConstruction();
    }

    partial void OnConstruction();

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public GetInstalledCertificateIdsCommandResponse(GetInstalledCertificateIdsCommandResponse other) : this() {
      status_ = other.status_;
      certificateHashData_ = other.certificateHashData_.Clone();
      _unknownFields = pb::UnknownFieldSet.Clone(other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public GetInstalledCertificateIdsCommandResponse Clone() {
      return new GetInstalledCertificateIdsCommandResponse(this);
    }

    /// <summary>Field number for the "status" field.</summary>
    public const int StatusFieldNumber = 1;
    private global::GetInstalledCertificateIds.GetInstalledCertificateIdsCommandResponse.Types.GetInstalledCertificateStatusEnumType status_ = global::GetInstalledCertificateIds.GetInstalledCertificateIdsCommandResponse.Types.GetInstalledCertificateStatusEnumType.Accepted;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public global::GetInstalledCertificateIds.GetInstalledCertificateIdsCommandResponse.Types.GetInstalledCertificateStatusEnumType Status {
      get { return status_; }
      set {
        status_ = value;
      }
    }

    /// <summary>Field number for the "certificate_hash_data" field.</summary>
    public const int CertificateHashDataFieldNumber = 2;
    private static readonly pb::FieldCodec<global::CertificateHashDataType.CertificateHashDataType> _repeated_certificateHashData_codec
        = pb::FieldCodec.ForMessage(18, global::CertificateHashDataType.CertificateHashDataType.Parser);
    private readonly pbc::RepeatedField<global::CertificateHashDataType.CertificateHashDataType> certificateHashData_ = new pbc::RepeatedField<global::CertificateHashDataType.CertificateHashDataType>();
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public pbc::RepeatedField<global::CertificateHashDataType.CertificateHashDataType> CertificateHashData {
      get { return certificateHashData_; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public override bool Equals(object other) {
      return Equals(other as GetInstalledCertificateIdsCommandResponse);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public bool Equals(GetInstalledCertificateIdsCommandResponse other) {
      if (ReferenceEquals(other, null)) {
        return false;
      }
      if (ReferenceEquals(other, this)) {
        return true;
      }
      if (Status != other.Status) return false;
      if(!certificateHashData_.Equals(other.certificateHashData_)) return false;
      return Equals(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public override int GetHashCode() {
      int hash = 1;
      if (Status != global::GetInstalledCertificateIds.GetInstalledCertificateIdsCommandResponse.Types.GetInstalledCertificateStatusEnumType.Accepted) hash ^= Status.GetHashCode();
      hash ^= certificateHashData_.GetHashCode();
      if (_unknownFields != null) {
        hash ^= _unknownFields.GetHashCode();
      }
      return hash;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public override string ToString() {
      return pb::JsonFormatter.ToDiagnosticString(this);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void WriteTo(pb::CodedOutputStream output) {
    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
      output.WriteRawMessage(this);
    #else
      if (Status != global::GetInstalledCertificateIds.GetInstalledCertificateIdsCommandResponse.Types.GetInstalledCertificateStatusEnumType.Accepted) {
        output.WriteRawTag(8);
        output.WriteEnum((int) Status);
      }
      certificateHashData_.WriteTo(output, _repeated_certificateHashData_codec);
      if (_unknownFields != null) {
        _unknownFields.WriteTo(output);
      }
    #endif
    }

    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    void pb::IBufferMessage.InternalWriteTo(ref pb::WriteContext output) {
      if (Status != global::GetInstalledCertificateIds.GetInstalledCertificateIdsCommandResponse.Types.GetInstalledCertificateStatusEnumType.Accepted) {
        output.WriteRawTag(8);
        output.WriteEnum((int) Status);
      }
      certificateHashData_.WriteTo(ref output, _repeated_certificateHashData_codec);
      if (_unknownFields != null) {
        _unknownFields.WriteTo(ref output);
      }
    }
    #endif

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public int CalculateSize() {
      int size = 0;
      if (Status != global::GetInstalledCertificateIds.GetInstalledCertificateIdsCommandResponse.Types.GetInstalledCertificateStatusEnumType.Accepted) {
        size += 1 + pb::CodedOutputStream.ComputeEnumSize((int) Status);
      }
      size += certificateHashData_.CalculateSize(_repeated_certificateHashData_codec);
      if (_unknownFields != null) {
        size += _unknownFields.CalculateSize();
      }
      return size;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void MergeFrom(GetInstalledCertificateIdsCommandResponse other) {
      if (other == null) {
        return;
      }
      if (other.Status != global::GetInstalledCertificateIds.GetInstalledCertificateIdsCommandResponse.Types.GetInstalledCertificateStatusEnumType.Accepted) {
        Status = other.Status;
      }
      certificateHashData_.Add(other.certificateHashData_);
      _unknownFields = pb::UnknownFieldSet.MergeFrom(_unknownFields, other._unknownFields);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public void MergeFrom(pb::CodedInputStream input) {
    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
      input.ReadRawMessage(this);
    #else
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            _unknownFields = pb::UnknownFieldSet.MergeFieldFrom(_unknownFields, input);
            break;
          case 8: {
            Status = (global::GetInstalledCertificateIds.GetInstalledCertificateIdsCommandResponse.Types.GetInstalledCertificateStatusEnumType) input.ReadEnum();
            break;
          }
          case 18: {
            certificateHashData_.AddEntriesFrom(input, _repeated_certificateHashData_codec);
            break;
          }
        }
      }
    #endif
    }

    #if !GOOGLE_PROTOBUF_REFSTRUCT_COMPATIBILITY_MODE
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    void pb::IBufferMessage.InternalMergeFrom(ref pb::ParseContext input) {
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            _unknownFields = pb::UnknownFieldSet.MergeFieldFrom(_unknownFields, ref input);
            break;
          case 8: {
            Status = (global::GetInstalledCertificateIds.GetInstalledCertificateIdsCommandResponse.Types.GetInstalledCertificateStatusEnumType) input.ReadEnum();
            break;
          }
          case 18: {
            certificateHashData_.AddEntriesFrom(ref input, _repeated_certificateHashData_codec);
            break;
          }
        }
      }
    }
    #endif

    #region Nested types
    /// <summary>Container for nested types declared in the GetInstalledCertificateIdsCommandResponse message type.</summary>
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    [global::System.CodeDom.Compiler.GeneratedCode("protoc", null)]
    public static partial class Types {
      public enum GetInstalledCertificateStatusEnumType {
        [pbr::OriginalName("Accepted")] Accepted = 0,
        [pbr::OriginalName("NotFound")] NotFound = 1,
      }

    }
    #endregion

  }

  #endregion

}

#endregion Designer generated code

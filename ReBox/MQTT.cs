﻿using System;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using Charger;
using Electricity;
using WindTurbine;
using Google.Protobuf;
using Google.Protobuf.WellKnownTypes;

namespace ReBox
{
    partial class Rebox
    {
        public static void HelloMQTT()
        {
            Console.WriteLine("Hello MQTT!");
        }

        public string readData(int d)
        {
            string ret = "";
            if (DATA[d].TYPE == "BYTE") ret = DATA[d].uVALUE16.ToString();
            if (DATA[d].TYPE == "INT16") ret = DATA[d].dVALUE16.ToString();
            if (DATA[d].TYPE == "UINT16") ret = DATA[d].uVALUE16.ToString();
            if (DATA[d].TYPE == "INT32") ret = DATA[d].dVALUE32.ToString();
            if (DATA[d].TYPE == "UINT32") ret = DATA[d].uVALUE32.ToString();
            if (DATA[d].TYPE == "DOUBLE") ret = DATA[d].fVALUE.ToString();
            if (DATA[d].TYPE == "BOOL") ret = DATA[d].bVALUE.ToString();

            return ret;
        }


        public bool sendMessage(int _i)
        {
            bool ret = false;
            Measurements obj1 = new Measurements();


            Measurement obj = new Measurement();
            Measurement objEL = new Measurement();
            Measurement objPF = new Measurement();
            Measurement objWE = new Measurement();
            Measurement objWT = new Measurement();
            Measurement objWTG = new Measurement();
            Measurement objSI = new Measurement();
            Measurement objST = new Measurement();

            obj1.Measurements_.Add(objWT);
            obj1.Measurements_.Add(objPF);
            obj1.Measurements_.Add(objWE);
            obj1.Measurements_.Add(objWT);
            obj1.Measurements_.Add(objSI);
            obj1.Measurements_.Add(objST);
            obj1.Measurements_.Add(objWTG);
            Measurements obj2 = new Measurements();


            objEL.Electricity = new Electricity.Electricity();
            objPF.PowerFactors = new PowerFactors();
            objWE.Weather = new Weather.Weather();
            objWT.WindTurbine = new WindTurbine.WindTurbine();
            objWTG.WindTurbineGenerator = new WindTurbineGenerator();
            objSI.Site = new Site.Site();
            objST.State = new Generic.State();

            obj2.Measurements_.Add(objWT);

            if (PAYLOAD[_i].PHASE == "") objEL.Electricity.Phase = Electricity.Electricity.Types.Phase.Total;
            if (PAYLOAD[_i].PHASE == "L1") objEL.Electricity.Phase = Electricity.Electricity.Types.Phase.L1;
            if (PAYLOAD[_i].PHASE == "L2") objEL.Electricity.Phase = Electricity.Electricity.Types.Phase.L2;
            if (PAYLOAD[_i].PHASE == "L3") objEL.Electricity.Phase = Electricity.Electricity.Types.Phase.L3;
            if (PAYLOAD[_i].PHASE == "TOTAL") objEL.Electricity.Phase = Electricity.Electricity.Types.Phase.Total;
            if (PAYLOAD[_i].SAMPLETYPE == "") objEL.Electricity.SampleType = Electricity.Electricity.Types.SampleType.Avg;
            if (PAYLOAD[_i].SAMPLETYPE == "AVG") objEL.Electricity.SampleType = Electricity.Electricity.Types.SampleType.Avg;
            if (PAYLOAD[_i].SAMPLETYPE == "MIN") objEL.Electricity.SampleType = Electricity.Electricity.Types.SampleType.Min;
            if (PAYLOAD[_i].SAMPLETYPE == "MAX") objEL.Electricity.SampleType = Electricity.Electricity.Types.SampleType.Max;

            obj.DeviceIdentifier = PAYLOAD[_i].SN;
            obj.Timestamp = Timestamp.FromDateTime(DateTime.UtcNow);

            objWT.DeviceIdentifier = obj.DeviceIdentifier;
            objWTG.DeviceIdentifier = obj.DeviceIdentifier;
            objWE.DeviceIdentifier = obj.DeviceIdentifier;
            objEL.DeviceIdentifier = obj.DeviceIdentifier;
            objSI.DeviceIdentifier = obj.DeviceIdentifier;
            objST.DeviceIdentifier = obj.DeviceIdentifier;
            objPF.DeviceIdentifier = obj.DeviceIdentifier;

            objWT.Timestamp = obj.Timestamp;
            objWTG.Timestamp = obj.Timestamp;
            objWE.Timestamp = obj.Timestamp;
            objEL.Timestamp = obj.Timestamp;
            objSI.Timestamp = obj.Timestamp;
            objST.Timestamp = obj.Timestamp;
            objPF.Timestamp = obj.Timestamp;


            bool isPF = false;
            bool isWE = false;
            bool isEL = false;
            bool isWT = false;
            bool isWTG = false;
            bool isSI = false;
            bool isST = false;

            for (int t = 1; t < ITEM_MAX; t++)
            {
                if (ITEM[t].OBJECT == "PowerFactors") isPF = true;
                if (ITEM[t].OBJECT == "Weather") isWE = true;
                if (ITEM[t].OBJECT == "Electricity") isEL = true;
                if (ITEM[t].OBJECT == "WindTurbine") isWT = true;
                if (ITEM[t].OBJECT == "WindTurbineGenerator") isWTG = true;
                if (ITEM[t].OBJECT == "Site") isSI = true;
                if (ITEM[t].OBJECT == "State") isST = true;

                if (ITEM[t].OBJECT == "PowerFactors" && ITEM[t].TYPE == "ActivePower") objPF.PowerFactors.ActivePower = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "PowerFactors" && ITEM[t].TYPE == "ApparentPower") objPF.PowerFactors.ApparentPower = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "PowerFactors" && ITEM[t].TYPE == "ReactivePower") objPF.PowerFactors.ReactivePower = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "PowerFactors" && ITEM[t].TYPE == "CosPhi") objPF.PowerFactors.CosPhi = readData(ITEM[t].DATA_ID);

                if (ITEM[t].OBJECT == "Weather" && ITEM[t].TYPE == "Weather") objWE.Weather.Weather_ = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "Weather" && ITEM[t].TYPE == "WeatherDescription") objWE.Weather.WeatherDescription = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "Weather" && ITEM[t].TYPE == "WeatherIcon") objWE.Weather.WeatherIcon = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "Weather" && ITEM[t].TYPE == "Temperature") objWE.Weather.Temperature = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "Weather" && ITEM[t].TYPE == "TemperatureFeelsLike") objWE.Weather.TemperatureFeelsLike = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "Weather" && ITEM[t].TYPE == "TemperaturePvPanel") objWE.Weather.TemperaturePvPanel = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "Weather" && ITEM[t].TYPE == "Pressure") objWE.Weather.Pressure = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "Weather" && ITEM[t].TYPE == "Humidity") objWE.Weather.Humidity = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "Weather" && ITEM[t].TYPE == "Cloudiness") objWE.Weather.Cloudiness = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "Weather" && ITEM[t].TYPE == "WindSpeed") objWE.Weather.WindSpeed = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "Weather" && ITEM[t].TYPE == "WindDegree") objWE.Weather.WindDegree = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "Weather" && ITEM[t].TYPE == "Rain") objWE.Weather.Rain = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "Weather" && ITEM[t].TYPE == "Snow") objWE.Weather.Snow = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "Weather" && ITEM[t].TYPE == "Irradiance") objWE.Weather.Irradiance = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "Weather" && ITEM[t].TYPE == "DailyIrradiance") objWE.Weather.DailyIrradiance = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "Weather" && ITEM[t].TYPE == "WindDirection")
                {
                    string winddirection = readData(ITEM[t].DATA_ID);
                    if (System.Enum.TryParse(winddirection, out Weather.Weather.Types.CardinalDirection winddir))
                    {
                        objWE.Weather.WindDirection = winddir;
                    }
                }

                if (ITEM[t].OBJECT == "Electricity" && ITEM[t].TYPE == "Current") objEL.Electricity.Current = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "Electricity" && ITEM[t].TYPE == "Voltage") objEL.Electricity.Voltage = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "Electricity" && ITEM[t].TYPE == "Frequency") objEL.Electricity.Frequency = readData(ITEM[t].DATA_ID);

                if (ITEM[t].OBJECT == "WindTurbineGenerator" && ITEM[t].TYPE == "ActivePower") objWTG.WindTurbineGenerator.ActivePower = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "WindTurbineGenerator" && ITEM[t].TYPE == "ReactivePower") objWTG.WindTurbineGenerator.ReactivePower = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "WindTurbineGenerator" && ITEM[t].TYPE == "StatorCurrent") objWTG.WindTurbineGenerator.StatorCurrent = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "WindTurbineGenerator" && ITEM[t].TYPE == "StatorVoltage") objWTG.WindTurbineGenerator.StatorVoltage = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "WindTurbineGenerator" && ITEM[t].TYPE == "StatorTemperature") objWTG.WindTurbineGenerator.StatorTemperature = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "WindTurbineGenerator" && ITEM[t].TYPE == "SideCurrent") objWTG.WindTurbineGenerator.SideCurrent = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "WindTurbineGenerator" && ITEM[t].TYPE == "SideVoltage") objWTG.WindTurbineGenerator.SideVoltage = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "WindTurbineGenerator" && ITEM[t].TYPE == "SideTemperature") objWTG.WindTurbineGenerator.SideTemperature = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "WindTurbineGenerator" && ITEM[t].TYPE == "Frequency") objWTG.WindTurbineGenerator.Frequency = readData(ITEM[t].DATA_ID);

                if (ITEM[t].OBJECT == "WindTurbine" && ITEM[t].TYPE == "NacelleTemperature") objWT.WindTurbine.NacelleTemperature = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "WindTurbine" && ITEM[t].TYPE == "NacelleYawAngle") objWT.WindTurbine.NacelleYawAngle = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "WindTurbine" && ITEM[t].TYPE == "ConverterTemperature") objWT.WindTurbine.ConverterTemperature = readData(ITEM[t].DATA_ID);

                if (ITEM[t].OBJECT == "Site" && ITEM[t].TYPE == "SelfConsumption") objSI.Site.SelfConsumption = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "Site" && ITEM[t].TYPE == "TotalSelfConsumedEnergy") objSI.Site.TotalSelfConsumedEnergy = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "Site" && ITEM[t].TYPE == "TotalProducedEnergy") objSI.Site.TotalProducedEnergy = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "Site" && ITEM[t].TYPE == "TodayProducedEnergy") objSI.Site.TodayProducedEnergy = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "Site" && ITEM[t].TYPE == "ThisMonthProducedEnergy") objSI.Site.ThisMonthProducedEnergy = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "Site" && ITEM[t].TYPE == "ThisYearProducedEnergy") objSI.Site.ThisYearProducedEnergy = readData(ITEM[t].DATA_ID);

                if (ITEM[t].OBJECT == "State" && ITEM[t].TYPE == "StateOfCharge") objST.State.StateOfCharge = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "State" && ITEM[t].TYPE == "Charge") objST.State.Charge = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "State" && ITEM[t].TYPE == "Discharge") objST.State.Discharge = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "State" && ITEM[t].TYPE == "SerialNumber") objST.State.SerialNumber = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "State" && ITEM[t].TYPE == "Manufacturer") objST.State.Manufacturer = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "State" && ITEM[t].TYPE == "Model") objST.State.Model = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "State" && ITEM[t].TYPE == "Status") objST.State.Status = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "State" && ITEM[t].TYPE == "OperationalModel") objST.State.OperationalMode = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "State" && ITEM[t].TYPE == "IpAddress") objST.State.IpAddress = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "State" && ITEM[t].TYPE == "FirmwareVersion") objST.State.FirmwareVersion = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "State" && ITEM[t].TYPE == "HardwareVersion") objST.State.HardwareVersion = readData(ITEM[t].DATA_ID);
                //if (ITEM[t].OBJECT == "State" && ITEM[t].TYPE == "GridConnected") objST.State.GridConnected = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "State" && ITEM[t].TYPE == "DeviceTime") objST.State.DeviceTime = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "State" && ITEM[t].TYPE == "DailyWorktime") objST.State.DailyWorktime = readData(ITEM[t].DATA_ID);
                //if (ITEM[t].OBJECT == "State" && ITEM[t].TYPE == "power_limitation") objST.State.PowerLimitation = readData(ITEM[t].DATA_ID);
                if (ITEM[t].OBJECT == "State" && ITEM[t].TYPE == "PowerLimitationValue") objST.State.PowerLimitationValue = readData(ITEM[t].DATA_ID);


            }//for (int t=1; t<ITEM_MAX; t++)

            //if (PAYLOAD[_i].OBJECT == "Electricity") obj.Electricity = objEL.Electricity;
            //if (PAYLOAD[_i].OBJECT == "Weather") obj.Weather = objWE.Weather;
            //if (PAYLOAD[_i].OBJECT == "WindTurbine") obj.WindTurbine = objWT.WindTurbine;
            //if (PAYLOAD[_i].OBJECT == "WindTurbineGenerator") obj.WindTurbineGenerator = objWTG.WindTurbineGenerator;
            //if (PAYLOAD[_i].OBJECT == "State") obj.State = objST.State;
            //if (PAYLOAD[_i].OBJECT == "Site") obj.Site = objSI.Site;
            //if (PAYLOAD[_i].OBJECT == "PowerFactors") obj.PowerFactors = objPF.PowerFactors;


            byte[] serializedData;
            using (MemoryStream memoryStream = new MemoryStream())
            {
                CodedOutputStream codedOutputStream = new CodedOutputStream(memoryStream);
                obj1.WriteTo(codedOutputStream);
                codedOutputStream.Flush();
                serializedData = memoryStream.ToArray();
            }

            if (MQTT_offline == false)
            {
                ret = sendToMqtt(serializedData, PAYLOAD[_i].SN, PAYLOAD[_i].TOPIC);
            }
            else
            {
                ret = false;
            }

            if (ret == false)
            {
                MQTT_offline = true;
                sendToOfflineMQTT(PAYLOAD[_i].SN, PAYLOAD[_i].TOPIC, codeBytes(serializedData));
            }

            //Debug.WriteLine(Encoding.ASCII.GetString(serializedData));

            string hexString = BitConverter.ToString(serializedData).Replace("-", " ");
            Console.WriteLine(hexString);

            return ret;
        }
        public bool sendToMqtt(byte[] serializedData, string SN, string TOPIC)
        {
            bool ret = false;


            try
            {
                if (MqttClient == null)
                    MqttClient = new MqttClient(MQTT_URL, Convert.ToInt16(MQTT_PORT), false, null, null, MqttSslProtocols.None);
                // Połączenie z brokerem MQTT
                if (MqttClient.IsConnected == false)
                    MqttClient.Connect(SERIALNUMBER, MQTT_USERNAME, MQTT_PASSWORD);

                if (MqttClient.IsConnected)
                {
                    MqttClient.Publish($"{MQTT_TOPIC}{SN}{TOPIC}", serializedData, MqttMsgBase.QOS_LEVEL_AT_LEAST_ONCE, false);
                    //MqttClient.Disconnect();
                    //Console.WriteLine("Połączono z brokerem MQTT.");
                    ret = true;
                }
                else
                {
                    ret = false;
                    //Console.WriteLine("Nie udało się połączyć z brokerem MQTT.");
                }
            }
            catch (Exception ex)
            {
                ret = false;
                //Console.WriteLine($"Wystąpił błąd: {ex.Message}");
            }
            return ret;
        }



        public async Task<bool> writeProcedure(int _i, string value)
        {
            //_i nr w SAVE
            //i nr SRV
            bool ret = false;
            int i = SAVE[_i].SERVER_ID;
            if (SRV[i].TYPE == "OPC-UA")
            {
                ret = await writeOpcUa(_i, value);
            }
            if (SRV[i].TYPE == "OPC-DA")
            {
                ret = writeOpcDa(_i, value);
            }
            if (SRV[i].TYPE == "MODBUSTCP")
            {
                ret = writeModbusTcp(_i, value);
            }
            if (SRV[i].TYPE == "MODBUSRTU")
            {
                ret = writeModbusRtu(_i, value);
            }
            if (SRV[i].TYPE == "MODBUSASCII")
            {
                ret = writeModbusAscii(_i, value);
            }
            MEAS[_i].TIMESTAMP = GetTimeStamp();
            return ret;
        }

        public bool compareTimeStamps(string now, string timestamp)
        {
            // Jeśli timestamp jest pusty, zwróć true
            if (string.IsNullOrEmpty(timestamp))
            {
                return true;
            }

            // Jeśli timestamp nie jest pusty, porównaj znaczniki czasowe
            DateTimeOffset nowTime = DateTimeOffset.Parse(now);
            DateTimeOffset timestampTime = DateTimeOffset.Parse(timestamp);

            // Zwróć true, jeśli now jest większe lub równe timestamp
            return nowTime >= timestampTime;
        }


        public void runFromQueue(int _i)
        {
            string now = GetTimeStamp();
            //Przeszukanie wszystkich procedur
            for (int c = 0; c < COMMAND_MAX; c++)
            {
                //czy COMMAND[c] zawiera procedure
                if (COMMAND[c].ON == 1)
                {
                    //czy jest czas aby uruchomic
                    if (compareTimeStamps(now, COMMAND[c].TIMESTAMP) == true)
                    {
                        //przeszukanie wszystkich peocedur
                        for (int p = 1; p < PROC_MAX; p++)
                        {
                            //wybor zgodnej z PROCEDURE_NUMBER z COMMAND
                            if (PROC[p].PROCEDURE_NUMBER == COMMAND[c].PROCEDURE_NUMBER)
                            {
                                //uruchomienie konkretnego polecenia zapisu
                                //SAVE[PROC[p].SAVE_ID] to jest obiekt ktory uruchamiam
                                writeProcedure(PROC[p].SAVE_ID, COMMAND[c].VALUE);
                            }

                        }
                        removeFromQueue(_i);

                    }
                }
            }
        }


        public void removeFromQueue(int _i)
        {
            COMMAND[_i].ON = 0;
        }

        public void addToQueue(string timestamp, int procedure_number, string value)
        {
            bool finded = false;
            int pos = 0;
            while (finded == false)
            {
                if (COMMAND[pos].ON == 0)
                {
                    COMMAND[pos].ON = 1;
                    COMMAND[pos].VALUE = value;
                    COMMAND[pos].TIMESTAMP = timestamp;
                    COMMAND[pos].PROCEDURE_NUMBER = procedure_number;
                    finded = true;
                }
                else
                {
                    pos++;
                }


                if (pos == COMMAND_MAX) finded = true;
            }

        }


        private static void Client_ConnectionClosed(object sender, EventArgs e)
        {
            // Obsługa zamknięcia połączenia z brokerem MQTT
            //Console.WriteLine("Połączenie z brokerem MQTT zostało zamknięte.");
        }

        public void Client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        {
            // Obsługa przychodzących wiadomości
            string receivedMessage = BitConverter.ToString(e.Message);

            // Deserializacja wiadomości na serializedData
            byte[] serializedData = e.Message;

            // Obsługa wiadomości w zależności od zawartości
            string Topic = e.Topic;

            Command.Command cmd = new Command.Command();
            int receivedCommand = 0;
            try
            {
                cmd = Command.Command.Parser.ParseFrom(serializedData);
                receivedCommand = 1; //odebrano obiekt protobuf
            }
            catch
            {
                receivedCommand = 0;
            }

            if (receivedCommand == 1 && cmd != null)
            {

                Commands cmd_rcv = new Commands(); ;
                switch (cmd.CommandCase)
                {

                    case Command.Command.CommandOneofCase.ChangeConfigurationCommand:
                        var changeConfigCmd = cmd.ChangeConfigurationCommand;
                        Console.WriteLine("Otrzymano ChangeConfigurationCommand");

                        // Obsługa ChangeConfigurationCommand
                        foreach (var keyValue in changeConfigCmd.Keys)
                        {
                            if (keyValue.Key == "TIMESTAMP") cmd_rcv.TIMESTAMP = keyValue.Value;
                            if (keyValue.Key == "SN") cmd_rcv.SN = keyValue.Value;
                            if (keyValue.Key == "PROCEDURE_NUMBER") cmd_rcv.PROCEDURE_NUMBER = Convert.ToInt16(keyValue.Value);
                            if (keyValue.Key == "VALUE") cmd_rcv.VALUE = keyValue.Value;

                            //Console.WriteLine($"Key: {keyValue.Key}, Value: {keyValue.Value}");
                            // Dodaj obsługę dla każdej pary klucz-wartość (KeyValue)
                        }

                        for (int r = 1; r < RECEIVER_MAX; r++)
                        {
                            string master_topic = $"{MQTT_TOPIC}{RECEIVER[r].SN}{RECEIVER[r].TOPIC}";
                            if (cmd_rcv.PROCEDURE_NUMBER > 0 && cmd_rcv.PROCEDURE_NUMBER < COMMAND_MAX && Topic == master_topic)
                            {
                                addToQueue(cmd_rcv.TIMESTAMP, cmd_rcv.PROCEDURE_NUMBER, cmd_rcv.VALUE);
                            }

                        }

                        break;
                    /*
                                        case Command.Command.CommandOneofCase.ResetCommand:
                                            var resetCmd = cmd.ResetCommand;
                                            Console.WriteLine("Otrzymano ResetCommand");

                                            if (resetCmd.Type == Reset.ResetCommand.Types.ResetType.Soft)
                                            {
                                                Console.WriteLine("Typ resetu: Soft");
                                                // Dodaj obsługę dla Soft Reset
                                            }
                                            break;
                    */

                    /*
                                        case Command.Command.CommandOneofCase.UpdateFirmwareCommand:
                                            var updateFirmwareCmd = cmd.UpdateFirmwareCommand;
                                            Console.WriteLine("Otrzymano UpdateFirmwareCommand");
                                            // Dodaj obsługę dla UpdateFirmwareCommand
                                            break;
                    */

                    default:
                        //Console.WriteLine("Otrzymano nieznany typ komendy w polu command");
                        break;
                }
            }

        }
    }
}
